<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Printing extends Model
{
    protected $table = 'printing';

    public function imageUrl(){
        return '/assets/images/'.$this->product_image;
    }
    public function link(){
        $urlbuilder = trim(strtolower($this->product_printing));
        return '/printing/'.$this->printing_id.'/'.preg_replace("![^a-z0-9]+!i", "-", $urlbuilder);
    }
}
