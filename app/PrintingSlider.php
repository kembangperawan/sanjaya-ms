<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrintingSlider extends Model
{
    protected $table = 'printing_slider';
    public function Printing(){
        return $this->belongsTo('App\Printing');
    }
    public function imgUrl(){
        return '/assets/images/'.$this->printing_slider_path;
    }
}
