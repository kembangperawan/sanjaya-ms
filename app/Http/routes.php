<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PrintingController@index');
Route::get('/printing', 'PrintingController@index');
Route::get('/printing/{id}/{name}', 'PrintingController@showContent');
Route::get('/gallery', 'GalleryController@index');
Route::get('/gallery/{id}/{name}', 'GalleryController@showContent');
Route::get('/works/{state}', 'WorkController@index');
Route::get('/contact', function () {
    return View('contact.index');
});
Route::get('/client', function () {
    return View('client.index');
});
Route::get('/souvenir', 'SouvenirController@index');
Route::get('/category/{categoryID}', 'SouvenirController@getSouvenirAjax');
Route::get('/souvenir/{categoryID}/{categoryName}', 'SouvenirController@getSouvenirByCategory');
Route::get('/souvenir/product/{souvenirID}/{souvenirName}', 'SouvenirController@getSouvenirBySouvenirID');
Route::get('/search', 'SearchController@index');
