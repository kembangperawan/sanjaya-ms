<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Printing;
use App\PrintingSlider;
use App\Category;
use Redirect;
use DB;

class PrintingController extends Controller
{
    public function index()
    {
        $printingSlider = PrintingSlider::where('printing_id', 0)->where('isactive', 1)->where('type', 1)->orderBy('rank')->get();
        $printingList = Printing::where('is_active', 1)->orderBy('rank')->get();
        $pageObj = array(
            'page_title' => "",
            'meta_description' => "Kipas Promosi Souvenir USB Map Plastik Promosi Flash Disk Percetakan Jakarta",
            'meta_keywords' => "Kipas Promosi, USB Promosi, Map Kancing, Map Plastik, Souvenir Pulpen, Tempat Kartu Nama, Barang Promosi, Souvenir Flash Disk, Jakarta"
        );
        $data = array(
            'pageObj' => $pageObj,
            'printingSlider' => $printingSlider,
            'printingList' => $printingList
        );
        return View('printing.index')->with($data);
    }
    public function showContent($id, $name)
    {
        $printing = Printing::where('printing_id', $id)->where('is_active', 1)->first();
        if ($printing == null) {
            return Redirect('/');
        }
        if (preg_replace('![^a-z0-9]+!i', '-', strtolower($printing->product_printing)) != $name) {
            return Redirect($printing->link());
        }
        $printingList = Printing::where('is_active', 1)->orderBy('rank')->get();
        $printingSlider = PrintingSlider::where('printing_id', $id)->where('isactive', 1)->where('type', 1)->orderBy('rank')->get();
        $printingSliderThumb = PrintingSlider::where('printing_id', $id)->where('isactive', 1)->where('type', 2)->orderBy('rank')->get();
        if (isset($printing['title'])) {
            $printing_title = $printing['title'];
        } else {
            $printing_title =  $printing['product_printing'].' - Percetakan & Souvenir Sanjaya Mitra Sejati - One Stop Services for all your need in Printing and Souvenirs';
        }
        $pageObj = array(
            'page_title' => $printing_title,
            'meta_description' => $printing['meta_description'],
            'meta_keywords' => $printing['keywords']
        );
        $data = array(
            'pageObj' => $pageObj,
            'printing' => $printing,
            'printingList' => $printingList,
            'printingSlider' => $printingSlider,
            'printingSliderThumb' => $printingSliderThumb
        );
        return View('printing.printing-detail')->with($data);
    }
}
