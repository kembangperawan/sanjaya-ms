<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index(Request $request)
    {
        $page = $request->input('page');
        if (!isset($page)) {
            $page = 1;
        }
        $tag = $request->input('tag');
        $exact = $request->input('exact');
        $allTag = array();
        $limit = 10;
        $gallery = Gallery::all();
        foreach ($gallery as $key => $value) {
            $tempArr = explode(',', $value['Tag']);
            foreach ($tempArr as $k => $v) {
                array_push($allTag, trim($v));
            }
        }
        if (!isset($tag)) {
            $gallery = Gallery::offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->get();
        } else {
            if (isset($exact)) {
                $gallery = $this->search($tag, true, $page, $limit);
            } else {
                $gallery = $this->search($tag, false, $page, $limit);
            }
        }
        $pageObj = array(
            'page_title' => 'Image Gallery - Sanjaya Mitra Sejati',
            'meta_description' => "Kipas Promosi Souvenir USB Map Plastik Promosi Flash Disk Percetakan Jakarta",
            'meta_keywords' => "Kipas Promosi, USB Promosi, Map Kancing, Map Plastik, Souvenir Pulpen, Tempat Kartu Nama, Barang Promosi, Souvenir Flash Disk, Jakarta"
        );
          $data = array(
            'pageObj' => $pageObj,
            'gallery_list' => $gallery,
            'allTag' => array_unique($allTag),
            'tag' => $tag,
            'page' => $page
            );
        return view('gallery.index')->with($data);
    }
    public function showContent($id, $name)
    {
        $gallery = Gallery::where('ImageID', $id)->first();
        if ($gallery == null) {
            return Redirect('/');
        }
        if (preg_replace('![^a-z0-9]+!i', '-', strtolower($gallery->Title)) != $name) {
            return Redirect($gallery->link());
        }
        $pageObj = array(
            "page_title" => $gallery->Title . " - Image Gallery - Sanjaya Mitra Sejati",
            "meta_description" => $gallery->Description,
            "meta_keywords" => $gallery->Tag
        );
        $data = array(
            "pageObj" => $pageObj,
            'gallery' => $gallery
        );
        return View('gallery.gallery-detail')->with($data);
    }
    public function search($keywords, $isExact, $page, $limit)
    {
        if (!$isExact) {
            $gallery = Gallery::where('Title', 'like', '%'.$keywords.'%')
                    ->orWhere('Tag', 'like', '%'.$keywords.'%')
                    ->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->get();
        } else {
            $gallery = Gallery::where('Title', '=', $keywords)
                    ->orWhere('Tag', '=', $keywords)
                    ->offset(($page - 1) * $limit)
                    ->limit($limit)
                    ->get();
        }
        
        return $gallery;
    }
}
