<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Printing;
use App\Souvenir;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $keywords = $request->input('q');
        $searchResultTitle = 'Search Result';
        $printingSearchResultTitle = $searchResultTitle;
        $souvenirSearchResultTitle = $searchResultTitle;
        $printingList = [];
        $souvenirList = [];
        if (!isset($keywords)) {
            $keywords = '';
        }
        if ($keywords != '') {
            $searchResultTitle .= ' for &quot;' . $keywords . '&quot;';
            $whereKeywords = str_replace(' ', '%', $keywords);
            $printingList = Printing::where('is_active', 1)
                                    ->where(function ($query) use ($whereKeywords) {
                                        $query->where('product_printing', 'like', '%' . $whereKeywords . '%')
                                            ->orWhere('product_image', 'like', '%' . $whereKeywords . '%')
                                            ->orWhere('link', 'like', '%' . $whereKeywords . '%')
                                            ->orWhere('keywords', 'like', '%' . $whereKeywords . '%')
                                            ->orWhere('title', 'like', '%' . $whereKeywords . '%')
                                            ->orWhere('meta_description', 'like', '%' . $whereKeywords . '%')
                                            ->orWhere('product_content', 'like', '%' . $whereKeywords . '%')
                                            ->orWhere('announcement_board', 'like', '%' . $whereKeywords . '%');
                                    })
                                    ->orderBy('rank')->get();
            $printingSearchResultTitle = $searchResultTitle . ' - ' . $printingList->count() . ' item(s) found';
            $souvenirList = Souvenir::where('is_active', 1)
                                    ->where(function ($query) use ($whereKeywords) {
                                        $query->where('souvenir_name', 'like', '%' . $whereKeywords . '%')
                                            ->orWhere('description', 'like', '%' . $whereKeywords . '%')
                                            ->orWhere('image', 'like', '%' . $whereKeywords . '%')
                                            ->orWhere('meta_description', 'like', '%' . $whereKeywords . '%')
                                            ->orWhere('keyword', 'like', '%' . $whereKeywords . '%');
                                    })
                                    ->orderBy('category_id')->orderBy('urutan')->get();
            $souvenirSearchResultTitle = $searchResultTitle . ' - ' . $souvenirList->count() . ' item(s) found';
        }
        $pageObj = array(
            'page_title' => $searchResultTitle . " - Sanjaya Mitra Sejati",
            'meta_description' => "Kipas Promosi Souvenir USB Map Plastik Promosi Flash Disk Percetakan Jakarta",
            'meta_keywords' => "Kipas Promosi, USB Promosi, Map Kancing, Map Plastik, Souvenir Pulpen, Tempat Kartu Nama, Barang Promosi, Souvenir Flash Disk, Jakarta"
        );
        $data = array(
            'pageObj' => $pageObj,
            'searchResultTitle' => $searchResultTitle,
            'printingSearchResultTitle' => $printingSearchResultTitle,
            'souvenirSearchResultTitle' => $souvenirSearchResultTitle,
            'printingList' => $printingList,
            "souvenirList" => $souvenirList
        );
        return View('search.index')->with($data);
    }
}
