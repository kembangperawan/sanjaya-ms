<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Souvenir;
use App\Category;
use App\Printing;
use App\SouvenirImage;

class SouvenirController extends Controller
{
    public function index()
    {
        return $this->loaddata(null);
    }

    public function loaddata($dataparam)
    {
        $categoryList = Category::orderBy("urutan")->get();
        $categoryNewProducts = new Category;
        $categoryNewProducts->category_id = 0;
        $categoryNewProducts->category_name = 'New Products';
        $categoryNewProducts->category_url = 'newproducts';
        $categoryNewProducts->urutan = 0;
        $categoryList->prepend($categoryNewProducts);
        $navigationCategory = $categoryList;
    
        $selectedCategory = null;
        if ($dataparam != null && isset($dataparam['selectedCategory']) && $dataparam['selectedCategory'] != null) {
            $selectedCategory = $dataparam['selectedCategory'];
        } else {
            if (isset($dataparam['souvenir']) && $dataparam['souvenir'] != null) {
                $selectedCategory = Category::where('category_id', $dataparam['souvenir']->category_id)->first();
            } else {
                $selectedCategory = $categoryNewProducts;
            }
        }
        if ($selectedCategory->category_id == 0) {
            $souvenirList = Souvenir::where('status', 2)->where('is_active', 1)->orderBy('category_id')->orderBy('urutan')->get();
        } else {
            $souvenirList = $selectedCategory->souvenirs()->where('is_active', 1)->orderBy('urutan')->get();
        }
        if ($dataparam != null && $dataparam['pageObj'] != null) {
            $pageObj = $dataparam['pageObj'];
        } else {
            $pageObj = array(
                "page_title" => "Barang Promosi Perusahaan Souvenir Promosi - Sanjaya Mitra Sejati",
                "meta_description" => "Kami menjual berbagai macam aneka barang promosi dan souvenir promosi untuk keperluan promosi perusahaan Anda, seperti pulpen, pensil, tumbler, jam, tempat kartu nama, flash disk / USB, dan lainnya.",
                "meta_keywords" => "Barang Promosi , Souvenir Perusahaan , Promosi Perusahaan , Pulpen Promosi , Pulpen Plastik, Tempat kartu nama, Gelas stainless, Gelas Promosi, Tumbler Promosi, Jam Promosi ,Pulpen Souvenir"
            );
        }
        $souvenir = null;
        if ($dataparam != null && isset($dataparam['souvenir']) && $dataparam['souvenir'] != null) {
            $souvenir = $dataparam['souvenir'];
        }
        $souvenirImageList = null;
        if ($dataparam != null && isset($dataparam['souvenirImageList']) && $dataparam['souvenirImageList'] != null) {
            $souvenirImageList = $dataparam['souvenirImageList'];
        }
        $data = array(
            "pageObj" => $pageObj,
            "categoryList" => $categoryList,
            "navigationCategory" => $navigationCategory,
            "souvenirList" => $souvenirList,
            "souvenirCategoryName" => $selectedCategory->category_name,
            "souvenir" => $souvenir,
            "souvenirImageList" => $souvenirImageList
        );
        return view('souvenir.index')->with($data);
    }

    public function getSouvenirByCategory($categoryID, $categoryName)
    {
        if ($categoryID == 0) {
            $selectedCategory = new Category;
            $selectedCategory->category_id = 0;
            $selectedCategory->category_name = 'New Products';
            $selectedCategory->category_url = 'newproducts';
            $selectedCategory->urutan = 0;
        } else {
            $selectedCategory = Category::where("category_id", $categoryID)->first();
            if ($selectedCategory == null) {
                return Redirect('/');
            }
            if (preg_replace('![^a-z0-9]+!i', '-', strtolower($selectedCategory->category_name)) != $categoryName) {
                return Redirect($selectedCategory->link());
            }
        }
        $pageObj = array(
            "page_title" => $selectedCategory->category_name . " - Barang Promosi Perusahaan Souvenir Promosi - Sanjaya Mitra Sejati",
            "meta_description" => "Kami menjual berbagai macam aneka barang promosi dan souvenir promosi untuk keperluan promosi perusahaan Anda, seperti pulpen, pensil, tumbler, jam, tempat kartu nama, flash disk / USB, dan lainnya.",
            "meta_keywords" => "Barang Promosi , Souvenir Perusahaan , Promosi Perusahaan , Pulpen Promosi , Pulpen Plastik, Tempat kartu nama, Gelas stainless, Gelas Promosi, Tumbler Promosi, Jam Promosi ,Pulpen Souvenir"
        );
        $data = array(
            "pageObj" => $pageObj,
            "selectedCategory" => $selectedCategory
        );
        return $this->loaddata($data);
    }

    public function getSouvenirAjax($categoryID)
    {
        if ($categoryID == 0) {
            $souvenirList = Souvenir::where('status', 2)->where('is_active', 1)->orderBy('category_id')->orderBy('urutan')->get();
        } else {
            $souvenirList = Souvenir::where('category_id', $categoryID)->where('is_active', 1)->orderBy('urutan')->get();
        }
        $data = array(
            "souvenirList" => $souvenirList
        );
        return view('souvenir.ajax-souvenir')->with($data);
    }

    public function getSouvenirBySouvenirID($souvenirID, $souvenirName)
    {
        $souvenir = Souvenir::where('souvenir_id', $souvenirID)->first();
        if ($souvenir == null) {
            return Redirect('/souvenir');
        }
        if (preg_replace('![^a-z0-9]+!i', '-', strtolower($souvenir->souvenir_name))!=$souvenirName) {
            return Redirect('/souvenir/'.$souvenir->category_id);
        }
        $souvenirImageList = $souvenir->souvenirImages()->orderBy('rank')->get();
        $pageObj = array(
            "page_title" => $souvenir->souvenir_name,
            "meta_description" => $souvenir->meta_description,
            "meta_keywords" => $souvenir->keyword
        );
        $data = array(
            "pageObj" => $pageObj,
            "souvenir" => $souvenir,
            "souvenirImageList" => $souvenirImageList
        );
        return $this->loaddata($data);
    }
}
