<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Work;
use \Carbon\Carbon;
use Redirect;

class WorkController extends Controller
{
    public function index($state)
    {
        $datenow = Carbon::parse(date("Y-m-d H:i:s"));
        $workList = null;
        $workTitle = "";
        if ($state == 'last') {
            $workList = Work::orderBy('dateline', 'desc')->orderBy('work_id', 'desc')->paginate(4);
            $workTitle = "Last Works";
        } elseif ($state == 'recent') {
            $workList = Work::orderBy('dateline')->orderBy('work_id')->where('dateline', '>', $datenow)->paginate(4);
            $workTitle = "Recent Works";
        } else {
            return Redirect('/works/recent');
        }
        $pageObj = array(
            'page_title' => $workTitle . ' - Sanjaya Mitra Sejati',
            'meta_description' => "Kipas Promosi Souvenir USB Map Plastik Promosi Flash Disk Percetakan Jakarta",
            'meta_keywords' => "Kipas Promosi, USB Promosi, Map Kancing, Map Plastik, Souvenir Pulpen, Tempat Kartu Nama, Barang Promosi, Souvenir Flash Disk, Jakarta"
        );
        $data = array(
            'pageObj' => $pageObj,
            'workTitle' => $workTitle,
            'workList' => $workList,
            'workState' => $state
        );
        return View('work.index')->with($data);
    }
}
