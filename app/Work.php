<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Work extends Model
{
    protected $table = 'work';

    public function imageUrl(){
        return '/assets/images/works/'.$this->image;
    }
    public function dateString(){
        $date = new DateTime($this->dateline);
        return $date->format('d M Y');
    }
}
