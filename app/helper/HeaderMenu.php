<?php

use App\Http\Requests;
use App\Printing;
use App\Category;

function getHeaderMenu()
{
    $menuPrintingList = Printing::where('is_active', 1)->orderBy("rank")->get();
    $categoryList = Category::orderBy("urutan")->get();
    $categoryNewProducts = new Category;
    $categoryNewProducts->category_id = 0;
    $categoryNewProducts->category_name = 'New Products';
    $categoryNewProducts->category_url = 'newproducts';
    $categoryNewProducts->urutan = 0;
    $categoryList->prepend($categoryNewProducts);
    $serverName = URL("/");
    $actualLink = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $arrayPath = explode("/", str_replace($serverName, "", $actualLink));
    $isActive = "";
    if (isset($arrayPath[1])) {
        $isActive = $arrayPath[1];
    } else {
        $isActive = str_replace("/", "", url);
    }
    $dataMenu = array(
        "menuPrintingList" => $menuPrintingList,
        "menuCategoryList" => $categoryList,
        "isActive" => $isActive,
    );
    return $dataMenu;
}
