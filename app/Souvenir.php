<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Souvenir extends Model
{
    protected $table = 'souvenir';

	public function link() {
		return '/souvenir/product/'.$this->souvenir_id.'/'.preg_replace("![^a-z0-9]+!i", "-", strtolower($this->souvenir_name));
	}

	public function imageUrl() {
		return '/assets/images/souvenir/'.$this->image;
	}

	public function souvenirImages() {
		return $this->hasMany('App\SouvenirImage', 'souvenir_id', 'souvenir_id');
	}
}
