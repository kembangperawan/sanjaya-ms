<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = "imagegallery";

    public function imageUrl()
    {
        return '/assets/images/imagegallery/'.$this->URL;
    }
    public function thumbnailUrl()
    {
        return '/assets/images/imagegallerythumbnail/'.$this->URL;
    }
    public function link()
    {
        $urlbuilder = trim(strtolower($this->Title));
        return '/gallery/'.$this->ImageID.'/'.preg_replace("![^a-z0-9]+!i", "-", $urlbuilder);
    }
    public function imgTag()
    {
        $tagArr = explode(',', $this->Tag);
        return $tagArr;
    }
}
