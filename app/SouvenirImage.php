<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SouvenirImage extends Model
{
    protected $table = 'gallery';

	public function imageUrlProduct(){
		return '/assets/images/souvenir/'.$this->gallery_image;
	}
}
