<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    public function link(){
        $urlbuilder = trim(strtolower($this->category_name));
        return '/souvenir/'.$this->category_id. '/'.preg_replace("![^a-z0-9]+!i", "-", $urlbuilder);
    }

    public function imageUrl(){
        return '/assets/images/souvenir/icon_'.$this->category_url.'.png';
    }

    public function souvenirs()
    {
        return $this->hasMany('App\Souvenir', 'category_id', 'category_id');
    }
}
