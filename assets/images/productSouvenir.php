<style type="text/css">
#menuTop{
	position: absolute; 
	right: 10px; 
	bottom: 10px; 
	font-size: 12px;
	z-index:999;
}
div > div#menuTop{
	position:fixed; 
}
</style>
<? /*
Kode Barang Promosi
1 	Pulpen Promosi
2 	USB Promosi
4 	Gadget Aksesoris
5 	Barang Promosi Unik
6 	Tempat Kartu Nama
7 	Gift Set
8	Pulpen Ekslusif
*/?>
<div id="menuTop"><input type='button' name='cancel' value='TOP' class="btn" onClick="goToTop()"></div>
<div id="sticky_navigation_wrapper">
		<p id="kategoriSouvenir">Kategori Souvenir</p>
		<div class="clear"></div>
		<div id="sticky_navigation">
			<div class="demo_container">
				<ul class="tabs">
					<li><a href="#pen" class="selected">Pulpen Promosi</a></li>
					<li><a href="#namecard">Tempat Kartu Nama</a></li>
					<li><a href="#usb">USB / Flash Disk</a></li>
					<li><a href="#tumbler">Tumbler</a></li>
					<li><a href="#gadget">Gadget</a></li>

					
				</ul>
			</div>
		</div>
</div>
<div class="clear"></div>

<div style="display: block;" id="pen" class="tab_content">
<h3>PULPEN PROMOSI</h3>
<div class="border"></div>
	<?php
		$query = mysql_query('SELECT * FROM souvenir WHERE category_id = 1 order by urutan asc');
		while($row = mysql_fetch_array($query))
		{
	?>
		<div class="product productSouvenir">
		<? 
			if($row["status"]==1) echo '';  
			else if($row["status"]==2) echo '<p class="newItems"></p>';  
			else if($row["status"]==3) echo '<p class="bestPrice"></p>';  
			else if($row["status"]==4) echo '<p class="hotItems"></p>';  
		?>
		<p><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><img src="images/souvenir/<?=$row["image"]?>" alt="<?=$row["keyword"]?>"></a></p>
		<h2><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><?=$row["souvenir_name"]?></a></h2>
		</div>
	<?php
		}
	?>
	<div class="product productSouvenir">
	<p><a href="pulpen_grande.php" title="Pulpen Promosi Bahan Plastik Model Terbaru"><img src="images/souvenir/pulpen_promosi_grande_1.jpg" alt="Pulpen grande untuk promosi perusahaan"></a></p>
	<h2><a href="pulpen_grande.php" title="Pulpen Promosi Bahan Plastik Model Terbaru">Pulpen Grande</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="pulpen_candy.php" title="Souvenir pulpen dengan tali model insert paper"><img src="images/souvenir/pulpen_candy_model_insert_paper.jpg" alt="Pulpen promosi dengan tali"></a></p>
	<h2><a href="pulpen_candy.php" title="Pulpen warna warni cetak logo full color">Pulpen Candy</a></h2>
	</div>
<div class="clear"></div>
</div>

<div style="display: block;" id="namecard" class="tab_content">
<h3>TEMPAT KARTU NAMA</h3>
<div class="border"></div>
	<?php
		$query = mysql_query('SELECT * FROM souvenir WHERE category_id = 6 order by urutan asc');
		while($row = mysql_fetch_array($query))
		{
	?>
		<div class="product productSouvenir">
		<? 
			if($row["status"]==1) echo '';  
			else if($row["status"]==2) echo '<p class="newItems"></p>';  
			else if($row["status"]==3) echo '<p class="bestPrice"></p>';  
			else if($row["status"]==4) echo '<p class="hotItems"></p>';  
		?>
		<p><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><img src="images/souvenir/<?=$row["image"]?>" alt="<?=$row["keyword"]?>"></a></p>
		<h2><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><?=$row["souvenir_name"]?></a></h2>
		</div>
	<?php
		}
	?>
	<div class="product productSouvenir">
	<p><a href="tempat_kartu_nama_mln02.php" title="Tempat Kartu Nama Promosi Bahan Kulit"><img src="images/souvenir/tempat_kartu_nama_mln02_1.jpg" alt="Promosi Tempat Kartu Nama Bahan Kulit Business Card Holder"></a></p>
	<h2><a href="tempat_kartu_nama_mln02.php" title="Barang Promosi Name Card Holder Tempat Kartu Nama">Tempat Kartu Nama (MLN 02)</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="tempat_kartu_nama_mln03.php" title="Souvenir Tempat Kartu Nama Bahan Kulit"><img src="images/souvenir/tempat_kartu_nama_mln03_1.jpg" alt="Souvenir Tempat Kartu Nama Bahan Kulit Business Card Holder"></a></p>
	<h2><a href="tempat_kartu_nama_mln03.php" title="Barang Promosi Name Card Holder Tempat Kartu Nama">Tempat Kartu Nama (MLN 03)</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="tempat_kartu_nama_mln04.php" title="Tempat Kartu Nama Bahan Kulit RXC"><img src="images/souvenir/tempat_kartu_nama_mln04_2.jpg" alt="Barang Promosi Tempat Kartu Nama Bahan Kulit Business Card Holder"></a></p>
	<h2><a href="tempat_kartu_nama_mln04.php" title="Barang Promosi Name Card Holder Tempat Kartu Nama">Tempat Kartu Nama (MLN 04)</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="tempat_kartu_nama_mln05.php" title="Tempat Kartu Nama Promosi Bahan Kulit"><img src="images/souvenir/tempat_kartu_nama_mln05_1.jpg" alt="Jual Tempat Kartu Nama Bahan Kulit Business Card Holder"></a></p>
	<h2><a href="tempat_kartu_nama_mln05.php" title="Barang Promosi Name Card Holder Tempat Kartu Nama">Tempat Kartu Nama (MLN 05)</a></h2>
	</div>
	<div class="product productSouvenir">
	<p class="hotItems"></p>
	<p><a href="souvenir_tempat_kartu_nama_b01.php" title="Tempat Kartu Nama Bahan Kulit B01"><img src="images/souvenir/tempat_kartu_nama_B01_4.jpg" alt="Tempat Kartu Nama Bahan Kulit Business Card Holder"></a></p>
	<h2><a href="souvenir_tempat_kartu_nama_b01.php" title="Leather Business Card Holder">Tempat Kartu Nama (B01)</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="souvenir_tempat_kartu_nama_b03.php" title="Tempat Kartu Nama Bahan Kulit Kombinasi Stainless B03"><img src="images/souvenir/tempat_kartunama_stainless_B03_3.jpg" alt="Tempat Kartu Nama Bahan Kulit Business Card Holder"></a></p>
	<h2><a href="souvenir_tempat_kartu_nama_b03.php" title="Leather Business Card Holder">Tempat Kartu Nama (B03)</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="tempat_kartu_nama_b04.php" title="Tempat Kartu Nama Bahan Kulit B04"><img src="images/souvenir/tempat_kartu_nama_b04_1.jpg" alt="Tempat Kartu Nama Bahan Kulit Business Card Holder"></a></p>
	<h2><a href="tempat_kartu_nama_b04.php" title="Leather Business Card Holder">Tempat Kartu Nama (B04)</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="tempat_kartu_nama_b05.php" title="Tempat Kartu Nama Bahan Kulit B05"><img src="images/souvenir/tempat_kartu_nama_b05_1.jpg" alt="Tempat Kartu Nama Bahan Kulit Cokelat"></a></p>
	<h2><a href="tempat_kartu_nama_b05.php" title="Leather Business Card Holder">Tempat Kartu Nama (B05)</a></h2>
	</div>
<div class="clear"></div>
</div>

<div style="display: block;" id="usb" class="tab_content">
<h3>USB PROMOSI</h3>
<div class="border"></div>
	<?php
		$query = mysql_query('SELECT * FROM souvenir WHERE category_id = 2');
		while($row = mysql_fetch_array($query))
		{
	?>
		<div class="product productSouvenir">
		<? 
			if($row["status"]==1) echo '';  
			else if($row["status"]==2) echo '<p class="newItems"></p>';  
			else if($row["status"]==3) echo '<p class="bestPrice"></p>';  
			else if($row["status"]==4) echo '<p class="hotItems"></p>';  
		?>
		<p><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><img src="images/souvenir/<?=$row["image"]?>" alt="<?=$row["keyword"]?>"></a></p>
		<h2><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><?=$row["souvenir_name"]?></a></h2>
		</div>
	<?php
		}
	?>
	<div class="product productSouvenir">
	<p><a href="usb_promosi_plastik_up01.php" title="Usb Flash disk promosi bahan plastik warna silver"><img src="images/souvenir/usb_promosi_usb_plastik_p01.jpg" alt="USB Murah, Flash disk promosi, USB bahan plastik, cetak usb, promosi usb perusahaan" title="Usb Plastik Warna Silver Souvenir Perusahaan"></a></p>
	<h2><a href="usb_promosi_plastik_up01.php" title="Usb Flash disk promosi bahan plastik warna silver">USB Plastik (UP01)</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="usb_promosi_besi_ub01.php" title="Usb Promosi Model Swing Bahan Besi"><img src="images/souvenir/usb_besi_model_swing_b01.jpg" alt="USB Promosi, Souvenir USB, Usb Promosi, Sarung Kulit, Cetak USB, Promosi USB" title="Usb Promosi Model Sarung Kulit Souvenir Perusahaan"></a></p>
	<h2><a href="usb_promosi_besi_ub01.php" title="Usb Promosi Model Swing Bahan Besi">USB Besi Model Swing (UB01)</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="usb_promosi_kulit_uk03.php" title="Usb Promosi Model Sarung Kulit Kode K03"><img src="images/souvenir/usb_promosi_usb_kulit_k03.jpg" alt="Souvenir USB, Usb Promosi, Sarung Kulit, Cetak USB, Promosi USB" title="Usb Promosi Model Sarung Kulit Souvenir Perusahaan"></a></p>
	<h2><a href="usb_promosi_kulit_uk03.php" title="Usb Promosi Model Sarung Kulit Kode K03">USB Kulit (UK03)</a></h2>
	</div>
	<? /*
	<div class="product productSouvenir">
	<p><a href="usb_pulpen.php" title="Pulpen dengan USB Promosi"><img src="images/souvenir/usb_pulpen_promosi_1.jpg" alt="Jual USB Promosi, Pulpen dengan USB, USB Unik, Flash Disk Unik" title="Usb Plastik Warna Silver Souvenir Perusahaan"></a></p>
	<h2><a href="usb_pulpen.php" title="Pulpen dengan USB Promosi">USB Pulpen</a></h2>
	</div>
	*/ ?>
<div class="clear"></div>
</div>

<div style="display: block;" id="tumbler" class="tab_content">
<h3>MUG TUMBLER PROMOSI</h3>
<div class="border"></div>
	<?php
		$query = mysql_query('SELECT * FROM souvenir WHERE category_id = 3 order by urutan asc');
		while($row = mysql_fetch_array($query))
		{
	?>
		<div class="product productSouvenir">
		<? 
			if($row["status"]==1) echo '';  
			else if($row["status"]==2) echo '<p class="newItems"></p>';  
			else if($row["status"]==3) echo '<p class="bestPrice"></p>';  
			else if($row["status"]==4) echo '<p class="hotItems"></p>';  
		?>
		<p><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><img src="images/souvenir/<?=$row["image"]?>" alt="<?=$row["keyword"]?>"></a></p>
		<h2><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><?=$row["souvenir_name"]?></a></h2>
		</div>
	<?php
		}
	?>
	<div class="product productSouvenir">
	<p class="hotItems"></p>
	<p><a href="souvenir_tumbler_plastik.php" title="Tumbler promosi bahan plastik, bisa cetak full color"><img src="images/souvenir/promosi_tumbler_plastik.jpg" alt="Promosi tumbler plastik insert paper cetak foto" title="Tempat air minum untuk promosi perusahaan"></a></p>
	<h2><a href="souvenir_tumbler_plastik.php" title="Cetak logo di tumbler promosi untuk anak-anak dan dewasa">Tumbler Plastik Colorful</a></h2>
	</div>
	<div class="product productSouvenir">
	<p class="bestPrice"></p>
	<p><a href="souvenir_mug_tumbler_promosi_colorful.php" title="Mug tumbler warna merah, hijau, biru"><img src="images/souvenir/mug_tumbler_promosi_stainless_colorful.jpg" alt="Souvenir tumbler mug promosi murah" title="Tumbler mug dengan warna biru, hijau dan merah"></a></p>
	<h2><a href="souvenir_mug_tumbler_promosi_colorful.php" title="Tumbler stainless warna merah, hijau, dan biru untuk promosi perusahaan">Mug Tumbler Stainless</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="souvenir_tumbler_stainless.php" title="Tumbler promosi bahan stainless besi souvenir merchandise perusahaan"><img src="images/souvenir/tumbler_besi_promosi_pt_solusi.jpg" alt="Souvenir tumbler stainless bahan besi" title="Promosi mug bahan besi stainless"></a></p>
	<h2><a href="souvenir_tumbler_stainless.php" title="Tumbler stainless warna merah, hijau, dan biru untuk promosi perusahaan">Tumbler Stainless Gagang Hitam</a></h2>
	</div>
<div class="clear"></div>
</div>


<div style="display: block;" id="gadget" class="tab_content">
<h3>GADGET</h3>
<div class="border"></div>
	<?php
		$query = mysql_query('SELECT * FROM souvenir WHERE category_id = 4 order by urutan asc');
		while($row = mysql_fetch_array($query))
		{
	?>
		<div class="product productSouvenir">
		<? 
			if($row["status"]==1) echo '';  
			else if($row["status"]==2) echo '<p class="newItems"></p>';  
			else if($row["status"]==3) echo '<p class="bestPrice"></p>';  
			else if($row["status"]==4) echo '<p class="hotItems"></p>';  
		?>
		<p><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><img src="images/souvenir/<?=$row["image"]?>" alt="<?=$row["keyword"]?>"></a></p>
		<h2><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><?=$row["souvenir_name"]?></a></h2>
		</div>
	<?php
		}
	?>
<div class="clear"></div>
</div>
<div style="display: block;" id="giftset" class="tab_content">
<h3>GIFT SET (EKSKLUSIF)</h3>
<div class="border"></div>
	<?php
		$query = mysql_query('SELECT * FROM souvenir WHERE category_id = 7 order by urutan asc');
		while($row = mysql_fetch_array($query))
		{
	?>
		<div class="product productSouvenir">
		<? 
			if($row["status"]==1) echo '';  
			else if($row["status"]==2) echo '<p class="newItems"></p>';  
			else if($row["status"]==3) echo '<p class="bestPrice"></p>';  
			else if($row["status"]==4) echo '<p class="hotItems"></p>';  
		?>
		<p><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><img src="images/souvenir/<?=$row["image"]?>" alt="<?=$row["keyword"]?>"></a></p>
		<h2><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><?=$row["souvenir_name"]?></a></h2>
		</div>
	<?php
		}
	?>
	<div class="product productSouvenir">
	<p><a href="giftset_mgs005.php" title="Souvenir Eksklusif Gift Set Perusahaan Merchandise Kantor"><img src="images/souvenir/Barang Promosi Eksklusif Gift Set Souvenir_MGS005_1.jpg" alt="Barang Promosi Eksklusif, Souvenir Promosi, Souvenir Mewah, Gift Set Perusahaan, Souvenir Ekslkusif, Souvenir Bank, Merchandise Kantor" title="Gift Set Souvenir Perusahaan Eksklusif Mewah Satu Set Souvenir Merchandise Kantor"></a></p>
	<h2><a href="giftset_mgs005.php" title="Souvenir Eksklusif Gift Set Perusahaan Merchandise Kantor">Gift Set 005</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="giftset_mgs006.php" title="Souvenir Eksklusif Gift Set Perusahaan Merchandise Kantor"><img src="images/souvenir/Barang Promosi Eksklusif Gift Set Souvenir_MGS006_1.jpg" alt="Barang Promosi Eksklusif, Souvenir Promosi, Souvenir Mewah, Gift Set Perusahaan, Souvenir Ekslkusif, Souvenir Bank, Merchandise Kantor" title="Gift Set Souvenir Perusahaan Eksklusif Mewah Satu Set Souvenir Merchandise Kantor"></a></p>
	<h2><a href="giftset_mgs006.php" title="Souvenir Eksklusif Gift Set Perusahaan Merchandise Kantor">Gift Set 006</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="giftset_mgs007.php" title="Souvenir Eksklusif Gift Set Perusahaan Merchandise Kantor"><img src="images/souvenir/Barang Promosi Eksklusif Gift Set Souvenir_MGS007_3.jpg" alt="Barang Promosi Eksklusif, Souvenir Promosi, Souvenir Mewah, Gift Set Perusahaan, Souvenir Ekslkusif, Souvenir Bank, Merchandise Kantor" title="Gift Set Souvenir Perusahaan Eksklusif Mewah Satu Set Souvenir Merchandise Kantor"></a></p>
	<h2><a href="giftset_mgs007.php" title="Souvenir Eksklusif Gift Set Perusahaan Merchandise Kantor">Gift Set 007</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="giftset_mgs008.php" title="Souvenir Eksklusif Gift Set Perusahaan Merchandise Kantor"><img src="images/souvenir/Barang Promosi Eksklusif Gift Set Souvenir_MGS008_1.jpg" alt="Barang Promosi Eksklusif, Souvenir Promosi, Souvenir Mewah, Gift Set Perusahaan, Souvenir Ekslkusif, Souvenir Bank, Merchandise Kantor" title="Gift Set Souvenir Perusahaan Eksklusif Mewah Satu Set Souvenir Merchandise Kantor"></a></p>
	<h2><a href="giftset_mgs008.php" title="Souvenir Eksklusif Gift Set Perusahaan Merchandise Kantor">Gift Set 008</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="giftset_mgs009.php" title="Souvenir Eksklusif Gift Set Perusahaan Merchandise Kantor"><img src="images/souvenir/Barang Promosi Eksklusif Gift Set Souvenir_MGS009_1.jpg" alt="Barang Promosi Eksklusif, Souvenir Promosi, Souvenir Mewah, Gift Set Perusahaan, Souvenir Ekslkusif, Souvenir Bank, Merchandise Kantor" title="Gift Set Souvenir Perusahaan Eksklusif Mewah Satu Set Souvenir Merchandise Kantor"></a></p>
	<h2><a href="giftset_mgs009.php" title="Souvenir Eksklusif Gift Set Perusahaan Merchandise Kantor">Gift Set 009</a></h2>
	</div>
<div class="clear"></div>
</div>

<div style="display: block;" id="unik" class="tab_content">
<h3>BARANG PROMOSI UNIK</h3>
<div class="border"></div>
	<?php
		$query = mysql_query('SELECT * FROM souvenir WHERE category_id = 4 order by urutan asc');
		while($row = mysql_fetch_array($query))
		{
	?>
		<div class="product productSouvenir">
		<? 
			if($row["status"]==1) echo '';  
			else if($row["status"]==2) echo '<p class="newItems"></p>';  
			else if($row["status"]==3) echo '<p class="bestPrice"></p>';  
			else if($row["status"]==4) echo '<p class="hotItems"></p>';  
		?>
		<p><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><img src="images/souvenir/<?=$row["image"]?>" alt="<?=$row["keyword"]?>"></a></p>
		<h2><a href="barang_promosi.php?souvenir=<?=str_replace(" ", "_", $row["souvenir_name"])?>&id=<?=$row["souvenir_id"]?>" title="<?=$row["meta_description"]?>"><?=$row["souvenir_name"]?></a></h2>
		</div>
	<?php
		}
	?>
	<div class="product productSouvenir">
	<p><a href="souvenir_tempat_pulpen.php" title="Pulpen Promosi Barang Promosi Cetak di Pulpen Model Pulpen Promosi"><img src="images/souvenir/promosi_tempat_pulpen_jam_kalender.jpg" alt="Barang Promosi Jam, Tempat Pulpen, Kalender, dan Pengukur suhu" title="Souvenir Jam, Tempat Pulpen, dan Kalender"></a></p>
	<h2><a href="souvenir_tempat_pulpen.php" title="Souvenir Jam, Tempat Pulpen, dan Kalender">Colorful Calendar & Pen Holder</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="souvenir_notes_tempat_pulpen.php" title="Notes 3 warna dan tempat pulpen"><img src="images/souvenir/notes_tempat_pulpen_1.jpg" alt="Barang Promosi Notes dan Tempat Pulpen Kulit" title="Catatan Notes Post It dan Tempat Pulpen"></a></p>
	<h2><a href="souvenir_notes_tempat_pulpen.php" title="Tempat Pulpen dan notes dengan kulit">Notes & Pen Holder</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="souvenir_pulpen_donat.php" title="Pulpe Unik bentuk donat"><img src="images/souvenir/pulpen_unik_donat_1.jpg" alt="Barang Promosi Pulpen Donat Magnet" title="Pulpen unik bentuk donat warna silver dengan magnet"></a></p>
	<h2><a href="souvenir_pulpen_donat.php" title="Pulpen Silver Donat">Pulpen Donat</a></h2>
	</div>
	<div class="product productSouvenir">
	<p><a href="pulpen_3_fungsi.php" title="Pulpen promosi dengan 3 fungsi sebagai pulpen , pointer dan senter"><img src="images/souvenir/pulpen_pointer_senter_2.jpg" alt="Pulpen 3 Fungsi, Fungsi Senter, Fungsi Pointer, Souvenir Murah" title="Souvenir Jam, Tempat Pulpen, dan Kalender"></a></p>
	<h2><a href="pulpen_3_fungsi.php" title="Pulpen promosi dengan 3 fungsi sebagai pulpen , pointer dan senter">Pulpen 3 Fungsi</a></h2>
	</div>
<div class="clear"></div>
</div>

