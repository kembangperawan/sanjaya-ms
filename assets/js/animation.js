$(document).ready(function(){	
    $(document).pngFix(); 
	
	$("#slider").easySlider({
		controlsBefore:	'<p id="controls">',
		controlsAfter:	'</p>',
		auto: true, 
		continuous: true
	});	

	$("#sliderTumbler").easySlider({
		controlsBefore:	'<p id="controls">',
		controlsAfter:	'</p>',
		auto: true, 
		continuous: true
	});
	
	$("ul.thumbGalleryProduct li a").click(function() {
		var mainImage = $(this).attr("href"); //Find Image Name
		var mainTitle = $(this).attr("title"); //Find Image Name
		$(".galleryProduct img").attr({ src: mainImage });
		$(".galleryProduct p").text(mainTitle);
		$(".active").removeClass("active");
		$(this).parent().addClass("active");
		return false;		
	});
	
	$("ul.thumbGallerySouvenir li a").click(function() {
		var mainImage = $(this).attr("href"); //Find Image Name
		var mainTitle = $(this).attr("title"); //Find Image Name
		$(".gallerySouvenir img").attr({ src: mainImage });
		$(".gallerySouvenir p").text(mainTitle);
		$(".active").removeClass("active");
		$(this).parent().addClass("active");

		return false;		
	});

	
	/*
	$('.slideShow').innerfade({
		animationtype: 'fade',
		speed: 1000,
		timeout: 5000,
		//type: 'random',
		containerheight: '1em'
	});
	
	$(".faq h3").eq(0).addClass("active");
	$(".faq p").eq(0).show();

	$(".faq h3").click(function(){
		$(this).next("p").slideToggle("medium")
		//.siblings("p:visible").slideUp("medium");
		$(this).toggleClass("active");
		//$(this).siblings("h3").removeClass("active");
	});

	//Tab Souvenir
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("seleced").show(); //Activate first tab
	$("ul.tabs li:first").addClass("seleced").show(); //Activate first tab
	
			
	//On Click Event
	$("ul.tabs li").click(function() {
	$("ul.tabs li a").removeClass("selected"); //Remove any "active" class
	$(this).find('a').addClass("selected"); //Add "active" class to selected tab
	$(".tab_content").hide(); //Hide all tab content
	var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
	$(activeTab).fadeIn(); //Fade in the active content
	return false;
	});
	
	
	$( ".product" ).hover(
	function() {
		$(this).find('h2 a').addClass("active");
	}, function() {
		$( this ).find('h2 a').removeClass( "active" );
	});
	*/
});

function goToTop(){
     $('html, body').animate( { scrollTop: 0 },'slow' );
}
/*
$(function() {
	// grab the initial top offset of the navigation 
	var sticky_navigation_offset_top = $('#sticky_navigation').offset().top;
	
	// our function that decides weather the navigation bar should have "fixed" css position or not.
	var sticky_navigation = function(){
		var scroll_top = $(window).scrollTop(); // our current vertical position from the top
		
		// if we've scrolled more than the navigation, change its position to fixed to stick to top, otherwise change it back to relative
		if (scroll_top > sticky_navigation_offset_top) { 
			$('#sticky_navigation').css({ 'visibility': 'visible'});
			$('#sticky_navigation').css({ 'position': 'fixed', 'top':0, 'left':0,'padding-left':30 });
		} else {
			$('#sticky_navigation').css({ 'position': 'relative','padding-left':0 }); 
		}   
	};
	// run our function on load
	sticky_navigation();
	// and run it again every time you scroll
	$(window).scroll(function() {
		 sticky_navigation();
	});
});


$(function() {
	var zIndexNumber = 10000000;
	$('div').each(function() {
		$(this).css('zIndex', zIndexNumber);
		zIndexNumber -= 10;
	});
});
*/