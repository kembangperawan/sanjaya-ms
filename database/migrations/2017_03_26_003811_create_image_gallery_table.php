<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('imagegallery')) {
            Schema::create('imagegallery', function (Blueprint $table) {
            $table->increments('ImageID');
            $table->string('Title', 255);
            $table->string('Description');
            $table->string('URL', 255);
            $table->string('Tag', 255);
            $table->string('CreatorID', 100);
            $table->datetime('CreatorDateTime');
            $table->string('EditorID', 100);
            $table->datetime('EditorDateTime');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imagegallery');
    }
}
