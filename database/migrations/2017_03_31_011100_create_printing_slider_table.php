<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintingSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('printing_slider')) {
            Schema::create('printing_slider', function (Blueprint $table) {
                $table->increments('printing_slider_id');
                $table->integer('printing_id');
                $table->string('printing_slider_path', 255);
                $table->text('printing_slider_description')->nullable();
                $table->integer('type');
                $table->boolean('isactive')->default(false);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('printing_slider');
    }
}
