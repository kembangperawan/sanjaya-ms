<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    include 'templateLayout.php';
    $err = '';
if (isset($_GET['err']) && $_GET['err'] != '') {
    $err = $_GET['err'];
}
$query = mysql_query('select max(urutan) + 1 as next_rank from category;');
    $row = mysql_fetch_array($query);
if (mysql_num_rows($query)==0) {
    header("Location:home.php");
    exit;
} else {
    $next_rank = $row["next_rank"];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php getHead('Sanjaya MS - Add Souvenir Category'); ?>
    <style type="text/css">
    #err{
        background:#da0909;     
        border:1px solid #a6a6a6;       
        color:#fff; 
        font-size:12px; 
        font-weight:bold;
        margin-bottom:10px;         
        text-align:center;  
    }
    </style>
</head>

<body>
<div id="contain">
    <? getHeader(); ?>
    <div id="container">
        <div id="content">
            <h1>Add Souvenir Category</h1>
            <div class="border"></div>
            <div id="loading" style="display:none;text-align:center"><img src="images/ajax.gif" alt="loading..."/></div>
            <?php echo $err != '' ? '<div id="err">'.$err.'</div>' : '' ?>
            <form action="addSouvenirCategoryCheck.php" method="post" name="formAddWork" id="formAddWork" enctype="multipart/form-data">
                <label for="txt_name">Name :</label> <input type="text" name="txt_name" class="textbox" id="txt_name" size="50" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="file_upload">Image :</label> <input name="file_upload" id="file_upload" type="file" size="14" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="txt_rank">Rank :</label> <input type="text" name="txt_rank" class="textbox" id="txt_rank" size="5" value="<?=$next_rank?>" />
                <p class="form_clear"></p>
                <input type='button' name='cancel' value='Kembali' class="btn" onClick="backHome()">
                <input type="submit" name="submit" value="Simpan" class="btn" id="submitForm"/>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function backHome()
    {
        window.location.href="souvenirCategory.php";
    }
</script>
</body>
</html>
