<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
    
    $id = $_REQUEST["id"];
    
if (isset($_POST["submit"])) {
    $souvenir = $_REQUEST["txt_souvenir"];
    if ($HTTP_POST_FILES['file_upload']['name'] != "" && $HTTP_POST_FILES['file_upload']['type'] != "image/jpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/pjpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/gif") {
        header("Location:addSouvenirGallery.php?id=".$id ."&err=Format foto harus dalam format JPEG atau GIF");
        exit;
    }
    if ($_FILES['file_upload']['tmp_name'] != '') {
        $imgName   = $_FILES['file_upload']['name'];
        $tmpName   = $_FILES['file_upload']['tmp_name'];
        $titleName = str_replace(" ", "_", $souvenir);
                    
        $ext = strrchr($imgName, ".");
        
        $newName = $titleName ."_". time() . strtolower($ext);
        $imgPath = SOUVENIR_IMG_DIR . $newName;
        $result = createThumbnail($tmpName, $imgPath, SOUVENIR_GALLERY_WIDTH);
        if (!$result) {
            echo "Error uploading file";
            exit;
        }
    } else {
        header("location:addSouvenirGallery.php?id=".$id ."&err=Tidak ada file yang diupload");
        exit;
    }
    mysql_query("insert into `gallery` (`souvenir_id`, `gallery_image` ,`gallery_keyword` ,`rank`)
								    values ('".$id."', '".$newName."', '".$_POST["txt_keterangan"]."', '".$_POST["txt_rank"]."')")
    or die("<meta http-equiv='refresh' content='0;URL=addSouvenirGallery.php?err=Gagal memasukkan data.' >");
    header("location:addSouvenirGallery.php?id=".$id ."");
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include "title.php"; ?>
<link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/sms.css?d=201706150056">
<link rel="stylesheet" type="text/css" href="css/datepick.css">
<link rel="stylesheet" href="docs/style.css" type="text/css">
<script type="text/javascript" src="js/datepick.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script language="javascript" type="text/javascript">
$(function() {
    $('a.ajaxMenu').click(function() {
        $('#ajaxContent').html('<p class="center"><img src="images/loadingLightbox.gif"/><br><i>Loading...</i><p>');
        var url = $(this).attr('href');
        $('#ajaxContent').load(url);
        
        return false;
    });
    $('.err').click(function() {
        $(this).fadeOut('slow');
        return false;
    });
});
</script>

</head>

<body>
<div id="contain">
    <? include("header.php"); ?>
    <div id="container">
        <div id="content">
            <?php $err = isset($_REQUEST["err"]) ? $_REQUEST["err"] : "" ;
            if ($err) {
                echo '<p class="err">'.$err.'</p>';
            } ?>
            <div id="ajaxContent">
            <?php
                $res = mysql_query("select * from souvenir where souvenir_id = '".$id."'");
                $row = mysql_fetch_array($res);

            if (!isset($row["souvenir_name"])) {
                echo "<meta http-equiv='refresh' content='0;URL=home.php'>";
                exit;
            }
            ?>
            <h1><?php echo $row["souvenir_name"];?></h1>
            <div style="position:absolute; margin-left:500px; margin-top:-30px;"><img src="../assets/images/souvenir/<?php echo $row["image"];?>" width="230" style="background:grey;padding:5px; "/></div>
            <form action="" method="post" name="form1" enctype="multipart/form-data">
            <label for="txt_judul"></label> 
            
            <p class="form_clear"></p>
            <label for="txt_souvenir">Judul Berita :</label> <input type="text" name="txt_souvenir" class="textbox" id="txt_souvenir" size="50" value="<?php echo $row["souvenir_name"];?>"/><p class="form_clear"></p>
            <label for="file_upload">Upload Foto :</label><input name="file_upload" id="file_upload" type="file" size="20" /><p class="form_clear"></p>
            <label for="txt_keterangan">Keterangan Foto :</label> <input type="text" name="txt_keterangan" class="textbox" id="txt_keterangan" size="50" /><p class="form_clear"></p>
            <label for="txt_rank">Rank :</label> <input type="text" name="txt_rank" class="textbox" id="txt_rank" size="5" /><p class="form_clear"></p>
            <label></label>
            <input type="submit" name="submit" value="Simpan" class="buttonsubmit" onClick="if(cek())return true;else return false;"/>
            <input type='button' name='cancel' value='Kembali' class="buttonsubmit" onClick="javascript: history.back(1)">
            </form> 
            <h5>Gallery Photo</h5>
            <form action="addSouvenirGalleryCheck.php" method="post" name="form2">
            <div>
            <?php
                $x = 0;
                $res = mysql_query("select * from gallery where souvenir_id = '".$id."' order by rank");
            while ($row = mysql_fetch_array($res)) {
                $x++;
                echo "<div class='product productSouvenir'>";
                echo "<p><img src='../assets/images/souvenir/".$row["gallery_image"]."'  border='1' width='270'></p>";
                echo "<input type='text' name='txt_keterangan".$x."' class='textbox' id='txt_keterangan".$x."' size='30' value='".$row["gallery_keyword"]."' />";
                echo "<input type='text' name='txt_rank".$x."' class='textbox' id='txt_rank".$x."' size='5' value='".$row["rank"]."' />";
                echo " ";
                echo "<a href='delete_gallery.php?deleteId=".$row["gallery_id"]."&gallery=".$id."' onClick='if(confirm(\"Apakah Anda ingin menghapus gambar ini?\"))return true; else return false;'><img src='../assets/images/deleteForm.gif' border='0'></a>";
                echo " ";
                echo "<input type='hidden' name='editId".$x."' id='editId".$x."' value='".$row["gallery_id"]."'>";
                echo '<input type="image" class="clear" src="../assets/images/checkForm.gif" name="edit" value="Edit" onClick="if(confirm(\'Apakah Anda ingin mengubah keterangan foto ini?\'))return true; else return false;"/>';
                echo "</div>";
            }
                echo "<input type='hidden' name='sumx' id='sumx' value='".$x."'>";
                echo "<input type='hidden' name='souvenirId' id='souvenirId' value='".$id."'>";
            if (mysql_num_rows($res)==0) {
                echo "<p class='alert'>Untuk saat ini galeri foto belum ada.</p>";
            }
            ?>
            </div>
            </form> 
            <div class="clear"></div>
            </div>
        </div>
    </div>
</div>

        
</body>
</html>
