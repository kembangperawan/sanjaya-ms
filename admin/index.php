<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include "title.php"; ?>
<link rel="stylesheet" type="text/css" href="css/css.css?d=201703211830">
<script type="text/javascript" src="js/jquery.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	$().ajaxStart(function() {
		$('#loading').show();
		$('#errLogin').hide();
	}).ajaxStop(function() {
		$('#loading').hide();
		$('#errLogin').fadeIn('slow');
	});

	$('#loginform').submit(function() {
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data) {
				if(data == "sukses")
				{
				   	$('#errLogin').html('Logging in.....');
					setTimeout('document.location = \'home.php\'',1000);
				}
				else
				$('#errLogin').html(data);
			}
		})
		return false;
	});
})
</script>

</head>

<body>
<div id="contain">
	<div id="container">
	<div id="headerLogin"><img src="../assets/images/logo_sms.png" /></div>
	<div id="login">
		<div id="loading" style="display:none;text-align:center"><img src="images/ajax.gif" alt="loading..."/></div>
		<? 
			$err = "";
			if(isset($_REQUEST["err"])) 
			{
				$err = $_REQUEST["err"];
			} 
			if($err == "PX0Bk28Jy")
			{
				echo '<div id="errLogin">Silahkan Login terlebih dahulu</div>';
			}
			else
			{
				echo '<div id="errLogin" style="display:none;"></div>'; 
			}
		?>
		<form name="loginform" id="loginform" action="loginCheck.php" method="post">
		<input type="hidden" name="cekJavascript" id="cekJavascript" value="false" />
		<p>
			<label>Username<br />
			<input type="text" name="txtUser" id="txtUser" class="input" value="" size="20"/></label>
		</p>
		<p style="margin-top:15px; ">
			<label>Password<br />
			<input type="password" name="txtPass" id="txtPass" class="input" value="" size="20"/></label>
		</p>
		<p style="text-align:right;margin-top:5px;">
			<input type="submit" value="Log In" name="submit" class="btnLogin"/>
		</p>
		</form>
	</div>
</div>
</div>
<script language="javascript" type="text/javascript">document.getElementById('cekJavascript').value="true";</script>
</body>
</html>
