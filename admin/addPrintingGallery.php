<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    include 'templateLayout.php';
    require_once 'library/config.php';
    require_once 'library/functions.php';
    $err = '';
if (isset($_GET['err']) && $_GET['err'] != '') {
    $err = $_GET['err'];
}
    $id = '';
if (isset($_GET['id']) && $_GET['id'] != '') {
    $id = $_GET['id'];
}
    // if ($id == '') {
    // 	header("location:addPrintingGallery.php");
    // }
    $res = mysql_query("select * from printing where printing_id = '".$id."'");
        $row = mysql_fetch_array($res);
if (isset($_POST["submit"])) {
    $filename = str_replace(" ", "_", basename($_FILES["file_upload"]["name"]));
    $url = IMAGEGALLERY_IMG_DIR . $filename;
    $imageFileType = strtolower(pathinfo($url, PATHINFO_EXTENSION));
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
        header("Location:addPrintingGallery.php?id=".$id ."&err=Format foto harus dalam format JPEG atau GIF");
        exit;
    }
    if ($_FILES['file_upload']['tmp_name'] != '') {
        $img_name   = $_FILES['file_upload']['name'];
        $tmp_name   = $_FILES['file_upload']['tmp_name'];
        $title_name = str_replace(" ", "_", $row['product_printing']);
                
        $ext = strrchr($img_name, ".");

        $printing_slider_image = $title_name ."_". time() . strtolower($ext);
        $img_path = PRINTING_IMG_DIR . $printing_slider_image;
        if ($_POST["ddl_printing_slider_type"] == 1) {
            $result = createThumbnail($tmp_name, $img_path, PRINTING_SLIDE_WIDTH);
        } else {
            $result = createThumbnail($tmp_name, $img_path, PRINTING_WIDTH);
        }
                
        if (!$result) {
            echo "Error uploading file";
            exit;
        }
    } else {
        header("location:addPrintingGallery.php?id=".$id ."&err=Tidak ada file yang diupload");
        exit;
    }
    if (isset($_POST["chk_printing_slider_isactive"])) {
        $isactive = 1;
    } else {
        $isactive = 0;
    }
    mysql_query("insert into `printing_slider` (`printing_id`, `printing_slider_path`, `printing_slider_description`, `isactive`, `type`, `rank`)
								    values ('".$id."', '".$printing_slider_image."', '".$_POST["txt_printing_slider_description"]."', '".$isactive."', '".$_POST["ddl_printing_slider_type"]."', '".$_POST["txt_rank"]."')")
    or die(mysql_error());
    header("location:addPrintingGallery.php?id=".$id ."");
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php getHead('Sanjaya MS - Add Printing'); ?>
    <style type="text/css">
    #err{
        background:#da0909;     
        border:1px solid #a6a6a6;       
        color:#fff; 
        font-size:12px; 
        font-weight:bold;
        margin-bottom:10px;         
        text-align:center;  
    }
    </style>
</head>

<body>
<div id="contain">
    <? getHeader(); ?>
    <div id="container">
        <div id="content">
            <?php
                

                // if(!isset($row["product_printing"]))
                // {
                // 	echo "<meta http-equiv='refresh' content='0;URL=home.php'>";
                //  exit;
                // }
            ?>
            <h1><?php echo $row["product_printing"];?></h1>
            <?php echo $err != '' ? '<div id="err">'.$err.'</div>' : '' ?>
            <div style="position:absolute; margin-left:677px; margin-top:-30px;"><img src="../assets/images/<?php echo $row["product_image"];?>" width="230" style="background:grey;padding:5px; "/></div>
            <form action="" method="post" onsubmit="return formcheck();" name="form1" enctype="multipart/form-data">
                <label for="txt_judul"></label> 
                
                <p class="form_clear"></p>
                <label for="txt_product_printing">Product Printing :</label> <input type="text" name="txt_product_printing" readonly="readonly" class="textbox" id="txt_product_printing" size="55%" value="<?php echo $row["product_printing"];?>"/><p class="form_clear"></p>
                <label for="file_upload">Slider Image :</label><input name="file_upload" id="file_upload" type="file" size="20" /><p class="form_clear"></p>
                <label for="txt_printing_slider_description">Description :</label>
                <input type="text" name="txt_printing_slider_description" class="textbox" id="txt_printing_slider_description" size="55%" value="<?php echo (isset($row["printing_slider_description"]) ? $row["printing_slider_description"] : "");?>" /><p class="form_clear"></p>
                <label for="ddl_printing_slider_type">Type :</label>
                <select name="ddl_printing_slider_type" id="ddl_printing_slider_type">
                    <?php
                    if (isset($row["type"])) {
                        if ($row["type"] == 1) {
                            echo '<option value ="' . 1 . '" selected>Top banner</option><option value ="' . 2 . '">Side gallery</option>';
                        } else {
                            echo '<option value ="' . 1 . '">Top banner</option><option value ="' . 2 . '" selected>Side gallery</option>';
                        }
                    } else {
                        echo '<option value ="' . 1 . '" selected>Top banner</option><option value ="' . 2 . '">Side gallery</option>';
                    }
                    ?>
                </select><p class="form_clear"></p>
                <label for="chk_printing_slider_isactive">Active :</label>
                <input type="checkbox" name="chk_printing_slider_isactive" id="chk_printing_slider_isactive" style="margin: 12px;" checked><p class="form_clear"></p>
                <label for="txt_rank">Rank :</label>
                <input type="text" name="txt_rank" id="txt_rank" size="5" />
                <p class="form_clear"></p>
                <input type="submit" name="submit" value="Simpan" class="buttonsubmit" onClick="if(cek())return true;else return false;"/>
                <input type='button' name='cancel' value='Kembali' class="buttonsubmit" onClick="javascript: history.back(1)">
            </form> 
            <h5>Gallery Photo</h5>
            <form action="addPrintingGalleryCheck.php" method="post" name="form2">
                <div>
                <?php
                    $x = 0;
                    $res = mysql_query("select * from printing_slider where printing_id = '".$id."' order by type, rank");
                while ($row = mysql_fetch_array($res)) {
                    $x++;
                    echo "<div class='product productPrinting'>";
                    echo "<p><img src='../assets/images/".$row["printing_slider_path"]."'  border='1' width='270'></p>";
                    echo "<input type='text' name='txt_printing_slider_description".$x."' class='textbox' id='txt_printing_slider_description".$x."' style='width: 93%;' value='".$row["printing_slider_description"]."' />";
                    echo "<select name='ddl_printing_slider_type".$x."' id='ddl_printing_slider_type".$x."'>";
                    if (isset($row["type"])) {
                        if ($row["type"] == 2) {
                            echo '<option value ="' . 1 . '">Top banner</option><option value ="' . 2 . '" selected>Side gallery</option>';
                        } else {
                            echo '<option value ="' . 1 . '" selected>Top banner</option><option value ="' . 2 . '">Side gallery</option>';
                        }
                    } else {
                        echo '<option value ="' . 1 . '" selected>Top banner</option><option value ="' . 2 . '">Side gallery</option>';
                    }
                    echo "</select><br>";
                    if (isset($row["isactive"])) {
                        if ($row["isactive"] == 1) {
                            echo "<input type='checkbox' name='chk_printing_slider_isactive".$x."' id='chk_printing_slider_isactive".$x."' checked> Active<br>";
                        } else {
                            echo "<input type='checkbox' name='chk_printing_slider_isactive".$x."' id='chk_printing_slider_isactive".$x."'> Active<br>";
                        }
                    } else {
                        echo "<input type='checkbox' name='chk_printing_slider_isactive".$x."' id='chk_printing_slider_isactive".$x."'> Active<br>";
                    }
                    echo " ";
                    echo "Rank <input type='text' name='txt_rank".$x."' id='txt_rank".$x."' size='5' value='".$row["rank"]."' /><br />";
                    echo "<a href='deletePrintingSlider.php?delete_id=".$row["printing_slider_id"]."&id=".$row["printing_id"]."' onClick='if(confirm(\"Apakah Anda ingin menghapus gambar ini?\"))return true; else return false;'><img src='../assets/images/deleteForm.gif' border='0'></a>";
                    echo " ";
                    echo "<input type='hidden' name='editId".$x."' id='editId".$x."' value='".$row["printing_slider_id"]."'>";
                    echo '<input type="image" class="clear" src="../assets/images/checkForm.gif" name="edit" value="Edit" onClick="if(confirm(\'Apakah Anda ingin mengubah keterangan foto ini?\'))return true; else return false;"/>';
                    echo "</div>";
                }
                    echo "<input type='hidden' name='sumx' id='sumx' value='".$x."'>";
                    echo "<input type='hidden' name='printing_id' id='printing_id' value='".$id."'>";
                if (mysql_num_rows($res)==0) {
                    echo "<p class='alert'>Untuk saat ini galeri foto belum ada.</p>";
                }
                ?>
                </div>
            </form>
            <div class="clear"></div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    function formcheck() {
        var txt_printing_slider_description = $('#txt_printing_slider_description').val();
        if (txt_printing_slider_description == '') {
            alert('Description harus diisi.');
            return false;
        }
        return true;
    }
</script>
</html>
