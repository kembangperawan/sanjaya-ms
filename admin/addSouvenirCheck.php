<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
    
function getPost($name)
{
    if (isset($_POST[$name])) {
        return (get_magic_quotes_gpc() ? $_POST[$name] : addslashes($_POST[$name]));
    } else {
        return false;
    }
}

    $souvenir = trim(getPost('txt_souvenir'));
    $category = trim(getPost('txt_category'));
    $keyword = trim(getPost('txt_keyword'));
    $meta_description = trim(getPost('txt_meta_description'));
    $description = trim(getPost('txt_description'));
    $rank = trim(getPost('txt_rank'));
if (!isset($rank)) {
    $rank = 0;
}
    $is_active = trim(getPost('chk_is_active'));
if ($is_active == '') {
    $is_active = 0;
}
    
if ($souvenir && $category) {
    if ($HTTP_POST_FILES['file_upload']['name'] != "" && $HTTP_POST_FILES['file_upload']['type'] != "image/jpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/pjpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/gif") {
        $err = 'Format foto harus dalam format JPEG atau GIF';
    }
    if (!$err) {
        if ($_FILES['file_upload']['tmp_name'] != '') {
            $imgName   = $_FILES['file_upload']['name'];
            $tmpName   = $_FILES['file_upload']['tmp_name'];
            $titleName = strtolower(str_replace(" ", "-", str_replace("/", "-", str_replace(".", "-", $souvenir))));
                    
            $ext = strrchr($imgName, ".");
        
            $newName = $titleName ."-". time() . strtolower($ext);
            $imgPath = SOUVENIR_IMG_DIR . $newName;
                $result = createThumbnail($tmpName, $imgPath, SOUVENIR_WIDTH);
            if (!$result) {
                echo "Error uploading file";
                exit;
            }
        } else {
            $newName = "no_image.jpg";
        }

        $query = "INSERT INTO `souvenir` (`souvenir_name`, `category_id`, `description`, `image`, `meta_description`, `keyword`, `urutan`, `is_active`)
								  VALUES ('".$souvenir."', '".$category."', '".$description."', '".$newName."', '".$meta_description."', '".$keyword."', '".$rank."', '".$is_active."')";
        $result = mysql_query($query) or die(mysql_error());
        $err = 'sukses';
    }
} else {
    $err = 'Masukkan semua data dengan benar!';
}
    header("Location:souvenir.php?err=".$err."");
    exit;
