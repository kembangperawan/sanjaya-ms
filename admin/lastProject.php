<?	
	if(session_id() == '') {
		session_start();
	}
	include 'connect.php';
	include "security.php";
	include 'templateLayout.php';

	$rowperpage = 5;
	$pageNum = 1;
	$page = (isset($_REQUEST["page"])?$_REQUEST["page"]:"");
	
	if(isset($_GET['page']))
	{
		$pageNum = $_GET['page'];
	}
	$offset = ($pageNum - 1) * $rowperpage;
	$num = $offset;
	
	
	$query   = 'SELECT count(work_id) AS numrows FROM work WHERE dateline < NOW() ORDER BY dateline';
	$result  = mysql_query($query) or die('Error, query failed');
	$row     = mysql_fetch_array($result, MYSQL_ASSOC);
	$numrows = $row['numrows'];
	$maxPage = ceil($numrows/$rowperpage);
	
	if(isset($_GET['page']))
	{
		if(!is_numeric($page) || $page > $maxPage || $page < 1)
		{
			header("location:vehicle.php");
			exit;
		}
	}

	$url_ajax_paging = 'lastProject.php';
	$nav = '';
	$first = '';
	$last = '';
	if($maxPage != 1)
	{
		for($page = 1; $page <= $maxPage; $page++)
		{
			if ($page == $pageNum)
			{
				$nav .= " [$page] ";
			}
			else
			{
				$nav .= '<a href="'.$url_ajax_paging.'?page='.$page.'" class="ajaxMenu"> '.$page.' </a>';
			}		
		}
	}

	if ($pageNum > 1)
	{
		$page  = $pageNum - 1;
		$prev ='<span><a href="'.$url_ajax_paging.'?page='.$page.'" class="ajaxMenu"> < Prev </a></span>';
	} 
	else
	{
		$first ='<span></span>';
		$prev ='<span></span>';
	}
						
	if ($pageNum < $maxPage)
	{
		$page = $pageNum + 1;
		$next ='<span><a href="'.$url_ajax_paging.'?page='.$page.'" class="ajaxMenu"> Next > </a></span>';
	} 
	else
	{
		$next ='<span></span>';
		$last ='<span></span>';
	}
	$query = mysql_query('SELECT * FROM work WHERE dateline < NOW() ORDER BY dateline desc LIMIT '.$offset.','.$rowperpage.'');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php getHead(); ?>
	<style type="text/css">
	#err{
		background:#da0909;		
		border:1px solid #a6a6a6;		
		color:#fff; 
		font-size:12px;	
		font-weight:bold;
		margin-bottom:10px;			
		text-align:center;	
	}
	</style>
</head>

<body>
<div id="contain">
	<? getHeader(); ?>
	<div id="container">
		<div id="content">
			<?php
			while($row = mysql_fetch_array($query))
			{
				echo '<h1>'.$row["description"].'</h1><div class="border"></div>';
				echo '<p class="editDelete"><a href="editWork.php?id='.$row["work_id"].'">Edit</a> | <a href="deleteWork.php?id='.$row["work_id"].'">Delete</a></p>';
				echo '<div class="recentWorks">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td><img src="../assets/images/works/'.$row["image"].'" width="580"/></td>
								</tr>
								<tr>
									<td>
									<div class="infoFinished"></div>
									<div class="finished">'.date("d M Y",strtotime($row["dateline"])).'</div>
									</td>
								</tr>
							</table>
						</div>
					';
			}
			echo "<p class='clear'></p>";
			echo "<p class='paging center'>";
			echo $first . $prev. $nav . $next . $last;
			echo "</p>";
			?>
		</div>
	</div>
</div>
</body>
</html>