<?php
if (session_id() == '') {
    session_start();
}
define('ALBUM_IMG_DIR', '../assets/images/works/');
define('PRINTING_IMG_DIR', '../assets/images/');
define('SOUVENIR_IMG_DIR', '../assets/images/souvenir/');
define('IMAGEGALLERY_IMG_DIR', '../assets/images/imagegallery/');
define('IMAGEGALLERY_THUMBNAIL_DIR', '../assets/images/imagegallerythumbnail/');

// When we upload an image the thumbnail is created on the fly
// here we set the thumbnail width in pixel. The height will
// be adjusted proportionally

define('ALBUM_WIDTH', 700);
define('HOME_BANNER_WIDTH', 960);
define('PRINTING_WIDTH', 380);
define('PRINTING_SLIDE_WIDTH', 1200);
define('SOUVENIR_CATEGORY_WIDTH', 30);
define('SOUVENIR_WIDTH', 380);
define('SOUVENIR_GALLERY_WIDTH', 500);
define('IMAGEGALLERY_WIDTH', 800);
define('IMAGEGALLERYTHUMBNAIL_WIDTH', 380);
// make a connection to mysql here
