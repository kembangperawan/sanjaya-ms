<?
    session_start();
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
    
function getPost($name)
{
    if (isset($_POST[$name])) {
        return (get_magic_quotes_gpc() ? $_POST[$name] : addslashes($_POST[$name]));
    } else {
        return false;
    }
}

    $product_printing = trim(getPost('txt_product_printing'));
    $filename = str_replace(" ", "_", basename($_FILES["file_upload"]["name"]));
    $url = IMAGEGALLERY_IMG_DIR . $filename;
    $keywords = trim(getPost('txt_keywords'));
    $title = trim(getPost('txt_title'));
    $meta_description = trim(getPost('txt_meta_description'));
    $min_qty = trim(getPost('txt_min_qty'));
if (!isset($min_qty)) {
    $min_qty = 0;
}
    $rank = trim(getPost('txt_rank'));
if (!isset($rank)) {
    $rank = 0;
}
    $is_active = trim(getPost('chk_is_active'));
if ($is_active == '') {
    $is_active = 0;
}
    $product_content = trim(getPost('txt_product_content'));
    $announcement_board = trim(getPost('txt_announcement_board'));
    
if ($product_printing) {
    $imageFileType = strtolower(pathinfo($url, PATHINFO_EXTENSION));
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
        $err =  "Format foto harus dalam format JPEG atau GIF";
    }
    if (!isset($err)) {
        if ($_FILES['file_upload']['tmp_name'] != '') {
            $img_name   = $_FILES['file_upload']['name'];
            $tmp_name   = $_FILES['file_upload']['tmp_name'];
            $title_name = str_replace(" ", "_", $product_printing);
                    
            $ext = strrchr($img_name, ".");
        
            $product_image = $title_name ."_". time() . strtolower($ext);
            $img_path = PRINTING_IMG_DIR . $product_image;
                $result = createThumbnail($tmp_name, $img_path, PRINTING_WIDTH);
            if (!$result) {
                echo "Error uploading file";
                exit;
            }
        } else {
            $product_image = "no_image.jpg";
        }

        $query = "INSERT INTO `printing` (
				`product_printing`,
				`product_image`,
				`link`,
				`keywords`,
				`title`,
				`meta_description`,
				`min_qty`,
				`rank`,
				`is_active`,
				`product_content`,
				`announcement_board`,
				`created_at`)
				VALUES (
				'".$product_printing."',
				'".$product_image."',
				'',
				'".$keywords."',
				'".$title."',
				'".$meta_description."',
				'".$min_qty."',
				'".$rank."',
				'".$is_active."',
				'".$product_content."',
				'".$announcement_board."',
				'".date("Y-m-d H:i:s", time())."');
				";
        $result = mysql_query($query) or die(mysql_error());
        $err = 'sukses';
        header("Location:printing.php");
        exit;
    }
} else {
    $err = 'Product printing harus diisi!';
}
    header("Location:addPrinting.php?err=".$err."");
    exit;
