<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    include 'templateLayout.php';
    require_once 'library/config.php';
    $rowperpage = 5;
    $pageNum = 1;
    $page = (isset($_REQUEST["page"])?$_REQUEST["page"]:"");
    
if (isset($_GET['page'])) {
    $pageNum = $_GET['page'];
}
    $offset = ($pageNum - 1) * $rowperpage;
    $whereSearch = '';
    $pageSearch = '';
if (isset($_GET['search'])) {
    if ($_GET['search'] != '') {
        $whereSearch = ' WHERE Title LIKE \'%' . str_replace(' ', '%', $_GET['search']) . '%\' OR Tag LIKE  \'%' . str_replace(' ', '%', $_GET['search']) . '%\'';
        $pageSearch = '&search=' . $_GET['search'];
    }
}

    $query  = 'SELECT * FROM imagegallery' . $whereSearch . ' ORDER BY creatordatetime desc';
    $result = mysql_query($query) or die('Error, query failed');
    $totalitems = mysql_affected_rows();
    $maxPage = ceil($totalitems/$rowperpage);
    
if (isset($_GET['page'])) {
    if (!is_numeric($page) || $page > $maxPage || $page < 1) {
        header("location:ImageGalleryList.php");
        exit;
    }
}
    $url_ajax_paging = 'ImageGalleryList.php';
        $nav = '';
        $first = '';
        $last = '';
        $prev = '';
        $next = '';
if ($maxPage != 1) {
    for ($page = 1; $page <= $maxPage; $page++) {
        if ($page == $pageNum) {
            $nav .= " [$page] ";
        } else {
            $nav .= '<a href="'.$url_ajax_paging.'?page='.$page.$pageSearch.'" class="ajaxMenu"> '.$page.' </a>';
        }
    }
}

if ($pageNum > 1) {
    $page  = $pageNum - 1;
    $prev ='<span><a href="'.$url_ajax_paging.'?page='.$page.$pageSearch.'" class="ajaxMenu"> < Prev </a></span>';
} else {
    $first ='<span></span>';
    $prev ='<span></span>';
}
                         
if ($pageNum < $maxPage) {
    $page = $pageNum + 1;
    $next ='<span><a href="'.$url_ajax_paging.'?page='.$page.$pageSearch.'" class="ajaxMenu"> Next > </a></span>';
} else {
    $next ='<span></span>';
    $last ='<span></span>';
}
    $query = mysql_query('SELECT * FROM imagegallery' . $whereSearch . ' ORDER BY CreatorDateTime desc LIMIT '.$offset.','.$rowperpage.'');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php getHead(); ?>
    <script language="javascript" type="text/javascript">
        $(function() {
            $('#btnSubmit').click(function() {
                window.location = "?search=" + $('#txtSearch').val();
                return false;
            });
        });
    </script>
</head>

<body>
<div id="contain">
    <? getHeader(); ?>
    <div id="container">
        <div id="content">
            <div style="text-align: right; font-weight: bold;">
                <form method="post" name="form1" enctype="multipart/form-data">
                    <label for="txtSearch" style="display: inline; float: none;">Search:</label>
                    <input type="text" id="txtSearch" name="txtSearch" value="<?=(isset($_GET['search']) ? $_GET['search'] : '')?>" class="textbox" />
                    <input type="button" id="btnSubmit" name="btnSubmit" value="Go" class="buttonsubmit" />
                </form>
            </div>
        <?php
        while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) {
            echo '<h1>'.$row["Title"].'</h1><div class="border"></div>';
            echo '<p class="editDelete"><a href="editImageGallery.php?id='.$row["ImageID"].'">Edit</a> | <a href="deleteImageGallery.php?id='.$row["ImageID"].'">Delete</a></p>';
            echo '<div class="ImageGallery">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td><img src="'.IMAGEGALLERY_THUMBNAIL_DIR.$row["URL"].'" /></td>
								</tr>
							</table>
						</div>';
            echo '</br>';
        }
            echo "<p class='clear'></p>";
            echo "<p class='paging center'>";
            echo $first . $prev. $nav . $next . $last;
            echo "</p>";
        ?>
        </div>
    </div>
</div>
</body>
</html>
