<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    include 'templateLayout.php';
    $err = '';
if (isset($_GET['err']) && $_GET['err'] != '') {
    $err = $_GET['err'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php getHead('Sanjaya MS - Add Image Gallery'); ?>
    <style type="text/css">
    #err{
        background:#da0909;     
        border:1px solid #a6a6a6;       
        color:#fff; 
        font-size:12px; 
        font-weight:bold;
        margin-bottom:10px;         
        text-align:center;  
    }
    </style>
</head>

<body>
<div id="contain">
    <? getHeader(); ?>
    <div id="container">
        <div id="content">
            <h1>Add Image Gallery</h1>
            <div class="border"></div>
            <div id="loading" style="display:none;text-align:center"><img src="images/ajax.gif" alt="loading..."/></div>
            <?php echo $err != '' ? '<div id="err">'.$err.'</div>' : '' ?>
            <form action="addImageGalleryCheck.php" method="post" onsubmit="return formcheck();" name="formAddImageGallery" id="formAddImageGallery" enctype="multipart/form-data">
                <label for="txt_title">Title :</label> 
                <input type="text" name="txt_title" class="textbox" id="txt_title" size="50" />
                <p class="form_clear"></p>
                <label for="txt_description">Description :</label> 
                <input type="text" name="txt_description" class="textbox" id="txt_description" size="50" />
                <p class="form_clear"></p>
                <label for="file_upload">Image :</label>
                <input name="file_upload" id="file_upload" type="file" size="14" />
                <p class="form_clear"></p>
                <label for="txt_tag">Tags :</label> 
                <input type="text" name="txt_tag" class="textbox" id="txt_tag" size="50" /> (example : image1, image2, ...)
                <p class="form_clear"></p>
                <label></label>
                <input type='button' name='cancel' value='Kembali' class="btn" onClick="backHome()">
                <input type="submit" name="submit" value="Simpan" class="btn" id="submitForm"/>
            </form>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    function backHome()
    {
        window.location.href="ImageGalleryList.php";
    }
    function formcheck() {
        var txt_title = $('#txt_title').val();
        var txt_description = $('#txt_description').val();
        if (txt_title == '') {
            alert('Title harus diisi.');
            return false;
        }
        if (txt_description == '') {
            alert('Description harus diisi.');
            return false;
        }
        return true;
    }
</script>
</html>
