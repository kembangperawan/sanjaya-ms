<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    include 'templateLayout.php';
    $err = '';
if (isset($_GET['err']) && $_GET['err'] != '') {
    $err = $_GET['err'];
}
$query = mysql_query('select max(rank) + 1 as next_rank from printing;');
    $row = mysql_fetch_array($query);
if (mysql_num_rows($query)==0) {
    header("Location:home.php");
    exit;
} else {
    $next_rank = $row["next_rank"];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php getHead('Sanjaya MS - Add Printing'); ?>
    <style type="text/css">
    #err{
        background:#da0909;     
        border:1px solid #a6a6a6;       
        color:#fff; 
        font-size:12px; 
        font-weight:bold;
        margin-bottom:10px;         
        text-align:center;  
    }
    </style>
</head>

<body>
<div id="contain">
    <? getHeader(); ?>
    <div id="container">
        <div id="content">
            <h1>Add Printing</h1>
            <div class="border"></div>
            <div id="loading" style="display:none;text-align:center"><img src="images/ajax.gif" alt="loading..."/></div>
            <?php echo $err != '' ? '<div id="err">'.$err.'</div>' : '' ?>
            <form action="addPrintingCheck.php" method="post" name="formAddWork" id="formAddWork" enctype="multipart/form-data">
                <label for="txt_product_printing">Product Printing :</label> <input type="text" name="txt_product_printing" class="textbox" id="txt_product_printing" size="50" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="file_upload">Product Image :</label> <input name="file_upload" id="file_upload" type="file" size="14" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="txt_keywords">Keywords :</label> <input type="text" name="txt_keywords" class="textbox" id="txt_keywords" size="100" value="" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="txt_title">Page Title :</label> <input type="text" name="txt_title" class="textbox" id="txt_title" size="100" value="" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="txt_meta_description">Meta Description :</label> <input type="text" name="txt_meta_description" class="textbox" id="txt_meta_description" size="100" value="" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="txt_min_qty">Min Qty :</label> <input type="text" name="txt_min_qty" class="textbox" id="txt_min_qty" size="5" value="" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="txt_rank">Rank :</label> <input type="text" name="txt_rank" class="textbox" id="txt_rank" size="5" value="<?=$next_rank?>" />
                <p class="form_clear"></p>
                <label for="chk_is_active">Is Active :</label> <input type="checkbox" name="chk_is_active" id="chk_is_active" checked="checked" value="1" style="margin-top: 10px;" />
                <p class="form_clear"></p>
                <label for="txt_product_content">Product Content :</label>
                <p class="form_clear"></p>
                <textarea name="txt_product_content" cols="80" rows="20" class="textarea" id="txt_product_content" style="width:300;height:100px;"></textarea>
                <p class="form_clear"></p>
                <label for="txt_announcement_board">Announcement :</label>
                <p class="form_clear"></p>
                <textarea name="txt_announcement_board" cols="80" rows="20" class="textarea" id="txt_announcement_board" style="width:300;height:100px;"></textarea>
                <p class="form_clear"></p>
                <input type='button' name='cancel' value='Kembali' class="btn" onClick="backHome()">
                <input type="submit" name="submit" value="Simpan" class="btn" id="submitForm"/>
            </form>
            <br>Contoh Description : "Beli TV LED SAMSUNG dengan Kualitas TERJAMIN, 100% ASLI, Harga MURAH, CICILAN 0%, dan GRATIS Pengiriman - Hanya di Bhinneka.Com" <br><br>
            Contoh Keyword : "Jual TV LED SAMSUNG - Daftar Lengkap Harga dan Spesifikasi Produk Bhinneka.Com, Product Price and Specification List, Kualitas TERJAMIN, 100% ASLI, Harga MURAH, CICILAN 0%, dan GRATIS Pengiriman, Buy, Sell, Jual, Beli"

        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    function backHome()
    {
        window.location.href="printing.php";
    }
    tinymce.init({
    mode: "exact",
    elements : ["txt_product_content",  "txt_announcement_board"],
    themes: "modern",
    height:"250px",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    relative_urls: false,
    filemanager_title:"Responsive Filemanager",
    external_filemanager_path:"../filemanager/",
    external_plugins: { "filemanager" : "../../filemanager/plugin.min.js"},
    filemanager_access_key:"myPrivateKey",
    image_advtab: true
});
</script>
</html>
