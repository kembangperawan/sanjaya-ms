<?
    session_start();
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
    $id = $_REQUEST["id"];
    $query = mysql_query('select * from printing where printing_id = "'.$id.'"');
    $row = mysql_fetch_array($query);
if (mysql_num_rows($query)==0) {
    header("Location:home.php");
    exit;
} else {
    $id = $row["printing_id"];
}
    $err = '';
if (isset($_GET['err']) && $_GET['err'] != '') {
    $err = $_GET['err'];
}
function getPost($name)
{
    if (isset($_POST[$name])) {
        return (get_magic_quotes_gpc() ? $_POST[$name] : addslashes($_POST[$name]));
    } else {
        return false;
    }
}
if (isset($_POST["submit"])) {
    $product_printing = trim(getPost('txt_product_printing'));
    $keywords = trim(getPost('txt_keywords'));
    $title = trim(getPost('txt_title'));
    $meta_description = trim(getPost('txt_meta_description'));
    $min_qty = trim(getPost('txt_min_qty'));
    if (!isset($min_qty)) {
        $min_qty = 0;
    }
    $rank = trim(getPost('txt_rank'));
    if (!isset($rank)) {
        $rank = 0;
    }
    $is_active = trim(getPost('chk_is_active'));
    if ($is_active == '') {
        $is_active = 0;
    }
    $product_content = trim(getPost('txt_product_content'));
    $announcement_board = trim(getPost('txt_announcement_board'));
        
    if ($product_printing) {
        // if($HTTP_POST_FILES['file_upload']['name'] != "" && $HTTP_POST_FILES['file_upload']['type'] != "image/jpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/pjpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/gif")
        // {
        // 	$err = 'Format foto harus dalam format JPEG atau GIF';
        // }
        if ($_FILES['file_upload']['tmp_name'] != '') {
            $img_name   = $_FILES['file_upload']['name'];
            $tmp_name   = $_FILES['file_upload']['tmp_name'];
            $title_name = str_replace(" ", "_", $product_printing);
                    
            $ext = strrchr($img_name, ".");

            $product_image = $title_name ."_". time() . strtolower($ext);
            $img_path = PRINTING_IMG_DIR . $product_image;
                $result = createThumbnail($tmp_name, $img_path, PRINTING_WIDTH);
            if (!$result) {
                echo "Error uploading file";
                exit;
            }
        } else {
            // don't change the image
            $res = mysql_query("select product_image from `printing` where printing_id = '".$id."'")
            or die("<meta http-equiv='refresh' content='0;URL=editPrinting.php?err=Gagal mengambil data.'>");
        
            $row = mysql_fetch_assoc($res);
            $product_image = $row['product_image'];
        }
            
        $query = "UPDATE `printing` SET					
									product_printing = '".$product_printing."',
									product_image = '".$product_image."',
									keywords = '".$keywords."',
									title = '".$title."',
									meta_description = '".$meta_description."',
									min_qty  = '".$min_qty."',
									rank  = '".$rank."',
									is_active  = '".$is_active."',
									product_content = '".$product_content."',
									announcement_board = '".$announcement_board."',
									updated_at  = '".date("Y-m-d H:i:s", time())."'
									WHERE printing_id = $id";
        $result = mysql_query($query) or die(mysql_error());
        
        header("Location:printing.php");
        exit;
    } else {
        $err = 'Product printing harus diisi!';
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include "title.php"; ?>
<link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/sms.min.css?d=201706150056">
<link rel="stylesheet" type="text/css" href="css/datepick.css">
<link rel="stylesheet" href="docs/style.css" type="text/css">
<script type="text/javascript" src="js/datepick.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script language="javascript" type="text/javascript">
function backHome()
{
    window.location.href="printing.php";
}
</script>
<script type="text/javascript">
tinymce.init({
    mode: "exact",
    elements : ["txt_product_content",  "txt_announcement_board"],
    themes: "modern",
    height:"250px",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    relative_urls: false,
    filemanager_title:"Responsive Filemanager",
    external_filemanager_path:"../filemanager/",
    external_plugins: { "filemanager" : "../../filemanager/plugin.min.js"},
    filemanager_access_key:"myPrivateKey",
    image_advtab: true
});
</script>
<script>
$(function() {
    $('a.ajaxMenu').click(function() {
        $('#ajaxContent').html('<p class="center"><img src="images/loadingLightbox.gif"/><br><i>Loading...</i><p>');
        var url = $(this).attr('href');
        $('#ajaxContent').load(url);
        
        return false;
    });
    $('.err').click(function() {
        $(this).fadeOut('slow');
        return false;
    });
});
</script>
<style type="text/css">
#err{
    background:#da0909;     
    border:1px solid #a6a6a6;       
    color:#fff; 
    font-size:12px; 
    font-weight:bold;
    margin-bottom:10px;         
    text-align:center;  
}
</style>

</head>

<body>
<div id="contain">
    <? include("header.php"); ?>
    <div id="container">
        <div id="content">
        
            <h1>Edit Printing</h1>
            <div class="border"></div>
            <div id="loading" style="display:none;text-align:center"><img src="images/ajax.gif" alt="loading..."/></div>
            <?php echo $err != '' ? '<div id="err">'.$err.'</div>' : '' ?>
            <form action="" method="post" name="form1" enctype="multipart/form-data">
                        <label for="txt_product_printing">Product Printing :</label> <input type="text" name="txt_product_printing" class="textbox" id="txt_product_printing" size="50" value="<?=$row["product_printing"]?>" /><p class="form_clear"></p>
                        <p class="form_clear"></p>
                        <label for="file_upload">Product Image :</label> <input name="file_upload" id="file_upload" type="file" size="14" /><p class="form_clear"></p>
                        <p class="form_clear"></p>
                        <label></label>
                        <div><img src="../assets/images/<?php echo $row["product_image"];?>" width="230" style="background:grey;padding:5px; "/></div>
                        <label></label>
                        <p class="form_clear"></p>
                        <label for="txt_keywords">Keywords :</label> <input type="text" name="txt_keywords" class="textbox" id="txt_keywords" size="100" value="<?=$row["keywords"]?>" /><p class="form_clear"></p>
                        <p class="form_clear"></p>
                        <label for="txt_title">Page Title :</label> <input type="text" name="txt_title" class="textbox" id="txt_title" size="100" value="<?=$row["title"]?>" /><p class="form_clear"></p>
                        <p class="form_clear"></p>
                        <label for="txt_meta_description">Meta Description :</label> <input type="text" name="txt_meta_description" class="textbox" id="txt_meta_description" size="100" value="<?=$row["meta_description"]?>" /><p class="form_clear"></p>
                        <p class="form_clear"></p>
                        <label for="txt_url">URL :</label> <input type="text" name="txt_url" class="textbox url" id="txt_url" size="50" value="<?=$row["link"]?>" readonly/>
                        <p class="form_clear"></p>
                        <label for="txt_min_qty">Min Qty :</label> <input type="text" name="txt_min_qty" class="textbox" id="txt_min_qty" size="5" value="<?=$row["min_qty"]?>" /><p class="form_clear"></p>
                        <p class="form_clear"></p>
                        <label for="txt_rank">Rank :</label> <input type="text" name="txt_rank" class="textbox" id="txt_rank" size="5" value="<?=$row["rank"]?>" />
                        <p class="form_clear"></p>
                        <label for="chk_is_active">Is Active :</label> <input type="checkbox" name="chk_is_active" id="chk_is_active"<?=($row["is_active"] == 1 ? " checked=\"checked\"" : "")?> value="1" style="margin-top: 10px;" />
                        <p class="form_clear"></p>
                        <label for="txt_product_content">Product Content :</label> 
                        <p class="form_clear"></p>
                        <textarea name="txt_product_content" cols="80" rows="20" class="textarea" id="txt_product_content" style="width:300;
                        height:100px;" ><?=$row["product_content"]?></textarea>
                        <p class="form_clear"></p>
                        <label for="txt_announcement_board">Announcement :</label> 
                        <p class="form_clear"></p>
                        <textarea name="txt_announcement_board" cols="80" rows="20" class="textarea" id="txt_announcement_board" style="width:300;
                        height:100px;" ><?=$row["announcement_board"]?></textarea>
                        <p class="form_clear"></p>
                        <input type='button' name='cancel' value='Kembali' class="btn" onClick="backHome()">
                        <input type="submit" name="submit" value="Simpan" class="btn"/>
            </form>
            <br>Contoh Description : "Beli TV LED SAMSUNG dengan Kualitas TERJAMIN, 100% ASLI, Harga MURAH, CICILAN 0%, dan GRATIS Pengiriman - Hanya di Bhinneka.Com" <br><br>
            Contoh Keyword : "Jual TV LED SAMSUNG - Daftar Lengkap Harga dan Spesifikasi Produk Bhinneka.Com, Product Price and Specification list, Kualitas TERJAMIN, 100% ASLI, Harga MURAH, CICILAN 0%, dan GRATIS Pengiriman, Buy, Sell, Jual, Beli"
                        
            
            <div class="clear"></div>
            </div>
        </div>
    </div>
</div>

        
</body>
</html>
