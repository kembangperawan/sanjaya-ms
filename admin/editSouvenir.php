<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
    
    $id = $_REQUEST["id"];
    
    $query = mysql_query('select * from souvenir where souvenir_id = "'.$id.'"');
    $row = mysql_fetch_array($query);
if (mysql_num_rows($query)==0) {
    header("Location:home.php");
    exit;
}
    
function getPost($name)
{
    if (isset($_POST[$name])) {
        return (get_magic_quotes_gpc() ? $_POST[$name] : addslashes($_POST[$name]));
    } else {
        return false;
    }
}
if (isset($_POST["submit"])) {
    $souvenir = trim(getPost('txt_souvenir'));
    $category = trim(getPost('txt_category'));
    $description = trim(getPost('txt_description'));
    $keyword = trim(getPost('txt_keyword'));
    $meta_description = trim(getPost('txt_meta_description'));
    $status = trim(getPost('txt_status'));
    $rank = trim(getPost('txt_rank'));
    if (!isset($rank)) {
        $rank = 0;
    }
    $is_active = trim(getPost('chk_is_active'));
    if ($is_active == '') {
        $is_active = 0;
    }
        
    if ($souvenir && $category) {
        if ($HTTP_POST_FILES['file_upload']['name'] != "" && $HTTP_POST_FILES['file_upload']['type'] != "image/jpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/pjpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/gif") {
            $err = 'Format foto harus dalam format JPEG atau GIF';
        }
        if (!$err) {
            if ($_FILES['file_upload']['tmp_name'] != '') {
                $imgName   = $_FILES['file_upload']['name'];
                $tmpName   = $_FILES['file_upload']['tmp_name'];
                $titleName = strtolower(str_replace(" ", "-", str_replace("/", "-", str_replace(".", "-", $souvenir))));
                    
                $ext = strrchr($imgName, ".");
        
                $newName = $titleName ."-". time() . strtolower($ext);
                $imgPath = SOUVENIR_IMG_DIR . $newName;
                    $result = createThumbnail($tmpName, $imgPath, SOUVENIR_WIDTH);
                if (!$result) {
                    echo "Error uploading file";
                    exit;
                }
            } else {
                // don't change the image
                $res = mysql_query("select image from `souvenir` where souvenir_id = '".$id."'")
                or die("<meta http-equiv='refresh' content='0;URL=editWork.php?err=Gagal mengambil data.'>");
            
                $row = mysql_fetch_assoc($res);
                $newName = $row['image'];
            }
                
            $query = "UPDATE `souvenir` SET					
										souvenir_name = '".$souvenir."',
										category_id  = '".$category."',
										description = '".$description."',
										image = '".$newName."',
										meta_description = '".$meta_description."',
										keyword = '".$keyword."',
										status = '".$status."',
										urutan = '".$rank."',
                                        is_active = '".$is_active."'
										WHERE souvenir_id = '$id'";
            $result = mysql_query($query) or die(mysql_error());
           
            header("Location:souvenir.php");
            exit;
        }
    } else {
        $err = 'Masukkan semua data dengan benar!';
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include "title.php"; ?>
<link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/sms.min.css?d=201706150056">
<link rel="stylesheet" type="text/css" href="css/datepick.css">
<link rel="stylesheet" href="docs/style.css" type="text/css">
<script type="text/javascript" src="js/datepick.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script language="javascript" type="text/javascript">

tinymce.init({
    mode: "exact",
    elements : ["txt_description"],
    themes: "modern",
    height:"250px",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    relative_urls: false,
    filemanager_title:"Responsive Filemanager",
    external_filemanager_path:"../filemanager/",
    external_plugins: { "filemanager" : "../../filemanager/plugin.min.js"},
    filemanager_access_key:"myPrivateKey",
    image_advtab: true
});

$(function() {
    $('a.ajaxMenu').click(function() {
        $('#ajaxContent').html('<p class="center"><img src="images/loadingLightbox.gif"/><br><i>Loading...</i><p>');
        var url = $(this).attr('href');
        $('#ajaxContent').load(url);
        
        return false;
    });
    $('.err').click(function() {
        $(this).fadeOut('slow');
        return false;
    });
});

/*
tinymce.init(
        {
        selector:'textarea'
        }
);
*/
</script>

</head>

<body>
<div id="contain">
    <? include("header.php"); ?>
    <div id="container">
        <div id="content">
        
            <h1>Edit Souvenir</h1>
            <div class="border"></div>
            <div id="loading" style="display:none;text-align:center"><img src="images/ajax.gif" alt="loading..."/></div>
            <div id="err" style="display:none;"></div>
            <form action="" method="post" name="form1" enctype="multipart/form-data">
                        <label for="txt_souvenir">Nama Souvenir :</label> <input type="text" name="txt_souvenir" class="textbox" id="txt_souvenir" size="50" value="<?=$row["souvenir_name"]?>" /><p class="form_clear"></p>
                        <p class="form_clear"></p>
                        <label for="txt_category">Kategori :</label> 
                        <select name="txt_category" id="txt_category">
                        <?php
                        $res2 = mysql_query("select * from category order by urutan asc");
                        while ($row2 = mysql_fetch_array($res2)) {
                            if ($row2["category_id"] == $row["category_id"]) {
                                echo "<option value =".$row2["category_id"]." selected>".$row2["category_name"];
                            } else {
                                echo "<option value =".$row2["category_id"].">".$row2["category_name"];
                            }
                            echo "</option>";
                        }
                        ?>
                        </select>
                        <p class="form_clear"></p>
                        <label for="txt_category">Status :</label> 
                        <select name="txt_status" id="txt_status">
                        <option value ="1" <? if ($row["status"]==1) {
                            echo 'selected';
}?>>Tidak Ada Keterangan</option>
                        <option value ="2" <? if ($row["status"]==2) {
                            echo 'selected';
}?>>New Product</option>
                        <option value ="3" <? if ($row["status"]==3) {
                            echo 'selected';
}?>>Best Price</option>
                        <option value ="4" <? if ($row["status"]==4) {
                            echo 'selected';
}?>>Hot Items</option>
                        </select>
                        <p class="form_clear"></p>
                        <label for="txt_category">Keterangan :</label>
                        <p class="form_clear"></p>
                        <textarea name="txt_description" cols="80" rows="20" class="textarea" id="txt_description" style="width:300;height:200px;"><?=$row["description"]?></textarea>
                        <p class="form_clear"></p>
                        <label for="txt_meta_description">Tags Description :</label> <input type="text" name="txt_meta_description" class="textbox" id="txt_meta_description" size="100" value="<?=$row["meta_description"]?>" /><p class="form_clear"></p>
                        <p class="form_clear"></p>
                        <label for="txt_keyword">Tags Keyword :</label> <input type="text" name="txt_keyword" class="textbox" id="txt_keyword" size="100" value="<?=$row["keyword"]?>" /><p class="form_clear"></p>
                        <p class="form_clear"></p>
                        <label for="txt_rank">Rank :</label> <input type="text" name="txt_rank" class="textbox" id="txt_rank" size="5" value="<?=$row["urutan"]?>" />
                        <p class="form_clear"></p>
                        <label for="chk_is_active">Is Active :</label> <input type="checkbox" name="chk_is_active" id="chk_is_active"<?=($row["is_active"] == 1 ? " checked=\"checked\"" : "")?> value="1" style="margin-top: 10px;" />
                        <p class="form_clear"></p>
                        <label for="file_upload">Upload Foto :</label><input name="file_upload" id="file_upload" type="file" size="14" /><p class="form_clear"></p>
                        <label></label>
                        <div><img src="../assets/images/souvenir/<?php echo $row["image"];?>" width="230" style="background:grey;padding:5px; "/></div>
                        <label></label>
                        <input type='button' name='cancel' value='Kembali' class="btn" onclick="history.go(-1);">
                        <input type="submit" name="submit" value="Simpan" class="btn"/>
            </form>
            <br>Contoh Description : "Beli TV LED SAMSUNG dengan Kualitas TERJAMIN, 100% ASLI, Harga MURAH, CICILAN 0%, dan GRATIS Pengiriman - Hanya di Bhinneka.Com" <br><br>
            Contoh Keyword : "Jual TV LED SAMSUNG - Daftar Lengkap Harga dan Spesifikasi Produk Bhinneka.Com, Product Price and Specification List, Kualitas TERJAMIN, 100% ASLI, Harga MURAH, CICILAN 0%, dan GRATIS Pengiriman, Buy, Sell, Jual, Beli"
                        
            
            <div class="clear"></div>
            </div>
        </div>
    </div>
</div>

        
</body>
</html>
