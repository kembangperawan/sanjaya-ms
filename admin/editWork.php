<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
    
    $id = $_REQUEST["id"];
    
    $query = mysql_query('select * from work where work_id = "'.$id.'"');
    $row = mysql_fetch_array($query);
if (mysql_num_rows($query)==0) {
    header("Location:home.php");
    exit;
}
    $txt_workId = $row["work_id"];
    $txt_pekerjaan = $row["description"];
    $txt_image = $row["image"];
    $txt_keyword = $row["keyword"];
    $txt_dateline = date("d/m/Y", strtotime($row["dateline"]));
    
function getPost($name)
{
    if (isset($_POST[$name])) {
        return (get_magic_quotes_gpc() ? $_POST[$name] : addslashes($_POST[$name]));
    } else {
        return false;
    }
}
if (isset($_POST["submit"])) {
    $work = trim(getPost('txt_pekerjaan'));
    $keyword = trim(getPost('txt_keyword'));
    $date = trim(getPost('txt_tgl'));
        
    if ($work && $date) {
        if ($HTTP_POST_FILES['file_upload']['name'] != "" && $HTTP_POST_FILES['file_upload']['type'] != "image/jpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/pjpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/gif") {
            $err = 'Format foto harus dalam format JPEG atau GIF';
        }
        if (!$err) {
            if ($_FILES['file_upload']['tmp_name'] != '') {
                $imgName   = $_FILES['file_upload']['name'];
                $tmpName   = $_FILES['file_upload']['tmp_name'];
                $titleName = str_replace(" ", "_", $work);
                    
                $ext = strrchr($imgName, ".");
        
                $newName = $titleName ."_". time() . strtolower($ext);
                $imgPath = ALBUM_IMG_DIR . $newName;
                $result = createThumbnail($tmpName, $imgPath, ALBUM_WIDTH);
                if (!$result) {
                    echo "Error uploading file";
                    exit;
                }
            } else {
                // don't change the image
                $res = mysql_query("select image from `work` where work_id = '".$id."'")
                or die("<meta http-equiv='refresh' content='0;URL=editWork.php?err=Gagal mengambil data.'>");
            
                $row = mysql_fetch_assoc($res);
                $newName = $row['image'];
            }
                
            $query = "UPDATE `work` SET					
										dateline = str_to_date('".$date."', '%d/%c/%Y'),
										description = '".$work."',
										image = '".$newName."',
										keyword = '".$keyword."'
										WHERE work_id = '$id'";
            $result = mysql_query($query) or die(mysql_error());
           
            header("Location:home.php");
            exit;
        }
    } else {
        $err = 'Masukkan semua data dengan benar!';
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include "title.php"; ?>
<link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/sms.min.css?d=201706150056">
<link rel="stylesheet" type="text/css" href="css/datepick.css">
<link rel="stylesheet" href="docs/style.css" type="text/css">
<script type="text/javascript" src="js/datepick.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script language="javascript" type="text/javascript">
function backHome()
{
    window.location.href="recentWork.php";
}
function cek(){
    if(document.getElementById('txt_pekerjaan').value=="")
    {
        alert("Silahkan masukkan jenis pekerjaan");
        return 0;
    }
    if(document.getElementById('txt_tgl').value=="")
    {
        alert("Silahkan masukkan tanggal pekerjaan");
        return 0;
    }
    return 1;
}
$(function() {
    $('a.ajaxMenu').click(function() {
        $('#ajaxContent').html('<p class="center"><img src="images/loadingLightbox.gif"/><br><i>Loading...</i><p>');
        $('a.ajaxMenu').css("color","#da0909");
        $(this).css("color","#910000");
        var url = $(this).attr('href');
        $('#ajaxContent').load(url);
        
        return false;
    });
});
</script>

</head>

<body>
<div id="contain">
    <? include("header.php"); ?>
    <div id="container">
        <div id="content">
            <h1>Ubah Data Pekerjaan</h1>
            <div class="border"></div>
            <form action="" method="post" name="form1" enctype="multipart/form-data">
            <input name="id" type="hidden" id="id" value="<?php echo $id; ?>" />
            <label for="txt_pekerjaan">Jenis Pekerjaan :</label> <input type="text" name="txt_pekerjaan" class="textbox" id="txt_judul" size="50" value="<?=$txt_pekerjaan;?>" /><p class="form_clear"></p>
            <label for="txt_tgl">Dateline :</label> 
            <input type="text" name="txt_tgl" class="textbox" id="txt_tgl" size="20" onclick="displayDatePicker('txt_tgl', this);" readonly="true" value="<?=$txt_dateline;?>" /><input value="Pilih Tanggal" onclick="displayDatePicker('txt_tgl', this);" type="button">
            <p class="form_clear"></p>
            <label for="txt_judul">Tags Keyword :</label> <input type="text" name="txt_keyword" class="textbox" id="txt_keyword" size="50" value="<?=$txt_keyword;?>" /><p class="form_clear"></p>
            <p class="form_clear"></p>
            <label for="file_upload">Upload Foto :</label><input name="file_upload" id="file_upload" type="file" size="14" /><p class="form_clear"></p>
            <label></label>
            <input type='button' name='cancel' value='Kembali' class="btn" onClick="backHome()">
            <input type="submit" name="submit" value="Simpan" class="btn" onClick="if(cek())return true;else return false;"/>
            </form><br />
            <div class="border"></div>
            <?
                $query = mysql_query('SELECT * FROM work WHERE work_id = '.$id.'');
            while ($row = mysql_fetch_array($query)) {
                echo '<h3>'.$row["description"].'</h3>';
                echo '<div class="recentWorks">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td><img src="../assets/images/works/'.$row["image"].'" width="580"/></td>
									</tr>
									<tr>
										<td>
										<div class="infoCountdown"></div>
										<div class="finished">'.date("d M Y", strtotime($row["dateline"])).'</div>
										</td>
									</tr>
								</table>
							</div>
						';
            }
            ?>
        </div>
    </div>
</div>

        
</body>
</html>
