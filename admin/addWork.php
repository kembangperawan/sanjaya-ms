<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    include 'templateLayout.php';
    $err = '';
if (isset($_GET['err']) && $_GET['err'] != '') {
    $err = $_GET['err'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php getHead('Sanjaya MS - Add Work'); ?>
    <style type="text/css">
    #err{
        background:#da0909;     
        border:1px solid #a6a6a6;       
        color:#fff; 
        font-size:12px; 
        font-weight:bold;
        margin-bottom:10px;         
        text-align:center;  
    }
    </style>
</head>

<body>
<div id="contain">
    <? getHeader(); ?>
    <div id="container">
        <div id="content">
            <h1>Add Work</h1>
            <div class="border"></div>
            <div id="loading" style="display:none;text-align:center"><img src="images/ajax.gif" alt="loading..."/></div>
            <?php echo $err != '' ? '<div id="err">'.$err.'</div>' : '' ?>
            <form action="addWorkCheck.php" method="post" onsubmit="return formcheck();" name="formAddWork" id="formAddWork" enctype="multipart/form-data">
                <label for="txt_judul">Jenis Pekerjaan :</label> <input type="text" name="txt_pekerjaan" class="textbox" id="txt_judul" size="50" /><p class="form_clear"></p>
                <label for="txt_tgl">Dateline :</label> 
                <input type="text" name="txt_tgl" class="textbox" id="txt_tgl" size="20" onclick="displayDatePicker('txt_tgl', this);" readonly="true" /><input value="Pilih Tanggal" onclick="displayDatePicker('txt_tgl', this);" type="button">
                <p class="form_clear"></p>
                <label for="txt_keyword">Tags Keyword :</label> <input type="text" name="txt_keyword" class="textbox" id="txt_keyword" size="50" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="file_upload">Upload Foto :</label><input name="file_upload" id="file_upload" type="file" size="14" /><p class="form_clear"></p>
                <label></label>
                <input type='button' name='cancel' value='Kembali' class="btn" onClick="backHome()">
                <input type="submit" name="submit" value="Simpan" class="btn" id="submitForm"/>
            </form>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    function backHome()
    {
        window.location.href="recentWork.php";
    }
    function formcheck() {
        var txt_judul = $('#txt_judul').val();
        var txt_keyword = $('#txt_keyword').val();
        if (txt_judul == '') {
            alert('Judul harus diisi.');
            return false;
        }
        if (txt_keyword == '') {
            alert('Keyword harus diisi.');
            return false;
        }
        return true;
    }
</script>
</html>
