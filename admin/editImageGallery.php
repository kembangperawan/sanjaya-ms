<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
    
    $id = '';
    $txt_title = '';
    $txt_description = '';
    $txt_tag = '';
    $image = '';

if (isset($_REQUEST['id']) && $_REQUEST['id'] != null) {
    $id = $_REQUEST["id"];
    $query = mysql_query('select * from imagegallery where ImageID = "'.$id.'"');
    $row = mysql_fetch_array($query);
    if (mysql_num_rows($query)==0) {
        header("Location:home.php");
        exit;
    }
    $txt_title = $row["Title"];
    $txt_description = $row["Description"];
    $image = $row["URL"];
    $txt_tag = $row["Tag"];
}
    
function getPost($name)
{
    if (isset($_POST[$name])) {
        return (get_magic_quotes_gpc() ? $_POST[$name] : addslashes($_POST[$name]));
    } else {
        return false;
    }
}
if (isset($_POST["submit"])) {
    $title = trim(getPost('txt_title'));
    $description = trim(getPost('txt_description'));
    $filename = str_replace(" ", "-", basename($_FILES["file_upload"]["name"]));
    $url = IMAGEGALLERY_IMG_DIR . $filename;
    $imageFileType = strtolower(pathinfo($url, PATHINFO_EXTENSION));
    $filename = strtolower(str_replace(" ", "-", str_replace("/", "-", str_replace(".", "-", $title)))) . "-" . date("Ymdhis") . "." . $imageFileType;
    $url = IMAGEGALLERY_IMG_DIR . $filename;
    $tag = trim(getPost('txt_tag'));
    $editordatetime = date("Y-m-d h:i:s");
    $editorid = trim($_SESSION["username"]);
    $errormessage = "";

    if ($title == "") {
        $errormessage = "Image title cannot be empty.";
    } else {
        $tmpName = $_FILES['file_upload']['tmp_name'];
        if ($tmpName != '') {
            $watermark = imagecreatefrompng('../assets/images/watermark.png');
            switch ($imageFileType) {
                case 'jpg':
                case 'jpeg':
                    $image = imagecreatefromjpeg($tmpName);
                    break;
                case 'gif':
                    $image = imagecreatefromgif($tmpName);
                    break;
                case 'png':
                    $image = imagecreatefrompng($tmpName);
                    break;
            }
            imagecopy($image, $watermark, (imagesx($image) - imagesx($watermark)) / 2, (imagesy($image) - imagesy($watermark)) / 2, 0, 0, imagesx($watermark), imagesy($watermark));
            switch ($imageFileType) {
                case 'jpg':
                case 'jpeg':
                    imagejpeg($image, $tmpName);
                    break;
                case 'gif':
                    imagegif($image, $tmpName);
                    break;
                case 'png':
                    imagepng($image, $tmpName);
                    break;
            }

            $result = createThumbnail($tmpName, $url, IMAGEGALLERY_WIDTH);
            $result = createThumbnail($tmpName, str_replace(IMAGEGALLERY_IMG_DIR, IMAGEGALLERY_THUMBNAIL_DIR, $url), IMAGEGALLERYTHUMBNAIL_WIDTH);
            if ($result == '') {
                header("Location:editImageGallery.php?id=".$id."&err=Sorry, there was an error uploading your file.");
                exit;
            }
        } else {
            // don't change the image
            $filename = $image;
        }
        $query = "UPDATE imagegallery SET					
                                        Title = '".$title."',
										Description = '".$description."',
										URL = '".$filename."',
										Tag = '".$tag."'
										WHERE ImageID = '$id'";
        $result = mysql_query($query) or die(mysql_error());
        if ($result) {
            $errormessage = "Update Successful.";
        }
    }
    $location = "Location:editImageGallery.php";
    if ($errormessage != "") {
        $location = $location . "?id=" . $id . "&err=" . $errormessage;
    }
    header($location);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include "title.php"; ?>
<link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/sms.min.css?d=201706150056">
<link rel="stylesheet" type="text/css" href="css/datepick.css">
<link rel="stylesheet" href="docs/style.css" type="text/css">
<script type="text/javascript" src="js/datepick.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script language="javascript" type="text/javascript">
function backHome()
{
    window.location.href="ImageGalleryList.php";
}
function cek(){
    if(document.getElementById('txt_title').value=="")
    {
        alert("Image Title cannot be empty.");
        return 0;
    }
    if(document.getElementById('txt_tag').value=="")
    {
        alert("Tag cannot be empty.");
        return 0;
    }
    return 1;
}
$(function() {
    $('a.ajaxMenu').click(function() {
        $('#ajaxContent').html('<p class="center"><img src="images/loadingLightbox.gif"/><br><i>Loading...</i><p>');
        $('a.ajaxMenu').css("color","#da0909");
        $(this).css("color","#910000");
        var url = $(this).attr('href');
        $('#ajaxContent').load(url);
        
        return false;
    });
});
</script>
</head>
<body>
<div id="contain">
    <? include("header.php"); ?>
    <div id="container">
        <div id="content">
            <h1>Edit Image Gallery</h1>
            <div class="border"></div>
            <?php
                    $err = "";
            if (isset($_REQUEST["err"])) {
                $err = $_REQUEST["err"];
            }
            if ($err != "") {
                echo '<p class="err">'.$err.'</p>';
            }
            ?>
            <form action="" method="post" name="form1" enctype="multipart/form-data">
            <input name="id" type="hidden" id="id" value="<?php echo $id; ?>" />
            <label for="txt_title">Title :</label> 
            <input type="text" name="txt_title" class="textbox" id="txt_title" size="50" value="<?=$txt_title;?>"/>
            <p class="form_clear"></p>
            <label for="txt_description">Description :</label> 
            <input type="text" name="txt_description" class="textbox" id="txt_description" size="50" value="<?=$txt_description;?>"/>
            <p class="form_clear"></p>
            <label for="file_upload">Image :</label>
            <input name="file_upload" id="file_upload" type="file" size="14" />
            <p class="form_clear"></p>
            <label for="txt_tag">Tags :</label> 
            <input type="text" name="txt_tag" class="textbox" id="txt_tag" size="50" value="<?=$txt_tag;?>"/>
            <p class="form_clear"></p>
            <label></label>
            <input type='button' name='cancel' value='Kembali' class="btn" onClick="backHome()">
            <input type="submit" name="submit" value="Simpan" class="btn" onClick="if(cek())return true;else return false;"/>
            </form><br />
            <img src="<?=IMAGEGALLERY_IMG_DIR.$image?>"/>
            <div class="border"></div>
        </div>
    </div>
</div>	
</body>
</html>
