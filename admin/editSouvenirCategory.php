<?
    session_start();
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
    $id = $_REQUEST["id"];
    $query = mysql_query('select * from category where category_id = "'.$id.'"');
    $row = mysql_fetch_array($query);
if (mysql_num_rows($query)==0) {
    header("Location:home.php");
    exit;
} else {
    $id = $row["category_id"];
}
    $err = '';
if (isset($_GET['err']) && $_GET['err'] != '') {
    $err = $_GET['err'];
}
function getPost($name)
{
    if (isset($_POST[$name])) {
        return (get_magic_quotes_gpc() ? $_POST[$name] : addslashes($_POST[$name]));
    } else {
        return false;
    }
}
if (isset($_POST["submit"])) {
    $name = trim(getPost('txt_name'));
    $rank = trim(getPost('txt_rank'));
    if (!isset($rank)) {
        $rank = 0;
    }
        
    if ($name) {
        // if($HTTP_POST_FILES['file_upload']['name'] != "" && $HTTP_POST_FILES['file_upload']['type'] != "image/jpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/pjpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/gif")
        // {
        // 	$err = 'Format foto harus dalam format JPEG atau GIF';
        // }
        if ($_FILES['file_upload']['tmp_name'] != '') {
            $img_name   = $_FILES['file_upload']['name'];
            $tmp_name   = $_FILES['file_upload']['tmp_name'];
            $title_name = str_replace(" ", "", $name);
                    
            $ext = strrchr($img_name, ".");

            $product_image = strtolower($title_name) . strtolower($ext);
            $img_path = SOUVENIR_IMG_DIR . 'icon_' . $product_image;
            $product_image = strtolower($title_name);
                $result = createThumbnail($tmp_name, $img_path, SOUVENIR_CATEGORY_WIDTH);
            if (!$result) {
                echo "Error uploading file";
                exit;
            }
        } else {
            // don't change the image
            $res = mysql_query("select category_url from `category` where category_id = '".$id."'")
            or die("<meta http-equiv='refresh' content='0;URL=editPrinting.php?err=Gagal mengambil data.'>");
        
            $row = mysql_fetch_assoc($res);
            $product_image = $row['category_url'];
        }
            
        $query = "UPDATE `category` SET					
									category_name = '".$name."',
									category_url = '".$product_image."',
									urutan  = '".$rank."'
									WHERE category_id = $id";
        $result = mysql_query($query) or die(mysql_error());
        
        header("Location:souvenirCategory.php");
        exit;
    } else {
        $err = 'Name harus diisi!';
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include "title.php"; ?>
<link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/sms.min.css?d=201706150056">
<link rel="stylesheet" type="text/css" href="css/datepick.css">
<link rel="stylesheet" href="docs/style.css" type="text/css">
<script type="text/javascript" src="js/datepick.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script language="javascript" type="text/javascript">
function backHome()
{
    window.location.href="souvenirCategory.php";
}
</script>
<script>
$(function() {
    $('.err').click(function() {
        $(this).fadeOut('slow');
        return false;
    });
});
</script>
<style type="text/css">
#err{
    background:#da0909;     
    border:1px solid #a6a6a6;       
    color:#fff; 
    font-size:12px; 
    font-weight:bold;
    margin-bottom:10px;         
    text-align:center;  
}
</style>

</head>

<body>
<div id="contain">
    <? include("header.php"); ?>
    <div id="container">
        <div id="content">
        
            <h1>Edit Printing</h1>
            <div class="border"></div>
            <div id="loading" style="display:none;text-align:center"><img src="images/ajax.gif" alt="loading..."/></div>
            <?php echo $err != '' ? '<div id="err">'.$err.'</div>' : '' ?>
            <form action="" method="post" name="form1" enctype="multipart/form-data">
                        <label for="txt_name">Name :</label> <input type="text" name="txt_name" class="textbox" id="txt_name" size="50" value="<?=$row["category_name"]?>" /><p class="form_clear"></p>
                        <p class="form_clear"></p>
                        <label for="file_upload">Image :</label> <input name="file_upload" id="file_upload" type="file" size="14" /><p class="form_clear"></p>
                        <p class="form_clear"></p>
                        <label></label>
                        <div><img src="../assets/images/souvenir/icon_<?php echo $row["category_url"];?>.png"/></div>
                        <label></label>
                        <p class="form_clear"></p>
                        <label for="txt_rank">Rank :</label> <input type="text" name="txt_rank" class="textbox" id="txt_rank" size="5" value="<?=$row["urutan"]?>" />
                        <p class="form_clear"></p>
                        <input type='button' name='cancel' value='Kembali' class="btn" onClick="backHome()">
                        <input type="submit" name="submit" value="Simpan" class="btn"/>
            </form>
            <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
