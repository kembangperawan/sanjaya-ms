<?
    session_start();
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include "title.php"; ?>
<link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/sms.min.css?d=201706150056">
<link rel="stylesheet" type="text/css" href="css/datepick.css">
<link rel="stylesheet" href="docs/style.css" type="text/css">
<script type="text/javascript" src="js/datepick.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script language="javascript" type="text/javascript">
$(function() {
    $('#btnAdd').click(function() {
        window.location = 'addSouvenirCategory.php';
        return false;
    });
});
</script>
    
</head>

<body>
<div id="contain">
    <? include("header.php"); ?>
    <div id="container">
        <div id="content">
            <?php //$err = $_REQUEST["err"]; if($err) echo '<p class="err">'.$err.'</p>'; ?>
            <div id="ajaxContent">
            <?
            // $res2 = mysql_query("select * from category ORDER BY urutan");
            // while ($row2 = mysql_fetch_array($res2))
            // {
                echo '<div style="display: block;" class="tab_content">';
                echo '<form action="souvenirCategoryMassUpdate.php" method="post" name="formMassUpdate" id="formMassUpdate" enctype="multipart/form-data">';
                echo "<br><h2 style=\"display: inline;\">Souvenir Category</h2>";
                echo "<input id=\"btnAdd\" type=\"button\" value=\"Add\" style=\"margin-left: 20px;\" />";
                echo "<input type=\"submit\" value=\"Mass Update\" style=\"margin-left: 20px;\" />";
                echo "<div class='border'></div>";
                // echo "<br><h2>".$row2["category_name"]."</h2><div class='border'></div>";
                $query = mysql_query('SELECT * FROM category order by urutan');
            while ($row = mysql_fetch_array($query)) {
                echo '<div class="product productPrinting">';
                echo '<h5><a target="_blank" href="http://'.$_SERVER['HTTP_HOST'].'/souvenir/'.$row["category_id"].'/'.strtolower(preg_replace("![^a-z0-9]+!i", "-", $row["category_name"])).'">'.$row["category_name"].'</a></h5><div class="border"></div>';
                echo '<div><img src="../assets/images/souvenir/icon_'.$row["category_url"].'.png"/></div>';
                echo '<a href="editSouvenirCategory.php?id='.$row["category_id"].'">Edit</a> | <a href="deleteSouvenirCategory.php?id='.$row["category_id"].'" onClick="if(confirm(\'Apakah Anda ingin menghapus category ini?\'))return true; else return false;">Delete</a>';
                echo '<div style="font-size: 12px;">Rank: <input type="text" name="txt_rank_'.$row["category_id"].'" size="5" value="'.$row["urutan"].'" /></div>';
                echo '</div>';
            }
                echo '</form>';
                echo '</div>';
                echo '<div class="clear"></div>';
            // }
            ?>
            </div>
        </div>
    </div>
</div>

        
</body>
</html>
