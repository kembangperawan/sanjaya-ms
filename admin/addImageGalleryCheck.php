<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
    
function getPost($name)
{
    if (isset($_POST[$name])) {
        return (get_magic_quotes_gpc() ? $_POST[$name] : addslashes($_POST[$name]));
    } else {
        return false;
    }
}

    $title = trim(getPost('txt_title'));
    $description = trim(getPost('txt_description'));
    $filename = str_replace(" ", "-", basename($_FILES["file_upload"]["name"]));
    $url = IMAGEGALLERY_IMG_DIR . $filename;
    $imageFileType = strtolower(pathinfo($url, PATHINFO_EXTENSION));
    $filename = strtolower(str_replace(" ", "-", str_replace("/", "-", str_replace(".", "-", $title)))) . "-" . date("Ymdhis") . "." . $imageFileType;
    $url = IMAGEGALLERY_IMG_DIR . $filename;
    $tag = trim(getPost('txt_tag'));
    $creatordatetime = date("Y-m-d h:i:s");
    $creatorid = trim($_SESSION["username"]);
    $errormessage = "";

if ($title == "") {
    $errormessage = "Image title cannot be empty.";
} elseif ($filename == "") {
    $errormessage = "No Image file chosen.";
} elseif ($tag == "") {
    $errormessage = "Tag cannot be empty.";
} else {
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
        $errormessage =  "Sorry, only JPG, JPEG, PNG and GIF files are allowed.";
    } else {
        $tmpName = $_FILES['file_upload']['tmp_name'];

        $watermark = imagecreatefrompng('../assets/images/watermark.png');
        switch ($imageFileType) {
            case 'jpg':
            case 'jpeg':
                $image = imagecreatefromjpeg($tmpName);
                break;
            case 'gif':
                $image = imagecreatefromgif($tmpName);
                break;
            case 'png':
                $image = imagecreatefrompng($tmpName);
                break;
        }
        imagecopy($image, $watermark, (imagesx($image) - imagesx($watermark)) / 2, (imagesy($image) - imagesy($watermark)) / 2, 0, 0, imagesx($watermark), imagesy($watermark));
        switch ($imageFileType) {
            case 'jpg':
            case 'jpeg':
                imagejpeg($image, $tmpName);
                break;
            case 'gif':
                imagegif($image, $tmpName);
                break;
            case 'png':
                imagepng($image, $tmpName);
                break;
        }

        $result = createThumbnail($tmpName, $url, IMAGEGALLERY_WIDTH);
        $result = createThumbnail($tmpName, str_replace(IMAGEGALLERY_IMG_DIR, IMAGEGALLERY_THUMBNAIL_DIR, $url), IMAGEGALLERYTHUMBNAIL_WIDTH);
        if ($result != "") {
            $query = "insert into imagegallery
                (Title
                , Description
                , URL
                , Tag
                , CreatorDateTime
                , CreatorID
                )
                values
                ('".$title."'
                ,'".$description."'
                ,'".$filename."'
                ,'".$tag."'
                ,'".$creatordatetime."'
                ,'".$creatorid."')";
            $result = mysql_query($query);
            $affectedrows = mysql_affected_rows();
            if ($affectedrows > 0) {
                $errormessage = "The file has been uploaded.";
            } else {
                $errormessage = "Sorry, there was an error uploading your file.";
            }
        } else {
            $errormessage = "Sorry, there was an error uploading your file.";
        }
    }
}
    $location = "Location:addImageGallery.php";
if ($errormessage != "") {
    $location = $location . "?err=" . $errormessage;
}
    header($location);
