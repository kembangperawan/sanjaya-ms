<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include "title.php"; ?>
<link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/sms.min.css?d=201706150056">
<link rel="stylesheet" type="text/css" href="css/datepick.css">
<link rel="stylesheet" href="docs/style.css" type="text/css">
<script type="text/javascript" src="js/datepick.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script language="javascript" type="text/javascript">
$(function() {
    $('a.ajaxMenu').click(function() {
        $('#ajaxContent').html('<p class="center"><img src="images/loadingLightbox.gif"/><br><i>Loading...</i><p>');
        //$('a.ajaxMenu').css("color","#da0909");
        //$(this).css("color","#910000");
        var url = $(this).attr('href');
        $('#ajaxContent').load(url);
        
        return false;
    });
    $('.err').click(function() {
        $(this).fadeOut('slow');
        return false;
    });
});
</script>

</head>

<body>
<div id="contain">
    <? include("header.php"); ?>
    <div id="container">
        <div id="content">
            <?php
            if (isset($err)) {
                $err = $_REQUEST["err"];
                if ($err) {
                    echo '<p class="err">'.$err.'</p>';
                }
            }
            
                ?>
            <?php
                    $err = "";
            if (isset($_REQUEST["err"])) {
                $err = $_REQUEST["err"];
            }
            if ($err != "") {
                echo '<p class="err">'.$err.'</p>';
            }
            ?>
            <div id="ajaxContent"></div>
        </div>
    </div>
</div>
</body>
</html>
