<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    include 'templateLayout.php';
    require_once 'library/config.php';
    require_once 'library/functions.php';
    $err = '';
if (isset($_GET['err']) && $_GET['err'] != '') {
    $err = $_GET['err'];
}
    $id = 0;
if (isset($_POST["submit"])) {
    $filename = str_replace(" ", "_", basename($_FILES["file_upload"]["name"]));
    $url = IMAGEGALLERY_IMG_DIR . $filename;
    $imageFileType = strtolower(pathinfo($url, PATHINFO_EXTENSION));
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
        header("Location:addHomeBanner.php?err=Format foto harus dalam format JPEG atau GIF");
        exit;
    }
    if ($_FILES['file_upload']['tmp_name'] != '') {
        $img_name   = $_FILES['file_upload']['name'];
        $tmp_name   = $_FILES['file_upload']['tmp_name'];
        $title_name = str_replace(" ", "_", 'Home Banner');
                
        $ext = strrchr($img_name, ".");

        $printing_slider_image = $title_name ."_". time() . strtolower($ext);
        $img_path = PRINTING_IMG_DIR . $printing_slider_image;
        $result = createThumbnail($tmp_name, $img_path, HOME_BANNER_WIDTH);
                
        if (!$result) {
            echo "Error uploading file";
            exit;
        }
    } else {
        header("location:addHomeBanner.php?err=Tidak ada file yang diupload");
        exit;
    }
    if (isset($_POST["chk_printing_slider_isactive"])) {
        $isactive = 1;
    } else {
        $isactive = 0;
    }
    mysql_query("insert into `printing_slider` (`printing_id`, `printing_slider_path`, `printing_slider_description`, `isactive`, `type`, `rank`, `url`)
								    values ('".$id."', '".$printing_slider_image."', '".$_POST["txt_printing_slider_description"]."', '".$isactive."', 1, '".$_POST["txt_rank"]."', '".$_POST["txt_url"]."')")
    or die(mysql_error());
    header("location:addHomeBanner.php");
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php getHead('Sanjaya MS - Add Home Banner'); ?>
    <style type="text/css">
    #err{
        background:#da0909;     
        border:1px solid #a6a6a6;       
        color:#fff; 
        font-size:12px; 
        font-weight:bold;
        margin-bottom:10px;         
        text-align:center;  
    }
    </style>
</head>

<body>
<div id="contain">
    <? getHeader(); ?>
    <div id="container">
        <div id="content">
            <h1>Home Banner</h1>
            <?php echo $err != '' ? '<div id="err">'.$err.'</div>' : '' ?>
            <form action="" method="post" onsubmit="return formcheck();" name="form1" enctype="multipart/form-data">
                <label for="txt_judul"></label> 
                
                <p class="form_clear"></p>
                <label for="file_upload">Slider Image :</label><input name="file_upload" id="file_upload" type="file" size="20" /><p class="form_clear"></p>
                <label for="txt_printing_slider_description">Description :</label>
                <input type="text" name="txt_printing_slider_description" class="textbox" id="txt_printing_slider_description" size="55%" /><p class="form_clear"></p>
                <label for="txt_url">URL :</label>
                <input type="text" name="txt_url" class="textbox" id="txt_url" size="55%" /><p class="form_clear"></p>
                <label for="chk_printing_slider_isactive">Active :</label>
                <input type="checkbox" name="chk_printing_slider_isactive" id="chk_printing_slider_isactive" style="margin: 12px;" checked><p class="form_clear"></p>
                <label for="txt_rank">Rank :</label>
                <input type="text" name="txt_rank" id="txt_rank" size="5" />
                <p class="form_clear"></p>
                <input type="submit" name="submit" value="Simpan" class="buttonsubmit" onClick="if(cek())return true;else return false;"/>
                <input type='button' name='cancel' value='Kembali' class="buttonsubmit" onClick="javascript: history.back(1)">
            </form> 
            <h5>Gallery Photo</h5>
            <form action="addHomeBannerCheck.php" method="post" name="form2">
                <div>
                <?php
                    $x = 0;
                    $res = mysql_query("select * from printing_slider where printing_id = '".$id."' order by type, rank");
                while ($row = mysql_fetch_array($res)) {
                    $x++;
                    echo "<div class='product productPrinting'>";
                    echo "<p><img src='../assets/images/".$row["printing_slider_path"]."'  border='1' width='270'></p>";
                    echo "<input type='text' name='txt_printing_slider_description".$x."' class='textbox' id='txt_printing_slider_description".$x."' style='width: 93%;' value='".$row["printing_slider_description"]."' />";
                    echo "<input type='text' name='txt_url".$x."' class='textbox' id='txt_url".$x."' style='width: 93%;' value='".$row["url"]."' />";
                    if (isset($row["isactive"])) {
                        if ($row["isactive"] == 1) {
                            echo "<input type='checkbox' name='chk_printing_slider_isactive".$x."' id='chk_printing_slider_isactive".$x."' checked> Active<br>";
                        } else {
                            echo "<input type='checkbox' name='chk_printing_slider_isactive".$x."' id='chk_printing_slider_isactive".$x."'> Active<br>";
                        }
                    } else {
                        echo "<input type='checkbox' name='chk_printing_slider_isactive".$x."' id='chk_printing_slider_isactive".$x."'> Active<br>";
                    }
                    echo " ";
                    echo "Rank <input type='text' name='txt_rank".$x."' id='txt_rank".$x."' size='5' value='".$row["rank"]."' /><br />";
                    echo "<a href='deleteHomeBanner.php?delete_id=".$row["printing_slider_id"]."' onClick='if(confirm(\"Apakah Anda ingin menghapus gambar ini?\"))return true; else return false;'><img src='../assets/images/deleteForm.gif' border='0'></a>";
                    echo " ";
                    echo "<input type='hidden' name='editId".$x."' id='editId".$x."' value='".$row["printing_slider_id"]."'>";
                    echo '<input type="image" class="clear" src="../assets/images/checkForm.gif" name="edit" value="Edit" onClick="if(confirm(\'Apakah Anda ingin mengubah keterangan foto ini?\'))return true; else return false;"/>';
                    echo "</div>";
                }
                    echo "<input type='hidden' name='sumx' id='sumx' value='".$x."'>";
                    echo "<input type='hidden' name='printing_id' id='printing_id' value='".$id."'>";
                if (mysql_num_rows($res)==0) {
                    echo "<p class='alert'>Untuk saat ini galeri foto belum ada.</p>";
                }
                ?>
                </div>
            </form>
            <div class="clear"></div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    function formcheck() {
        var txt_printing_slider_description = $('#txt_printing_slider_description').val();
        if (txt_printing_slider_description == '') {
            alert('Description harus diisi.');
            return false;
        }
        return true;
    }
</script>
</html>
