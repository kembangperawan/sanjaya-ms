<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include "title.php"; ?>
<link rel="stylesheet" type="text/css" href="css/css.css?d=201703211830">

</head>

<body>
<div id="contain">
	<div id="container">
	<div id="warningJavascript">
	  <br />
	  <div id="errLogin"><img src="images/warning.gif" /> Untuk mengakses situs ini, Anda harus mengaktifkan javascript pada browser Anda.</div>
	  <h2>Bagaimana cara mengaktifkan Javascript? </h2>
	  <p>Internet Explorer 6.X dan versi lebih rendah: </p>
	  <ol>
        <li>Klik menu Tools yang ada di bagian atas jendela Explorer. </li>
        <li>Klik Internet Options . </li>
        <li>Dari deretan tab yang ada di bagian atas, klik Security . </li>
        <li>Pada kotak pertama, Web Content “Zone”, klik Internet . </li>
        <li>Pada kotak berikutnya, “Security level for this zone”, klik Custom Level . </li>
        <li>Pada jendela yang terbuka, arahkan kursor ke bawah ke bagian “Scripting”, dan pada “Active scripting”, pilih “Enable” . </li>
        <li>Klik OK , kemudian klik OK lagi. Keluar lalu buka lagi browser Anda. Siap! </li>
      </ol>
	  <p>Internet Explorer 7: </p>
	  <ol>
        <li>Klik menu Tools yang ada di bagian atas jendela Explorer. </li>
        <li>Klik Internet Options . </li>
        <li>Dari deretan tab yang ada di bagian atas, klik Advanced . </li>
        <li>Pada jendela yang terbuka, arahkan kursor ke bawah sekitar setengahnya. Temukan simbol cangkir kopi. Fungsikan JavaScript! </li>
        <li>Klik OK , kemudian klik OK lagi. Keluar lalu buka lagi browser Anda. Siap! </li>
      </ol>
	  <p>Firefox: </p>
	  <ol>
        <li>Klik menu Tools yang ada di bagian atas jendela Firefox. </li>
        <li>Klik Options . </li>
        <li>Dari deretan ikon yang ada di bagian atas, klik Content . </li>
        <li>Pada bagian pertama, beri centang di sebelah “Enable Java” dan “Enable JavaScript”. </li>
        <li>Klik OK , kemudian keluar dan buka lagi Firefox. Siap! </li>
      </ol>
	  <p>Safari: </p>
	  <ol>
        <li>Dari menu Safari , klik Security . </li>
        <li>Beri centang pada “Use Plug-Ins”, “Java” dan “JavaScript”. Selesai! </li>
      </ol>
	  <div style="text-align:center;margin-top:20px;"><a href="index.php">Kembali ke Halaman Login</a></div>
	</div>
</div>
<script language="javascript" type="text/javascript">document.getElementById('cekJavascript').value="true";</script>
</body>
</html>
