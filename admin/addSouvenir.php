<?
if (session_id() == '') {
    session_start();
}
    include 'connect.php';
    include "security.php";
    include 'templateLayout.php';
    $err = '';
if (isset($_GET['err']) && $_GET['err'] != '') {
    $err = $_GET['err'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php getHead('Sanjaya MS - Add Souvenir'); ?>
    <style type="text/css">
    #err{
        background:#da0909;     
        border:1px solid #a6a6a6;       
        color:#fff; 
        font-size:12px; 
        font-weight:bold;
        margin-bottom:10px;         
        text-align:center;  
    }
    </style>
</head>

<body>
<div id="contain">
    <? getHeader(); ?>
    <div id="container">
        <div id="content">
            <h1>Add Souvenir</h1>
            <div class="border"></div>
            <div id="loading" style="display:none;text-align:center"><img src="images/ajax.gif" alt="loading..."/></div>
            <?php echo $err != '' ? '<div id="err">'.$err.'</div>' : '' ?>
            <form action="addSouvenirCheck.php" onsubmit="return formcheck();" method="post" name="formAddWork" id="formAddWork" enctype="multipart/form-data">
                <label for="txt_souvenir">Nama Souvenir :</label> <input type="text" name="txt_souvenir" class="textbox" id="txt_souvenir" size="50" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="txt_category"><span>*</span>Kategori :</label> 
                <select name="txt_category" id="txt_category">
                <?php
                $res = mysql_query("select * from category order by urutan asc");
                while ($row = mysql_fetch_array($res)) {
                    if ($row["category_id"] == $id) {
                        echo "<option value =".$row["category_id"]." selected>".$row["category_name"];
                    } else {
                        echo "<option value =".$row["category_id"].">".$row["category_name"];
                    }
                    echo "</option>";
                }
                ?>
                </select>
                <p class="form_clear"></p>
                <label for="txt_category">Keterangan :</label>
                <p class="form_clear"></p>
                <textarea name="txt_description" cols="80" rows="20" class="textarea" id="txt_description" style="width:300;height:100px;"></textarea>
                <p class="form_clear"></p>
                <label for="txt_meta_description">Tags Description :</label> <input type="text" name="txt_meta_description" class="textbox" id="txt_meta_description" size="100" value="" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="txt_keyword">Tags Keyword :</label> <input type="text" name="txt_keyword" class="textbox" id="txt_keyword" size="100" value="" /><p class="form_clear"></p>
                <p class="form_clear"></p>
                <label for="txt_rank">Rank :</label> <input type="text" name="txt_rank" class="textbox" id="txt_rank" size="5" value="" />
                <p class="form_clear"></p>
                <label for="chk_is_active">Is Active :</label> <input type="checkbox" name="chk_is_active" id="chk_is_active" checked="checked" value="1" style="margin-top: 10px;" />
                <p class="form_clear"></p>
                <label for="file_upload">Upload Foto :</label><input name="file_upload" id="file_upload" type="file" size="14" /><p class="form_clear"></p>
                <label></label>
                
                <input type='button' name='cancel' value='Kembali' class="btn" onClick="backHome()">
                <input type="submit" name="submit" value="Simpan" class="btn" id="submitForm"/>
            </form>
            <br>Contoh Description : "Beli TV LED SAMSUNG dengan Kualitas TERJAMIN, 100% ASLI, Harga MURAH, CICILAN 0%, dan GRATIS Pengiriman - Hanya di Bhinneka.Com" <br><br>
            Contoh Keyword : "Jual TV LED SAMSUNG - Daftar Lengkap Harga dan Spesifikasi Produk Bhinneka.Com, Product Price and Specification List, Kualitas TERJAMIN, 100% ASLI, Harga MURAH, CICILAN 0%, dan GRATIS Pengiriman, Buy, Sell, Jual, Beli"

        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    function backHome()
    {
        window.location.href="souvenir.php";
    }
    tinymce.init({
        mode: "exact",
        elements : ["txt_description"],
        themes: "modern",
        height:"250px",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        relative_urls: false,
        filemanager_title:"Responsive Filemanager",
        external_filemanager_path:"../filemanager/",
        external_plugins: { "filemanager" : "../../filemanager/plugin.min.js"},
        filemanager_access_key:"myPrivateKey",
        image_advtab: true
    });
    function formcheck() {
        var txt_souvenir = $('#txt_souvenir').val();
        var txt_description = tinyMCE.get('txt_description').getContent();
        if (txt_souvenir == '') {
            alert('Nama souvenir harus diisi.');
            return false;
        }
        if (txt_description == '') {
            alert('Description harus diisi.');
            return false;
        }
        return true;
    }
</script>
</html>
