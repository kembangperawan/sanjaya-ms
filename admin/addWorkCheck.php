<?	
	if(session_id() == '') {
		session_start();
	}
	include 'connect.php';
	include "security.php";
	require_once 'library/config.php';
	require_once 'library/functions.php';
	
	function getPost($name){
		if(isset($_POST[$name])) 
		  return (get_magic_quotes_gpc() ? $_POST[$name] : addslashes($_POST[$name]));
		else
		  return false;
	}

	$work = trim(getPost('txt_pekerjaan'));
	$keyword = trim(getPost('txt_keyword'));
	$date = trim(getPost('txt_tgl'));
	
	
	if($work && $date)
	{
		if($HTTP_POST_FILES['file_upload']['name'] != "" && $HTTP_POST_FILES['file_upload']['type'] != "image/jpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/pjpeg" && $HTTP_POST_FILES['file_upload']['type'] != "image/gif")
		{
			$err = 'Format foto harus dalam format JPEG atau GIF';
		}
		if(!$err)
		{
			if ($_FILES['file_upload']['tmp_name'] != '') 
			{
				$imgName   = $_FILES['file_upload']['name'];
				$tmpName   = $_FILES['file_upload']['tmp_name'];
				$titleName = str_replace(" ", "_", $work);
					
				$ext = strrchr($imgName, ".");
		
				$newName = $titleName ."_". time() . strtolower($ext);
				$imgPath = ALBUM_IMG_DIR . $newName;
				$result = createThumbnail($tmpName, $imgPath, ALBUM_WIDTH);
				if (!$result) {
					echo "Error uploading file";
					exit;
				}
			}
			else{
				$newName = "no_image.jpg";
			}	

			$query = "INSERT INTO `work` (`description` ,`image`, `dateline`, `keyword`)
								  VALUES ('".$work."','".$newName."',str_to_date('".$date."', '%d/%c/%Y'),'".$keyword."')";				
			$result = mysql_query($query) or die(mysql_error());
			$err = 'sukses';
		}
	}
	else $err = 'Masukkan semua data dengan benar!';
	header("Location:addWork.php?err=".$err."");
	exit;
?>
