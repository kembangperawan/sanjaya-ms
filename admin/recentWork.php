<?	
	if(session_id() == '') {
		session_start();
	}
	include 'connect.php';
	include "security.php";
	include 'templateLayout.php';
	$query = mysql_query('SELECT * FROM work WHERE dateline >= NOW() ORDER BY dateline');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php getHead('Sanjaya MS - Recent Work'); ?>
	<style type="text/css">
	#err{
		background:#da0909;		
		border:1px solid #a6a6a6;		
		color:#fff; 
		font-size:12px;	
		font-weight:bold;
		margin-bottom:10px;			
		text-align:center;	
	}
	</style>
</head>

<body>
<div id="contain">
	<? getHeader(); ?>
	<div id="container">
		<div id="content">
			<h1>Recent Work</h1>
			<div class="border"></div>
			<?php 
			while($row = mysql_fetch_array($query))
			{
				echo '<h2>'.$row["description"].'</h2><div class="border"></div>';
				echo '<p class="editDelete"><a href="editWork.php?id='.$row["work_id"].'">Edit</a> | <a href="deleteWork.php?id='.$row["work_id"].'">Delete</a></p>';
				echo '<div class="recentWorks">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td><img src="../assets/images/works/'.$row["image"].'" width="580"/></td>
								</tr>
								<tr>
									<td>
									<div class="infoCountdown"></div>
									<div class="finished">'.date("d M Y",strtotime($row["dateline"])).'</div>
									</td>
								</tr>
							</table>
						</div>
					';
			}
			?>
		</div>
	</div>
</div>
</body>
</html>