<?
    session_start();
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include "title.php"; ?>
<link href="uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/sms.min.css?d=201706150056">
<link rel="stylesheet" type="text/css" href="css/datepick.css">
<link rel="stylesheet" href="docs/style.css" type="text/css">
<script type="text/javascript" src="js/datepick.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function(){
            //Default Action
            $(".tab_content").hide(); //Hide all content
            $("ul.tabs li:first").addClass("active").show(); //Activate first tab
            $(".tab_content:first").show(); //Show first tab content
            
            //On Click Event
            $("ul.tabs li").click(function() {
            $("ul.tabs li a").removeClass("selected"); //Remove any "active" class
            $(this).find('a').addClass("selected"); //Add "active" class to selected tab
            $(".tab_content").hide(); //Hide all tab content
            var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
            $(activeTab).fadeIn(); //Fade in the active content
            return false;
            });
});


$(function() {
    $('a.ajaxMenu').click(function() {
        $('#ajaxContent').html('<p class="center"><img src="images/loadingLightbox.gif"/><br><i>Loading...</i><p>');
        //$('a.ajaxMenu').css("color","#da0909");
        //$(this).css("color","#910000");
        var url = $(this).attr('href');
        $('#ajaxContent').load(url);
        
        return false;
    });
    $('.err').click(function() {
        $(this).fadeOut('slow');
        return false;
    });
});
</script>
    
</head>

<body>
<div id="contain">
    <? include("header.php"); ?>
    <div id="container">
        <div id="content">
            <?php //$err = $_REQUEST["err"]; if($err) echo '<p class="err">'.$err.'</p>'; ?>
            <div id="ajaxContent">
            <?
            // $res2 = mysql_query("select * from category ORDER BY urutan");
            // while ($row2 = mysql_fetch_array($res2))
            // {
                echo '<div style="display: block;" class="tab_content">';
                echo '<form action="printingMassUpdate.php" method="post" name="formMassUpdate" id="formMassUpdate" enctype="multipart/form-data">';
                echo "<br><h2 style=\"display: inline;\">Printing</h2>";
                echo "<input type=\"submit\" value=\"Mass Update\" style=\"margin-left: 20px;\" />";
                echo "<div class='border'></div>";
                // echo "<br><h2>".$row2["category_name"]."</h2><div class='border'></div>";
                $query = mysql_query('SELECT * FROM printing order by rank');
            while ($row = mysql_fetch_array($query)) {
                echo '<div class="product productPrinting">';
                echo '<h5><a target="_blank" href="http://'.$_SERVER['HTTP_HOST'].'/printing/'.$row["printing_id"].'/'.strtolower(preg_replace("![^a-z0-9]+!i", "-", $row["product_printing"])).'">'.$row["product_printing"].'</a></h5><div class="border"></div>';
                echo '<p><img src="../assets/images/'.$row["product_image"].'" width="200"/></p>';
                echo '<a href="addPrintingGallery.php?id='.$row["printing_id"].'">Add Gallery</a> | <a href="editPrinting.php?id='.$row["printing_id"].'">Edit</a> | <a href="deletePrinting.php?id='.$row["printing_id"].'" onClick="if(confirm(\'Apakah Anda ingin menghapus gambar ini?\'))return true; else return false;">Delete</a>';
                echo '<div style="font-size: 12px;">Rank: <input type="text" name="txt_rank_'.$row["printing_id"].'" size="5" value="'.$row["rank"].'" /> Is Active? <input type="checkbox" name="chk_is_active_'.$row["printing_id"].'"'.($row["is_active"] == 1 ? ' checked="checked"' : '').' value="1" /></div>';
                echo '</div>';
            }
                echo '</form>';
                echo '</div>';
                echo '<div class="clear"></div>';
            // }
            ?>
            </div>
        </div>
    </div>
</div>

        
</body>
</html>
