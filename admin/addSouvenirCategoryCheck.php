<?
    session_start();
    include 'connect.php';
    include "security.php";
    require_once 'library/config.php';
    require_once 'library/functions.php';
    
function getPost($name)
{
    if (isset($_POST[$name])) {
        return (get_magic_quotes_gpc() ? $_POST[$name] : addslashes($_POST[$name]));
    } else {
        return false;
    }
}

    $name = trim(getPost('txt_name'));
    $filename = str_replace(" ", "_", basename($_FILES["file_upload"]["name"]));
    $url = IMAGEGALLERY_IMG_DIR . $filename;
    $rank = trim(getPost('txt_rank'));
if (!isset($rank)) {
    $rank = 0;
}
    
if ($name) {
    $imageFileType = strtolower(pathinfo($url, PATHINFO_EXTENSION));
    if ($imageFileType != "png") {
        $err =  "Format foto harus dalam format PNG";
    }
    if (!isset($err)) {
        if ($_FILES['file_upload']['tmp_name'] != '') {
            $img_name   = $_FILES['file_upload']['name'];
            $tmp_name   = $_FILES['file_upload']['tmp_name'];
            $title_name = str_replace(" ", "", $name);
                    
            $ext = strrchr($img_name, ".");
        
            $product_image = strtolower($title_name) . strtolower($ext);
            $img_path = SOUVENIR_IMG_DIR . 'icon_' . $product_image;
            $product_image = strtolower($title_name);
                $result = createThumbnail($tmp_name, $img_path, SOUVENIR_CATEGORY_WIDTH);
            if (!$result) {
                echo "Error uploading file";
                exit;
            }
        } else {
            $product_image = "no_image.jpg";
        }

        $query = "INSERT INTO `category` (
				`category_name`,
				`category_url`,
				`urutan`)
				VALUES (
				'".$name."',
				'".$product_image."',
				'".$rank."');
				";
        $result = mysql_query($query) or die(mysql_error());
        $err = 'sukses';
        header("Location:souvenirCategory.php");
        exit;
    }
} else {
    $err = 'Name harus diisi!';
}
    header("Location:addSouvenirCategory.php?err=".$err."");
    exit;
