<div id="header" class="hidden-xs">
    <div class="container" style="padding-top: 13px;">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div id="logo"><a href="{{URL('/')}}"><img src="{{URL('/assets/images/logo_sms.png')}}" width="100%"></a></div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="menu menudefault">
                    <ul id="topnav">
                        <li>
                            <a href="{{URL('/')}}" onmouseover="mopen('menuPrinting')" onmouseout="mclosetime()" {{(strtolower(getHeaderMenu()['isActive'])=="printing" or getHeaderMenu()['isActive']=="") ? "class=active" : "" }}>Printing</a>
                            <div class="menuDiv printing" id="menuPrinting" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
                                <div class="row">
                                    @for($i=0; $i < count(getHeaderMenu()['menuPrintingList']); $i++)
                                    @if($i==0 || $i % ceil(count(getHeaderMenu()['menuPrintingList']) / 3) == 0)
                                        <div class="col-md-4 col-sm-12 inner-menu">
                                    @endif
                                            <a href="{{URL(getHeaderMenu()['menuPrintingList'][$i]->link())}}" title="{{getHeaderMenu()['menuPrintingList'][$i]->keywords}}">{{getHeaderMenu()['menuPrintingList'][$i]->product_printing}}
                                            </a>
                                    @if($i==count(getHeaderMenu()['menuPrintingList'])-1 || (($i+1) % ceil(count(getHeaderMenu()['menuPrintingList']) / 3) == 0 && $i !=0))
                                        </div>
                                    @endif
                                    @endfor
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="{{URL('/souvenir')}}" onmouseover="mopen('menuSouvenir')" onmouseout="mclosetime()" {{(strtolower(getHeaderMenu()['isActive'])=="souvenir") ? "class=active" : "" }}>Souvenir</a>
                            <div class="menuDiv souvenir" id="menuSouvenir" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
                                <div class="row">
                                    @for($i=0; $i < count(getHeaderMenu()['menuCategoryList']); $i++)
                                    @if($i==0 || $i % ceil(count(getHeaderMenu()['menuCategoryList']) / 2) == 0)
                                        <div class="col-md-6 col-xs-12 inner-menu">
                                    @endif
                                            <a href="{{URL(getHeaderMenu()['menuCategoryList'][$i]->link())}}" title="{{getHeaderMenu()['menuCategoryList'][$i]->category_name}}">
                                                <img src="{{URL(getHeaderMenu()['menuCategoryList'][$i]->imageUrl())}}" style="height:25px; vertical-align:middle; margin-right:7px;">
                                                {{getHeaderMenu()['menuCategoryList'][$i]->category_name}}
                                            </a>
                                    @if($i==count(getHeaderMenu()['menuCategoryList'])-1 || (($i+1) % ceil(count(getHeaderMenu()['menuCategoryList']) / 2) == 0 && $i !=0))
                                        </div>
                                    @endif
                                    @endfor
                                </div>
                            </div>	
                        </li>
                        <li><a href="{{URL('/client')}}" {{(strtolower(getHeaderMenu()['isActive'])=="client") ? "class=active" : "" }}>Clients</a></li>
                        <li>
                            <a href="{{URL('/works/recent')}}" onmouseover="mopen('menuWorks')" onmouseout="mclosetime()" {{(strtolower(getHeaderMenu()['isActive'])=="works") ? "class=active" : "" }}>Works</a>
                            <div class="menuDiv works inner-menu" id="menuWorks" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
                                <a href="{{URL('/works/recent')}}">Recent Works</a>
                                <a href="{{URL('/works/last')}}">Last Works</a>
                            </div>
                        </li>
                        <li><a href="{{URL('/gallery')}}" {{(strtolower(getHeaderMenu()['isActive'])=="gallery") ? "class=active" : "" }}>Gallery</a></li>
                        <li><a href="{{URL('/contact')}}" {{(strtolower(getHeaderMenu()['isActive'])=="contact") ? "class=active" : "" }}>Contact Us</a></li>
                        <li>
                            <a href="javascript:void(0);" onmouseover="mopen('menuSearch', this)" onmouseout="mclosetime()"><img alt="Search" title="Search" src="/assets/images/icon-search-red.png" /></a>
                            <div class="menuDiv clearfix" id="menuSearch" onmouseover="mcancelclosetime()" onmouseout="mclosetime()" style="width: 250px; padding: 5px; right: 0;">
                                <input type="text" id="txtSearchFull" name="txtSearchFull" placeholder="Search" style="float: left;" />
                                <a id="btnSearchFull" href="{{URL('/search')}}" style="float: right;"><img alt="Search" title="Search" src="/assets/images/icon-search-white.png" /></a>
                            </div>
                        </li>
                    </ul>
                </div>	
            </div>
        </div>
    </div>	
</div>
<div class="listHeader hidden-sm hidden-xs" style="z-index: 9999900;"><p style="line-height: 0px;"></p></div>