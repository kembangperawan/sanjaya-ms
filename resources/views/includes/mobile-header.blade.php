<nav class="navbar navbar-default visible-xs">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle toggle-menu menu-right push-body" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{URL('/')}}"><img src="{{URL('/assets/images/logo_sms.png')}}" height="25px"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown-header clearfix">
          <div style="float: left;">Menu</div> 
          <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: #FFF; opacity: 1; height: 17px; line-height: 17px;"><span aria-hidden="true">&times;</span></button>
          <div></div>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Printing <span class="caret"></span></a>
          <ul class="dropdown-menu">
            @foreach(getHeaderMenu()['menuPrintingList'] as $key => $value)
            <li><a href="{{URL($value->link())}}">{{$value->product_printing}}</a></li>
            @endforeach
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Souvenir <span class="caret"></span></a>
          <ul class="dropdown-menu">
            @foreach(getHeaderMenu()['menuCategoryList'] as $key => $value)
            <li><a href="{{URL($value->link())}}"><img src="{{URL($value->imageUrl())}}" style="height:25px; vertical-align:middle; margin-right:7px;" />{{$value->category_name}}</a></li>
            @endforeach
          </ul>
        </li>
        <li><a href="{{URL('/client')}}">Clients</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Works <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{URL('/works/recent')}}">Recent Works</a></li>
            <li><a href="{{URL('/works/last')}}">Last Works</a></li>
          </ul>
        </li>
        <li><a href="{{URL('/gallery')}}">Gallery</a></li>
        <li><a href="{{URL('/contact')}}">Contact Us</a></li>
        <li style="padding: 0 15px;">
          <input type="text" id="txtSearchMobile" name="txtSearchMobile" placeholder="Search" />
          <a id="btnSearchMobile" href="{{URL('/search')}}" style="display: inline;"><img alt="Search" title="Search" src="/assets/images/icon-search-white.png" /></a>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>