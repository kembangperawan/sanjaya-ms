@extends('layouts.default')
@section('slider-content')
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Peta Sanjaya MS</h4>
        </div>
        <div class="modal-body">
            <img src="{{URL('/assets/images/peta_sanjaya.jpg')}}" width="100%"> <br />
            <p class="center">Jl. Setia - Pertama no. 6 Otista. Kampung Melayu, Jakarta Timur. 13330</p>
        </div>
        </div>
    </div>
    </div>
    <div id="contentSlider" class="themeWoodNoTop hidden-xs hidden-sm" style="margin-bottom: 80px;">
        <div class="labelInfo"><a href="#?w=700" class="poplight" data-toggle="modal" data-target="#myModal"><img src="{{URL('/assets/images/tag_peta.png')}}" onmouseover="this.src='{{URL('/assets/images/tag_peta_over.png')}}'" onmouseout="this.src='{{URL('/assets/images/tag_peta.png')}}'" ></a></div>	
        <div class="slash" style="top: 473px;"></div>
        <div class="gallery">
            <div id="noSliderSouvenir">
                <ul>
                    <li>
                        <div class="div" style="background:url({{URL('/assets/images/bgContact.jpg')}}) no-repeat top left;padding-top:50px;padding-bottom:150px;">
                            <div style="float:left;width:295px;">
                            &nbsp;
                            </div>
                            <div id="formSouvenir">
                                <form action="{{URL('/func/contactCheck.php')}}" method="POST" id="contactForm">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label for="txt_nama" class="visible-xs visible-sm">Nama :</label>
                                                </div>
                                                <div class="col-md-8 col-xs-8">
                                                    <input type="text" name="txt_nama" class="form-control" id="txt_nama" size="30" />
                                                    <span class="alert_form"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label for="txt_email" class="visible-xs visible-sm">Email :</label> 
                                                </div>
                                                <div class="col-md-8 col-xs-8">
                                                    <input type="text" name="txt_email" class="form-control" id="txt_email" size="30" /> 
                                                    <span class="alert_form"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label for="txt_phone" class="visible-xs visible-sm">Nomor Telp:</label>
                                                </div>
                                                <div class="col-md-8 col-xs-8">
                                                    <input type="text" name="txt_phone" class="form-control" id="txt_phone" size="30" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label for="txt_pesan" class="visible-xs visible-sm">Pesan Anda:</label>
                                                </div>
                                                <div class="col-md-8 col-xs-8">
                                                    <textarea name="txt_pesan" cols="30" rows="5" class="form-control" id="txt_pesan"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-offset-3 col-xs-9">
                                            <input type="submit" name="submit" class="btn" value="Kirim Pesan" />
                                        </div>
                                    </div>
                                    <div>
                                        <div id="err" style="display:none;color:red;text-align:center;margin-top:4px;margin-bottom:10px;font-size:12px;margin-left:0px;font-style:italic;line-height:1.4em;"></div>
                                        <div id="loading" style="display:none;text-align:center;margin-top:4px;margin-bottom:10px;font-size:12px;font-style:italic;">Sedang diproses... <img src="{{URL('/assets/images/loading.gif')}}" alt="loading..."/></div>
                                    </div>
                                </form>
                            </div>
                            <p class="clear"></p>
                            <br>
                            <div class="clear"></div> 
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear"></div>
@stop

@section('content')
<div class="container visible-xs visible-sm" style="margin-bottom: 20px;">
    <div class="row" style="background: #FFF; padding-bottom: 20px;">
        <div class="col-xs-12">
            <h1>Hubungi Kami</h1>
            <span>Senin - Jumat : 08.00 - 17.00</span>
            <div id="emailForm"></div>
            <div>
                <h3>Telepon :</h3>
                <p>
                    (021) - 851 9007 <br />
                    (021) - 829 0036
                </p>
            </div>
            <div>
                <h3>Contact Person :</h3>
                <p>
                    <b>Peter</b> <br />
                    0813 1606 8306 <br />
                    0818 069 11400(WA)
                </p>
            </div>
            <div>
                <h3>Email</h3>
                <p>
                    s_mitrasejati@yahoo.co.id <br />
                    admin@sanjayams.com
                </p>
            </div>
            <div>
                <h3>Alamat</h3>
                <input type="text" name="inputName" />
                <p>
                    <b>Sanjaya Mitra Sejati</b><br />
                    Jl. Setia - Pertama No.6<br />
                    Otista, Jakarta Timur 13330
                </p>
                <a href="#" data-toggle="modal" data-target="#myModal">Klik disini untuk melihat alamat peta</a>
            </div>
        </div>
    </div>
</div>
@stop

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
		    windowsWidth = $( window ).width();
            moveForm();
            $().ajaxStart(function() {
                $('#loading').show();
                $('#err').hide();
            }).ajaxStop(function() {
                $('#loading').hide();
                $('#err').fadeIn('slow');
            });

            $('#contactForm').submit(function() {
                $('#loading').show();
                $('#err').hide();
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    success: function(data) {
                         $('#loading').hide();
                        if(data == "sukses")
                        {
                            $('#err').html('Pesan Anda telah kami terima. Kami akan memproses pesan Anda dan menghubungi Anda secepatnya. =)');
                            document.getElementById('txt_nama').value="";
                            document.getElementById('txt_email').value="";
                            document.getElementById('txt_phone').value="";
                            document.getElementById('txt_pesan').value="";
                        }
                        else
                        $('#err').html(data);
                        $('#err').fadeIn('slow');
                    }
                })
                return false;
            });
        });
        
        $(window).on("resize", function(){
            if (windowsWidth){
                if (windowsWidth != $( window ).width()){
                    moveForm();
                }
            }
        });

        function moveForm(){
            if($(window).width() < 992 ) {
                $("#contactForm").appendTo("#emailForm");
            } else{
                $("#contactForm").appendTo("#formSouvenir");
            }
        }

        $('#emailForm .form-control').each(function(obj, i){
            console.log($(obj));
        });
    </script>
@stop