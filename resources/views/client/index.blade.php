@extends('layouts.default')
@section('slider-content')
<div id="contentSlider" class="themeWoodNoTop hidden-sm hidden-xs">
	<div class="slash" style="top: 275px;"></div>
	<div class="gallery">
		<div id="noSlider">
			<ul>
				<li>
					<div class="div">
						<img src="{{URL('/assets/images/percetakan_souvenir_clients.jpg')}}" alt="Klien Sanjaya Mitra Sejati - Percetakan dan Souvenir" border="0" />
						<div class="clear"></div> 
					</div>
				</li>
			</ul>
		</div>
		
	</div>
</div>
@stop
@section('content')
<div class="container" style="margin-bottom: 20px;">
    <div class="row" style="background: #FFF; padding: 20px 0;">
        <div class="col-xs-12">
            <div style="padding: 20px 0;" class="hidden-xs hidden-sm"></div>
            <h1>Our Clients</h1>
            <div class="border"></div>
            <div class="row client">
                <div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-antam.jpg')}}" title="Antam" alt="Antam" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-nindya.jpg')}}" title="Nindya Karya" alt="Nindya Karya" /></div>
  				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-tmli.jpg')}}" title="Tokio Marine" alt="Tokio Marine" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-beritagar.jpg')}}" title="Beritagar" alt="Beritagar" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-ef.jpg')}}" title="English First" alt="English First" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-daikin.jpg')}}" title="DAIKIN" alt="DAIKIN" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-mufg.jpg')}}" title="MUFG" alt="MUFG" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-erlangga.jpg')}}" title="Erlangga" alt="Erlangga" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-timken.jpg')}}" title="TIMKEN" alt="TIMKEN" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-telkom.jpg')}}" title="Telkom" alt="Telkom" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-bhinneka.jpg')}}" title="Bhinneka" alt="Bhinneka" /></div>
				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-johnrobertpowers.jpg')}}" title="John Robert Powers" alt="John Robert Powers" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-summarecon.jpg')}}" title="Summarecon" alt="Summarecon" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-bankbtn.jpg')}}" title="Bank BTN" alt="Bank BTN" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-ashleyhotel.jpg')}}" title="Ashley Hotel" alt="Ashley Hotel" /></div>
				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-rad.jpg')}}" title="Royal Dental" alt="Royal Dental" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-pttira.jpg')}}" title="PT Tira" alt="PT Tira" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-maybank.jpg')}}" title="Maybank" alt="Maybank" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-ipmi.jpg')}}" title="IPMI" alt="IPMI" /></div>
				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-gree.jpg')}}" title="Gree" alt="Gree" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-ericsson.jpg')}}" title="Ericsson" alt="Ericsson" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-antara.jpg')}}" title="ANTARA" alt="ANTARA" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-primagama.jpg')}}" title="Primagama" alt="Primagama" /></div>
				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client_pu.jpg')}}" title="Kementerian PU" alt="Kementerian PU" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-unitedairlines.jpg')}}" title="United Airlines" alt="United Airlines" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-insight.jpg')}}" title="Insight" alt="Insight" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client_kpmg.jpg')}}" title="KPMGl" alt="KPMGI" /></div>
				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-toto.jpg')}}" title="TOTO" alt="TOTO" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-cvindonesia.jpg')}}" title="CV Indonessia" alt="CV Indonesia" /></div>
				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-pbtaxand.jpg')}}" title="PB Taxand" alt="PB Taxand" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-kapalapiglobal.jpg')}}" title="Kapal Api Global" alt="Kapal Api Global" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-quipper.jpg')}}" title="Quipper" alt="KPMGI" /></div>
				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-avnet.jpg')}}" title="AVNET" alt="AVNET" /></div>
 				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-waterbom.jpg')}}" title="Waterbom Jakarta" alt="Waterbom jakarta" /></div>
				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-stbalia.jpg')}}" title="STBA LIA" alt="STBA LIA" /></div>
				<div class="col-md-2 col-sm-3 col-xs-4"><img src="{{URL('/assets/images/client/client-composite.jpg')}}" title="Composite Solution" alt="Composite Solution" /></div>
            </div>
        </div>
    </div>
</div>
@stop

@section('script')
@stop