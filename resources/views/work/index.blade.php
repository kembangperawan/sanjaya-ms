@extends('layouts.default')
@section('content')
	<div class="row" style="background: #FFF;">
		<!-- content start -->
		<div class="col-xs-12">
			<h1>{{$workTitle}}</h1>
		</div>
        <div class="col-xs-12">
			<div class="border"></div>
			<div class="row">
                <div class="col-md-8 col-xs-12">
                    {{ count($workList) > 0 ? "" : "No data." }}
                    <div class="row works">
                        @foreach($workList as $key => $value)
                            <div class="col-xs-12">
                                <h2>{{$value->description}}</h2>
                                <div class="img-border" style="padding: 10px; background: url('{{URL('/assets/images/bgWood_2.jpg')}}') repeat;"><img src="{{URL($value->imageUrl())}}" alt="{{$value->description}}" title="{{$value->description}}" /></div>
                                @if($workState == "recent")
                                <div class="infoCountdown" data-date="{{$value->dateline}}">
                                    {{$value->dateString()}}
                                </div>
                                @else
                                <div class="infoFinished">
                                    {{$value->dateString()}}
                                </div>
                                @endif
                                <div style="margin-bottom: 20px;"></div>
                            </div>
                        @endforeach
                        <div class="col-xs-12">{{ $workList->links() }}</div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <a href="{{URL('/works/last')}}"><img width="100%" src="{{URL('/assets/images/works/banner_last_works.jpg')}}"></a>
                </div>
			</div>
		</div>
		<!-- content end -->
	</div>
@stop

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('.infoCountdown').each(function(i, obj) {
            if ($(obj).attr('data-date') != null && $(obj).attr('data-date') != ""){
                var splitDate = $(obj).attr('data-date').split('-');
                var endDate = new Date(splitDate[0], splitDate[1] - 1, splitDate[2]);
                $(obj).countdown({until: endDate});
            }
        });
    });
</script>
@stop