@extends('layouts.default')
@section('slider-content')
@if(count($printingSlider) > 0)
	<div id="contentSlider" class="themeWoodNoTop">
		<div class="slash"></div>
		<div id="headSlider">
			<div id="printingSlider" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner" role="listbox">
					@foreach($printingSlider as $key => $value)
					<div class="item item-carousel">
						<a href="{{$value->url}}" title="{{$value->printing_slider_description}}"><img class="img-slider" style="margin: 0 auto;" src="{{URL($value->imgUrl())}}" alt="{{$value->printing_slider_description}}"/></a>
					</div>
					@endforeach
				</div>
				<div id="sliderController">
					<a class="left carousel-control" style="background-image: none;" href="#printingSlider" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" style="background-image: none;" href="#printingSlider" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
@endif
@endsection
@section('content')
	<div id="mainContent" class="row" style="background: #FFF; padding: 10px 5px 35px 5px;">
		<div class="col-xs-12">
			<h2>Cetakan apa yang Anda butuhkan?</h2>
			<div class="border"></div>
			<div class="row">
				@foreach($printingList as $key => $value)
				<div class="col-md-3 col-xs-6">
					<div class="product">
						<a href="{{URL($value->link())}}" title="{{$value->keywords}}">
							<img src="{{URL($value->imageUrl())}}" alt="{{$value->keywords}}" />
							<h3>{{$value->product_printing}}</h3>
						</a>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
@stop
@section('script')
	<script type="text/javascript">
	$(document).ready(function(){
		checkSlider();
		showSlider("item-carousel");
		moveSlider();
		adjustSlash();

	});
	$(window).on("load", function(){
		adjustSlash();
	});
	$(window).on("resize", function(){
		moveSlider();
		adjustSlash();
	});
	function moveSlider(){
		if($(window).width() < 768 ) {
			$("#contentSlider").removeClass("themeWoodNoTop");
			$("#mainContent").css({"background-color":"#FFF", "padding-top":"30"});
		} else{
			$("#contentSlider").addClass("themeWoodNoTop");
			$("#mainContent").css({"background-color":"#FFF", "padding-top":"60px"});
			checkSlider();
		}
	}
	function showSlider(item){
		$("." + item).first().addClass("active");
	}
	function checkSlider(){
		if(!$(".item-carousel")[0]){
			$(".slash").hide();
			$("#mainContent").css({"background-color":"#FFF", "padding-top":"0"});
		}
		if($(".item-carousel").length <= 1){
			$("#sliderController").hide();	
		}
	}
	function adjustSlash(){
		var minHeight = '';
		if($(window).width() < 768 ) {
			minHeight = 10;
		} else {
			minHeight = 15;
		}
		var slashPosition = $("#contentSlider").height() - ($(".slash").height() / 2) - minHeight;
		var slashPx = slashPosition + "px";
		$(".slash").css({"top": slashPx});
	}
	</script>
@endsection