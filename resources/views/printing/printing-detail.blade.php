@extends('layouts.default')
@section('slider-content')
<div id="contentSlider" class="themeWoodNoTop">
	<div class="slash"></div>
	@if($printing->min_qty > 0)
	<div class="labelTag">Min qty {{$printing->min_qty}} pcs</div>
	@endif
	<div id="headSlider">
		<div id="printingSlider" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner" role="listbox">
				@foreach($printingSlider as $key => $value)
				<div class="item item-carousel">
					<img class="img-slider" style="margin: 0 auto;" src="{{URL($value->imgUrl())}}" alt="{{$value->printing_slider_description}}"/>
				</div>
				@endforeach
			</div>
			<div id="sliderController">
				<a class="left carousel-control" style="background-image: none;" href="#printingSlider" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" style="background-image: none;" href="#printingSlider" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
</div>
@endsection
@section('content')
	<div id="mainContent" class="row" style="background: #FFF; padding-top: 60px;">
		<div class="col-xs-12">
			<h1>{{$printing->product_printing}}</h1>
		</div>
		<div class="col-xs-12">
			<div class="row">
				@if(count($printingSliderThumb) > 0 || (isset($printing->announcement_board) && $printing->announcement_board != ""))
				<div class="col-xs-12 col-md-8">
				@else
				<div class="col-xs-12">
				@endif
					@if($printing->min_qty > 0)
					<div id="lblInfo" style="margin-bottom: 10px;">
						<span style="color: #D00018; font-weight: bold;">Min quantity {{$printing->min_qty}} pcs</span>
					</div>
					@endif
					<div>
						{!!$printing->product_content!!}
					</div>
				</div>
				@if(count($printingSliderThumb) > 0 || (isset($printing->announcement_board) && $printing->announcement_board != ""))
				<div class="col-xs-12 col-md-4">
					@if(count($printingSliderThumb) > 0)
					<div class="row" id="rightGallery">
						<div class="galleryProduct" id="mainImage" style="margin: 5px auto;">
							<img src="#" alt=""/>
							<p></p>
						</div>
						<div id="thumbGallery">
							<ul class="thumbGalleryProduct">
								@foreach($printingSliderThumb as $key => $value)
								<li>
									<a class="thumbImage" href="{{URL($value->imgUrl())}}" title="{{$value->printing_slider_description}}">
										<img src="{{URL($value->imgUrl())}}" style="width: 80px; height: 62px;" alt="{{$value->printing_slider_description}}" />
									</a>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
					@endif
					@if(isset($printing->announcement_board) && $printing->announcement_board != "")
					<div class="row">
						<div class="papanPengumuman" style="margin: 20px auto 0 auto; padding: 15px 30px;">
							{!!$printing->announcement_board!!}
						</div>
					</div>
					@endif
				</div>
				@endif
			</div>
		</div>
		<div class="col-xs-12">
			<h2>Cetakan apa yang Anda butuhkan?</h2>
			<div class="border"></div>
			<div class="row">
				@foreach($printingList as $key => $value)
				<div class="col-md-2 col-xs-4">
					<div class="product">
						<a href="{{URL($value->link())}}" title="{{$value->keywords}}">
							<img src="{{URL($value->imageUrl())}}" alt="{{$value->keywords}}" class="img-rounded" />
							<h3>{{$value->product_printing}}</h3>
						</a>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
@stop
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		checkSlider();
		if($(".galleryProduct").length > 0){
			var firstImg = $('.thumbGalleryProduct').first().find('a')[0];
			$(".galleryProduct img").attr({src: firstImg.getAttribute('href')});
			$(".galleryProduct p").text(firstImg.getAttribute('title'));
			$("ul.thumbGalleryProduct li a").click(function() {
				var mainImage = $(this).attr("href");
				var mainTitle = $(this).attr("title");
				$(".galleryProduct img").attr({ src: mainImage });
				$(".galleryProduct p").text(mainTitle);
				$(".thumbGalleryProduct .active").removeClass("active");
				$(this).parent().addClass("active");
				return false;		
			});
		}
		showSlider("item-carousel");
		moveSlider();
		adjustSlash();
	});
	$(window).on("load", function(){
		adjustSlash();
	});
	$(window).on("resize", function(){
		moveSlider();
		adjustSlash();
	});
	function moveSlider(){
		if($(window).width() < 768 ) {
			$("#contentSlider").removeClass("themeWoodNoTop");
			$("#lblInfo").show();
			$("#mainContent").css({"background-color":"#FFF", "padding-top":"0"});
			$("#thumbGallery").css({"margin":"0 20px"});
			$(".labelTag").hide();
		} else{
			$("#contentSlider").addClass("themeWoodNoTop");
			$("#lblInfo").hide();
			$("#mainContent").css({"background-color":"#FFF", "padding-top":"60px"});
			$("#thumbGallery").css({"margin":"0"});
			$(".labelTag").show();
			checkSlider();
		}
	}
	function showSlider(item){
		$("." + item).first().addClass("active");
	}
	function checkSlider(){
		if(!$(".item-carousel")[0]){
			$(".slash").hide();
			$("#mainContent").css({"background-color":"#FFF", "padding-top":"0"});
		}
		if($(".item-carousel").length <= 1){
			$("#sliderController").hide();	
		}
	}
	if(!$(".thumbImage")[0]){
		$("#rightGallery").hide();
	}
	function adjustSlash(){
		var minHeight = '';
		if($(window).width() < 768 ) {
			minHeight = 10;
		} else {
			minHeight = 15;
		}
		var slashPosition = $("#contentSlider").height() - ($(".slash").height() / 2) - minHeight;
		var slashPx = slashPosition + "px";
		$(".slash").css({"top": slashPx});
	}
</script>
@stop