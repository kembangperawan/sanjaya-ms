@extends('layouts.default')
@section('page-css')
  <link rel="stylesheet" href="{{URL('/assets/css/gallery.min.css?d=201704211651')}}" type="text/css" />
@endsection
@section('content')
<div class="row" style="background: #FFF;">
  <div class="col-xs-12">
			<h1>Image Gallery</h1>
	</div>
  <div class="col-xs-12">
    <div class="border"></div>
  </div>
  <div class="col-xs-12">
    <div class="row">
      @if(isset($tag))
        <div class="col-xs-4 pull-left" style="margin-top: 10px;">
          <a class="btn" href="{{URL('/gallery')}}">Back to Gallery</a>
        </div>
      @endif
      <div class="col-lg-6 col-md-8 col-xs-12 pull-right">
        <form method="get" action="{{URL('/gallery')}}">
          <div class="input-group">
            <input class="form-control" type="text" name="tag" style="padding: -12px; height: 30px;" value="{{$tag}}" />
            <div class="input-group-btn" style="padding-top: 10px;">
              <button class="btn" type="submit">Search</button>
            </div>
          </div>  
        </form>
      </div>
    </div>
    <div class="col-xs-12" style="margin: 10px 0;">
    @if(isset($allTag))
    <a style="color: #6B4200; text-decoration: none; font-weight: bold;" href="#tags" data-toggle="collapse">Tags</a>
    <div style="margin-top: 5px;">
      <div id="tags" class="collapse in">
        @foreach($allTag as $key => $value)
        <div class="galleryTag">
          <a href="{{URL('/gallery?tag='.$value)}}">{{$value}}</a>
        </div>
        @endforeach
        <div style="clear: both;"></div>
      </div>
    </div>
    @endif
    </div>
  </div>
  <div class="col-xs-12">
    @if(count($gallery_list) > 0)
      <div class="row">
        <div class="col-xs-12" style="padding: 30px 15px;">
          <div id="galleryItem" class="grid">
            @if(isset($gallery_list))
              @foreach($gallery_list as $key => $value)
                <div class="item">
                  <div class="hovereffect">
                    <img src="{{URL($value->thumbnailUrl())}}" />
                    <div class="overlay">
                      <h2>{{$value->Title}}</h2>
                      <a class="info" href="{{URL($value->link())}}">See More</a>
                    </div>
                  </div>
                </div>
              @endforeach
            @endif
          </div>
        </div>
      </div>
    @else
    <div style="text-align: center; padding: 30px 15px;">
      <span>No Data Found</span>
    </div>
    @endif
  </div>
</div>
<nav id="page-nav">
  <a href="{{URL('/gallery')}}?tag={{$tag}}&page={{$page + 1}}"></a>
</nav>
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/jquery.infinitescroll.min.js') }}"></script>
    <script>
      $(function(){
    
        var $container = $('.grid');
        
        $container.imagesLoaded(function(){
          $container.masonry({
            itemSelector: '.item',
            isAnimated: true
          });
        });
        
        $container.infinitescroll({
          navSelector  : '#page-nav',    // selector for the paged navigation 
          nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
          itemSelector : '.item',     // selector for all items you'll retrieve
          loading: {
              finishedMsg: 'No more pages to load.',
              img: '{{ URL::asset('/assets/images/infinite-scroll-loading.gif') }}'
            }
          },
          // trigger Masonry as a callback
          function( newElements ) {
            // hide new items while they are loading
            var $newElems = $( newElements ).css({ opacity: 0 });
            // ensure that images load before adding to masonry layout
            $newElems.imagesLoaded(function(){
              // show elems now they're ready
              $newElems.animate({ opacity: 1 });
              $container.masonry( 'appended', $newElems, true ); 
            });
          }
        );
        
      });
    </script>
@endsection