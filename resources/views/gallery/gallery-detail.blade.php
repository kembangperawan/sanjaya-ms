@extends('layouts.default')
@section('content')
	<div class="row" style="background: #FFF;">
		<div class="col-xs-12">
			<h1>{{$gallery->Title}}</h1>
		</div>
		<div class="col-xs-12" style="padding-bottom: 30px;">
			<div class="row">
                <div class="col-xs-12">
                    <div class="border"></div>
                </div>
                <div class="col-xs-4 pull-left" style="margin-top: 10px;">
                    <a class="btn" href="{{URL('/gallery')}}">Back to Gallery</a>
                </div>
				<div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12" style="margin: 30px 0;">
                            <img class="img-responsive" style="margin: 0 auto;" src="{{URL($gallery->imageUrl())}}" alt="{{$gallery->Title}}"/>
                        </div>
                        <div class="col-xs-12">
                            <p>
                                {{$gallery->Description}}
                            </p>
                        </div>
                        <div class="col-xs-12">
                            <div style="float: left; margin-right: 5px;">Tags:</div>
                            @foreach($gallery->imgTag() as $key => $value)
                                <div class="galleryTag">
                                    <a href="{{URL('/gallery?tag='.$value)}}">{{$value}}</a>
                                </div>
                            @endforeach
                            <div style="clear: both;"></div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
@stop