<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{ isset($pageObj) && $pageObj['page_title'] != '' ? $pageObj['page_title'] : Config('pageinfo.page_title') }}</title>
<meta name="description" content="{{ isset($pageObj) && $pageObj['meta_description'] != '' ? $pageObj['meta_description'] : Config('pageinfo.meta_description') }}">
<meta name="keywords" content="{{ isset($pageObj) && $pageObj['meta_keywords'] != '' ? $pageObj['meta_keywords'] : Config('pageinfo.meta_keywords') }}"/>
<meta name="language" content="en-us , English" />
<meta name="robots" content="index , follow , all" />
<meta name="location" content="Indonesia" />
<link rel="icon" href="{{URL('/assets/images/sms.ico')}}" type="image/sms-icon" />
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet"> 
<link rel="stylesheet" href="{{URL('/assets/css/jquery.countdown.min.css')}}" type="text/css" />
<link rel="stylesheet" href="{{URL('/assets/css/bootstrap.min.css')}}" type="text/css" />
<link rel="stylesheet" href="{{URL('/assets/css/sms.min.css?d=201706151821')}}" type="text/css" />
<link rel="stylesheet" href="{{URL('/assets/css/jPushMenu.min.css?d=201704111347')}}" type="text/css" />
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-16186500-8', 'sanjayams.com');
  ga('send', 'pageview');
</script>
@yield('page-css')
</head>
<body>
<div id="menubg" class="menubg"></div>
@include('includes.mobile-header')
@include('includes.full-header')
@yield('slider-content')
<div class="container">
	@yield('content')
</div>
@include('includes.full-footer')
<script type="text/javascript" src="{{URL('/assets/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{URL('/assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL('/assets/js/menu.min.js?d=201706151730')}}"></script>
<script type="text/javascript" src="{{URL('/assets/js/jquery.countdown.min.js')}}"></script>
<script type="text/javascript" src="{{URL('/assets/js/jPushMenu.min.js?d=201706151821')}}"></script>
@yield('script')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.toggle-menu').jPushMenu();
	});
  $('#btnSearchFull').click(function() {
      window.location = "{{URL('/search')}}?q=" + $('#txtSearchFull').val();
      return false;
  });
  $('#btnSearchMobile').click(function() {
      window.location = "{{URL('/search')}}?q=" + $('#txtSearchMobile').val();
      return false;
  });
  $('#txtSearchFull').on('keypress', function (e) {
    if(e.which === 13){
      $(this).attr("disabled", "disabled");
      $('#btnSearchFull').click();
      $(this).removeAttr("disabled");
    }
  });
  $('#txtSearchMobile').on('keypress', function (e) {
    if(e.which === 13){
      $(this).attr("disabled", "disabled");
      $('#btnSearchMobile').click();
      $(this).removeAttr("disabled");
    }
  });
</script>
</body>
</html>