@extends('layouts.default')
@section('content')
  <style>
    @media (min-width: 993px){
      #spnCategoryName {
        font-size: 24px;
      }
    }
    @media (max-width: 992px){
      #containerSouvenir {
        margin-bottom: 50px;
      }
      #spnCategoryName {
        font-size: 18px;
      }
      #divDropdownKategory {
        margin-top: 10px;
      }
    }
    @media (min-width: 640px) and (max-width: 992px) {
      .divProductSouvenirContainer{
        float: left !important;
        width: 50% !important;
      }
    }
    @media (min-width: 360px) and (max-width: 992px) {
      .productSouvenir {
        float: left !important;
        width: 50% !important;
      }
    }
    @media (min-width: 360px) and (max-width: 400px) {
      .productSouvenir {
        float: left !important;
        width: 50% !important;
      }
      p.newItems, p.bestPrice, p.hotItems {
        background-size: 60% auto;
      }
    }
    @media (min-width: 320px) and (max-width: 399px) {
      .dropdown-menu-columns-2 {
        min-width: 230px;
        font-size: 10px;
      }
      .divMenuColumn2 {
        width: 50%;
        float: left;
      }
      .lnkNavigationCategory img {
        height: 12px !important;
        margin-right: 3px !important;
      }
    }
    @media (min-width: 400px) {
      .dropdown-menu-columns-2 {
        min-width: 375px;
      }
      .divMenuColumn2 {
        width: 50%;
        float: left;
      }
    }
    .dropdown-menu li a {
      padding: 5px 8px;
      cursor: pointer;
    }
    .multi-column-dropdown {
      list-style: none;
      margin: 0px;
      padding: 0px;
    }
    .multi-column-dropdown li a {
      display: block;
      clear: both;
      color: #000;
    }
    .multi-column-dropdown li a:hover {
      text-decoration: none;
      background-color: #DDD;
    }
  </style>
	<div id="mainContent" class="row" style="background: #FFF;">
		<div class="col-xs-12">
			<h2>Printing {{$printingSearchResultTitle}}</h2>
			<div class="border"></div>
			<div class="row">
				@foreach($printingList as $key => $value)
				<div class="col-md-3 col-xs-6">
					<div class="product">
						<a href="{{URL($value->link())}}" title="{{$value->keywords}}">
							<img src="{{URL($value->imageUrl())}}" alt="{{$value->keywords}}" />
							<h3>{{$value->product_printing}}</h3>
						</a>
					</div>
				</div>
				@endforeach
			</div>
		</div>
    <div class="container">
      <div class="row" style="background: #FFF; padding: 0px;">
          <div style="margin: 0 15px;">
            <h2>Souvenir {{$souvenirSearchResultTitle}}</h2>
            <div class="border"></div>
          </div>
          <div class="clearfix"></div>
      <div class="row" style="margin: auto;">
        <div id="divSouvenir" class="tab_content col-xs-12">
          <div id="divContentProductSouvenir">
            @include('souvenir.ajax-souvenir')
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      </div>
    </div>
	</div>
@endsection