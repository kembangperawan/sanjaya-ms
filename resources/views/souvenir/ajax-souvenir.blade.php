<div style="margin: 20px auto;">
    @for($i=0; $i < count($souvenirList); $i++)
        <div class="product productSouvenir col-md-4" style="margin-bottom: 20px;">
            @if($souvenirList[$i]->status==1)
            @elseif($souvenirList[$i]->status==2)
                <p class="newItems"></p>
            @elseif($souvenirList[$i]->status==3)
                <p class="bestPrice"></p>
            @elseif($souvenirList[$i]->status==4)
                <p class="hotItems"></p>
            @endif
            <a href="{{URL($souvenirList[$i]->link())}}" title="{{$souvenirList[$i]->souvenir_name}}">
                <img style="width: 100%;" src="{{URL($souvenirList[$i]->imageUrl())}}" alt="{{$souvenirList[$i]->souvenir_name}}" />
                <h3>{{$souvenirList[$i]->souvenir_name}}</h3>
            </a>
        </div>
    @endfor
</div>