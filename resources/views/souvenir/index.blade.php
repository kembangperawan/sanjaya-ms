
@extends('layouts.default')
@section('slider-content')

<style>
	@media (min-width: 993px){
		#divSouvenir {
			margin-top: 80px !important;
		}
		#spnCategoryName {
			font-size: 24px;
		}
	}
	@media (max-width: 992px){
		#divContentBottom {
			margin: 0 -15px;
		}
		#containerSouvenir {
			margin-bottom: 50px;
		}
		#spnCategoryName {
			font-size: 18px;
		}
		#divDropdownKategory {
			margin-top: 10px;
		}
	}
	@media (min-width: 640px) and (max-width: 992px) {
		.divProductSouvenirContainer{
			float: left !important;
			width: 50% !important;
		}
	}
	@media (min-width: 360px) and (max-width: 992px) {
		.productSouvenir {
			float: left !important;
			width: 50% !important;
		}
	}
	@media (min-width: 360px) and (max-width: 400px) {
		.productSouvenir {
			float: left !important;
			width: 50% !important;
		}
		p.newItems, p.bestPrice, p.hotItems {
			background-size: 60% auto;
		}
	}
	@media (min-width: 320px) and (max-width: 399px) {
		.dropdown-menu-columns-2 {
			min-width: 230px;
			font-size: 10px;
		}
		.divMenuColumn2 {
			width: 50%;
			float: left;
		}
		.lnkNavigationCategory img {
			height: 12px !important;
			margin-right: 3px !important;
		}
	}
	@media (min-width: 400px) {
		.dropdown-menu-columns-2 {
			min-width: 375px;
		}
		.divMenuColumn2 {
			width: 50%;
			float: left;
		}
	}
	.dropdown-menu li a {
		padding: 5px 8px;
		cursor: pointer;
	}
	.multi-column-dropdown {
		list-style: none;
		margin: 0px;
		padding: 0px;
	}
	.multi-column-dropdown li a {
		display: block;
		clear: both;
		color: #000;
	}
	.multi-column-dropdown li a:hover {
		text-decoration: none;
		background-color: #DDD;
	}
</style>
@if(isset($souvenir) && $souvenir != null)
<div class="slash"></div>
<div id="contentSlider" class="themeWoodNoTop hidden-sm hidden-xs">
	<div class="gallery">
		<div id="divContentTop">
			<div id="containerSouvenir">
				<div id="contentSouvenir">
					<h6><a href="{{URL("/souvenir")}}">SOUVENIR</a> - {{$souvenirCategoryName}}</h6>
					<div class="border"></div>
					<div class="coloumnLeftSouvenir col-md-6"  style="padding:0px;">
						<div class="gallerySouvenir">
							<img src="{{URL($souvenirImageList[0]->imageUrlProduct())}}" alt="{{$souvenirImageList[0]->gallery_keyword}}" title="{{$souvenirImageList[0]->gallery_keyword}}"/>
							<p>{{$souvenirImageList[0]->gallery_keyword}}</p>
						</div>
						<div>
							<ul class="thumbGallerySouvenir">
								@foreach ($souvenirImageList as $souvenirImageList)
									<li>
										<a href="{{URL($souvenirImageList->imageUrlProduct())}}" title="{{$souvenirImageList->gallery_keyword}}">
											<img src="{{URL($souvenirImageList->imageUrlProduct())}}" alt="{{$souvenirImageList->gallery_keyword}}" />
										</a>
									</li>
								@endforeach						
							</ul>
						</div>
					</div>
					<div class="coloumnRightSouvenir col-md-6">
						<h3>{{$souvenir->souvenir_name}}</h3>
						<p>{!! $souvenir->description !!}</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@else
<div class="slash hidden-sm hidden-xs"></div>
<div id="contentSlider" class="themeWoodNoTop hidden-sm hidden-xs">
	<div class="gallery">
		<div id="noSliderSouvenir" style="margin-bottom: 80px;">
			<ul>
				<li>
					<div class="div" style="background:url({{URL('/assets/images/souvenir/bgSouvenir.jpg')}}) no-repeat top left;padding-top:300px;">
						<div style="float:left;width:515px;color:#000;">
							Kami menjual berbagai macam aneka barang promosi dan souvenir promosi untuk keperluan promosi perusahaan Anda,
							seperti pulpen, pensil, tumbler, jam, tempat kartu nama, flash disk / USB, dan lainnya. <br> <br>
							Berbagai macam pilihan barang promosi dan souvenir yang kami sediakan dapat membantu memberikan banyak alternatif pilihan kepada Anda.<br>
							Silahkan isi form disamping, untuk pertanyaan dan pemesanan barang.<br />
							<div id="slideProjectSouvenir">
								<div style="text-align: right;"><img src="{{URL('/assets/images/bgProjectSouvenir.png')}}" /></div>
								<div style="padding: 10px; background: #F7F6F4; -webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px;">
									<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
										<div class="carousel-inner" role="listbox">
											<div class="item active"><img src="{{URL('/assets/images/souvenir/project/usb_kulit_promosi_takasago.jpg')}}" /></div>
											<div class="item"><img src="{{URL('/assets/images/souvenir/project/pulpen_bank_sulteng.jpg')}}" /></div>
											<div class="item"><img src="{{URL('/assets/images/souvenir/project/name_card_holder_batr.jpg')}}" /></div>
											<div class="item"><img src="{{URL('/assets/images/souvenir/project/pulpen_promosi_ribuan_untuk_fortune-star.jpg')}}" /></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="formSouvenir">
							<form action="{{URL('/func/contactCheck.php')}}" method="post" name="contactForm" id="contactForm">
								<div class="row">
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-3">
												<label for="txt_nama" class="visible-xs visible-sm">Nama :</label>
											</div>
											<div class="col-md-8 col-xs-8">
												<input type="text" name="txt_nama" class="form-control" id="txt_nama" size="30" />
												<span class="alert_form"></span>
											</div>
										</div>
									</div>
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-3">
												<label for="txt_email" class="visible-xs visible-sm">Email :</label> 
											</div>
											<div class="col-md-8 col-xs-8">
												<input type="text" name="txt_email" class="form-control" id="txt_email" size="30" /> 
												<span class="alert_form"></span>
											</div>
										</div>
									</div>
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-3">
												<label for="txt_phone" class="visible-xs visible-sm">Nomor Telp:</label>
											</div>
											<div class="col-md-8 col-xs-8">
												<input type="text" name="txt_phone" class="form-control" id="txt_phone" size="30" />
											</div>
										</div>
									</div>
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-3">
												<label for="txt_pesan" class="visible-xs visible-sm">Pesan Anda:</label>
											</div>
											<div class="col-md-8 col-xs-8">
												<textarea name="txt_pesan" cols="30" rows="5" class="form-control" id="txt_pesan"></textarea>
											</div>
										</div>
									</div>
									<div class="col-xs-offset-3 col-xs-9">
										<input type="submit" name="submit" class="btn" value="Kirim Pesan">
									</div>
									<div class="clearfix"></div>
								</div>				
								<div id="err" style="display:none;color:red;text-align:center;margin-top:4px;margin-bottom:10px;font-size:12px;margin-left:20px;font-style:italic;"></div>
								<div id="loading" style="display:none;text-align:center;margin-top:4px;margin-bottom:10px;font-size:12px;font-style:italic;">Loading... <img src="{{URL('/assets/images/loading.gif')}}" alt="loading..."/></div>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
@endif
@stop
@section('content')
<div class="container" style="margin-bottom: 20px;">
    <div class="row" style="background: #FFF; padding: 0px; margin:0 -30px 0 -30px;">
        <div class="clearfix"></div>
		<div class="row" style="margin: auto;">
			<div id="divSouvenir" class="tab_content col-xs-12">
				<div id="divContentBottom" style="position: relative;"></div>
				<div id="divSouvenirSticky" style="width: 100%;">
					<div style="float: left;"><h1><span id="spnCategoryName">{{$souvenirCategoryName}}</span></h1></div>
					<div id="divDropdownKategory" style="float: right;">
						<div class="btn-group" style="margin: 10px auto 5px;">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Kategori Souvenir<span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right dropdown-menu-columns-2" style="z-index: 9999999; max-height: 600px; overflow: auto;">
								<div class="divMenuColumn2">
									<ul class="multi-column-dropdown">
										@for($i=0; $i < count($navigationCategory); $i++)
											@if ($i<count($navigationCategory)/2) 
												<li>
													<a class="lnkNavigationCategory" title="{{$categoryList[$i]->category_name}}" data-id="{{$categoryList[$i]->category_id}}" data-name="{{$categoryList[$i]->category_name}}" data-url="{{URL('/category/'.$categoryList[$i]->category_id)}}"><img src="{{URL($categoryList[$i]->imageUrl())}}" style="height:25px; vertical-align:middle; margin-right:7px;">{{$navigationCategory[$i]->category_name}}</a>
												</li>
											@endif
										@endfor
									</ul>
								</div>
								<div class="divMenuColumn2">
									<ul class="multi-column-dropdown">
										@for($i=0; $i < count($navigationCategory); $i++)
											@if ($i>=count($navigationCategory)/2) 
												<li>
													<a class="lnkNavigationCategory" title="{{$categoryList[$i]->category_name}}" data-id="{{$categoryList[$i]->category_id}}" data-name="{{$categoryList[$i]->category_name}}" data-url="{{URL('/category/'.$categoryList[$i]->category_id)}}"><img src="{{URL($categoryList[$i]->imageUrl())}}" style="height:25px; vertical-align:middle; margin-right:7px;">{{$navigationCategory[$i]->category_name}}</a>
												</li>
											@endif
										@endfor
									</ul>
								</div>
							</ul>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="border" style="margin-bottom: 20px;"></div>
				<div id="divLoader" style="display: none; text-align: center;"><img src="{{URL('/assets/images/ajax-loader.gif')}}" /></div>
				<div id="divContentProductSouvenir">
					@include('souvenir.ajax-souvenir')
				</div>
				<div class="clearfix"></div>
				<div class="visible-sm visible-xs" style="margin: 20px;">
					Kami menjual berbagai macam aneka barang promosi dan souvenir promosi untuk keperluan promosi perusahaan Anda, seperti pulpen, pensil, tumbler, jam, tempat kartu nama, flash disk / USB, dan lainnya. 
					Berbagai macam pilihan barang promosi dan souvenir yang kami sediakan dapat membantu memberikan banyak alternatif pilihan kepada Anda.
					Silahkan isi form disamping, untuk pertanyaan dan pemesanan barang.
				</div>
				<div id="emailForm" style="position: relative;"></div>
			</div>
		</div>
    </div>
</div>
@stop

@section('script')
<script type="text/javascript" src="{{URL('/assets/js/animation.js')}}"></script>
<script type="text/javascript" src="{{URL('/assets/js/jquery.innerfade.js')}}"></script>
<script type="text/javascript" src="{{URL('/assets/js/jquery.pngFix.js')}}"></script>
<script type="text/javascript" src="{{URL('/assets/js/jquery.browser.js')}}"></script>
<script type="text/javascript" src="{{URL('/assets/js/easySlider1.5.js')}}"></script>
<script>
	var divSouvenirStickyPosition = 0;
	var divScrollStickyPosition = 0;
	var divSouvenirSticky = $("#divSouvenirSticky");
	divSouvenirStickyPosition = divSouvenirSticky.offset().top;

	$(".lnkNavigationCategory").click(function(){
		var categoryID = $(this).attr("data-id");
		var categoryName = $(this).attr("data-name");
		var url = $(this).attr("data-url");
		$("#spnCategoryName").html(categoryName);
		$.ajax({
			beforeSend: function() {
				$('#divLoader').show();
			},
			url: url,
			success: function(result){
				$('#divLoader').hide();
				$("#divContentProductSouvenir").html(result);
			}
		});
	});

	$(document).ready(function(){
		if (window.location.pathname.substr(window.location.pathname.length - 8).replace(/\W/g, '') != "souvenir" && window.location.pathname.match("/product/") == null){
			if($("#divSouvenir")){
				$('html, body').animate({
					scrollTop: $("#divSouvenir").offset().top
				}, 1500);
			}
		}
		$().ajaxStart(function() {
			$('#loading').show();
			$('#err').hide();
		}).ajaxStop(function() {
			$('#loading').hide();
			$('#err').fadeIn('slow');
		});
		windowsWidth = $( window ).width();
		moveForm();
		$('#contactForm').submit(function() {
			$('#loading').show();
			$('#err').hide();
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data) {
						$('#loading').hide();
					if(data == "sukses")
					{
						$('#err').html('Pesan Anda telah kami terima. Kami akan memproses pesan Anda dan menghubungi Anda secepatnya. =)');
						document.getElementById('txt_nama').value="";
						document.getElementById('txt_email').value="";
						document.getElementById('txt_phone').value="";
						document.getElementById('txt_pesan').value="";
					}
					else
					$('#err').html(data);
					$('#err').fadeIn('slow');
				}
			})
			return false;
		});

		divScrollStickyPosition = $(window).scrollTop();
		adjustSlash();
	});

	$(window).on("load", function(){
		adjustSlash();
	});

	$( window ).scroll(function() {
		divScrollStickyPosition = $(window).scrollTop();
		var divSouvenirSticky = $("#divSouvenirSticky");
		if (divSouvenirStickyPosition<=divScrollStickyPosition){
			divSouvenirSticky.css("position", "fixed");
			divSouvenirSticky.css("top", "0");
			divSouvenirSticky.css("left", "0");
			divSouvenirSticky.css("background", "rgba(0,0,0,0.7)");
			divSouvenirSticky.css("padding", "0px 10%");
			divSouvenirSticky.css("z-index", "9");
		}
		else {
			divSouvenirSticky.css("position", "inherit");
			divSouvenirSticky.css("top", "inherit");
			divSouvenirSticky.css("left", "inherit");
			divSouvenirSticky.css("background", "inherit");
			divSouvenirSticky.css("padding", "inherit");
		}
	});

	$(window).on("resize", function(){
		if (windowsWidth){
			if (windowsWidth != $(window).width()){
				moveForm();
			}
			windowsWidth = $(window).width();
		}
		divSouvenirSticky.css("position", "inherit");
		divSouvenirSticky.css("top", "inherit");
		divSouvenirSticky.css("left", "inherit");
		divSouvenirSticky.css("background", "inherit");
		divSouvenirSticky.css("padding", "inherit");
		divSouvenirStickyPosition = divSouvenirSticky.offset().top;
		adjustSlash();
	});

	function moveForm(){
		if($(window).width() < 992 ) {
			$("#contactForm").appendTo("#emailForm");
			$("#containerSouvenir").appendTo("#divContentBottom");
		} else{
			$("#contactForm").appendTo("#formSouvenir");
			$("#containerSouvenir").appendTo("#divContentTop");
		}
		
		divSouvenirSticky.css("position", "inherit");
		divSouvenirSticky.css("top", "inherit");
		divSouvenirSticky.css("left", "inherit");
		divSouvenirSticky.css("background", "inherit");
		divSouvenirSticky.css("padding", "inherit");
		divSouvenirStickyPosition = divSouvenirSticky.offset().top;
	}
	function adjustSlash(){
		if($(window).width() < 992 ) {
			var slashPosition = $("#contentSouvenir").height() + 75;
			var slashPx = slashPosition + "px";
			$(".slash").css({"top": slashPx});
		} else{
			var slashPosition = $("#contentSlider").height() - 20;
			var slashPx = slashPosition + "px";
			$(".slash").css({"top": slashPx});
		}
	}
</script>
@stop